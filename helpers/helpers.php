<?php
//Retorna la URL de nuestro proyecto
function base_url(){
    return BASE_URL;
}
function media(){
    return base_url()."assets/";
}
function dep($data){
    $format  = print_r("<pre>");
    $format .= print_r($data);
    $format .= print_r("</pre>");
    return $format;
}
function getGrado($grado){
    $vGrade;
    switch ($grado) {
        case '1st Grade Elementary School':
            $vGrade = "E1";
            break;
        case '2nd Grade Elementary School':
            $vGrade = "E2";
            break;
        case '3rd Grade Elementary School':
            $vGrade = "E3";
            break;
        case '4th Grade Elementary School':
            $vGrade = "E4";
            break;
        case '5th Grade Elementary School':
            $vGrade = "E5";
            break;
        case '6th Grade Elementary School':
            $vGrade = "E6";
            break;
        case '7th Grade Middle School':
            $vGrade = "M7";
            break;
        case '8th Grade Middle School':
            $vGrade = "M8";
            break;
        case '9th Grade Middle School':
            $vGrade = "M9";
            break;
        case 'Kindergarten 1':
            $vGrade = "K1";
            break;
        case 'Kindergarten 2':
            $vGrade = "K2";
            break;
        case 'Kindergarten 3':
            $vGrade = "K3";
            break;
        case 'Pre-First':
            $vGrade = "PF";
            break;
        default:
            break;
    }
    return $vGrade;
}
function strClean($strCadena){
    $string = preg_replace(['/\s+/','/^\s|\s$/'],[' ',''], $strCadena);
    $string = trim($string); //Elimina espacios en blanco al inicio y al final
    $string = stripslashes($string); // Elimina las \ invertidas
    $string = str_ireplace("<script>","",$string);
    $string = str_ireplace("</script>","",$string);
    $string = str_ireplace("<script src>","",$string);
    $string = str_ireplace("<script type=>","",$string);
    $string = str_ireplace("SELECT * FROM","",$string);
    $string = str_ireplace("DELETE FROM","",$string);
    $string = str_ireplace("INSERT INTO","",$string);
    $string = str_ireplace("SELECT COUNT(*) FROM","",$string);
    $string = str_ireplace("DROP TABLE","",$string);
    $string = str_ireplace("OR '1'='1","",$string);
    $string = str_ireplace('OR "1"="1"',"",$string);
    $string = str_ireplace('OR ´1´=´1´',"",$string);
    $string = str_ireplace("is NULL; --","",$string);
    $string = str_ireplace("is NULL; --","",$string);
    $string = str_ireplace("LIKE '","",$string);
    $string = str_ireplace('LIKE "',"",$string);
    $string = str_ireplace("LIKE ´","",$string);
    $string = str_ireplace("OR 'a'='a","",$string);
    $string = str_ireplace('OR "a"="a',"",$string);
    $string = str_ireplace("OR ´a´=´a","",$string);
    $string = str_ireplace("OR ´a´=´a","",$string);
    $string = str_ireplace("--","",$string);
    $string = str_ireplace("^","",$string);
    $string = str_ireplace("[","",$string);
    $string = str_ireplace("]","",$string);
    $string = str_ireplace("==","",$string);
    return $string;
}
function token(){
    $r1 = bin2hex(random_bytes(10));
    $r2 = bin2hex(random_bytes(10));
    $r3 = bin2hex(random_bytes(10));
    $r4 = bin2hex(random_bytes(10));
    $token = $r1."-".$r2."-".$r3."-".$r4;
    return $token;
}
function getModal(string $nameModal, $data){
    $view_modal = "Views/template/modals/{$nameModal}.php";
    require_once $view_modal;
}
function initGoogle(){
    /* GOOGLE SESION */
        //Include Google Client Library for PHP autoload file
        include_once("assets/vendor/autoload.php");
        //Make object of Google API Client for call Google API
        $google_client = new Google_Client();
        //Set the OAuth 2.0 Client ID
        $google_client->setClientId('795745808440-nv9opjp5ob6jmet9dqalmui92nfgf6dg.apps.googleusercontent.com');
        //Set the OAuth 2.0 Client Secret key
        $google_client->setClientSecret('ahm7Duq12CYqpkfyXbDDllfE');
        //Set the OAuth 2.0 Redirect URI
        $google_client->setRedirectUri(BASE_URL.'login/correctLogin');
        //Data
        $google_client->addScope('email');
        $google_client->addScope('profile');
        //start session on web page
        // session_start();
        /* GOOGLE SESION */
        return $google_client;
}

function time_sesion(){
    return "86400";// un dia
}
/* Template */
function headerAdmin($data=""){
    $view_header = "Views/template/header_admin.php";
    require_once($view_header);
}
function footerAdmin($data=""){
    $view_footer = "Views/template/footer_admin.php";
    require_once($view_footer);
}
function preloadAdmin(){
    $view_preload = "Views/template/preload_admin.php";
    require_once($view_preload);
}
function topBarAdmin(){
    $view_topBar = "Views/template/topBar_admin.php";
    require_once($view_topBar);
}
function navAdmin(){
    $view_nav = "Views/template/nav_admin.php";
    require_once($view_nav);
}
/* Template */
?>