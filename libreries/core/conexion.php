<?php
class conexion{
    private $conect;
	private $conectCentral;

	public function __construct(){
		$connectionString = "mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8";
		try{
			$this->conect = new PDO($connectionString, DB_USER, DB_PASSWORD);
			$this->conect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    //echo "conexión exitosa";
		}catch(PDOException $e){
			$this->conect = 'Error de conexión';
		    echo "ERROR: " . $e->getMessage();
		}

		$connectionStringCentral = "mysql:host=".DB_HOST_CENTRAL.";dbname=".DB_NAME_CENTRAL.";charset=utf8";
		try{
			$this->conectCentral = new PDO($connectionStringCentral, DB_USER_CENTRAL, DB_PASSWORD_CENTRAL);
			$this->conectCentral->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    //echo "conexión exitosa";
		}catch(PDOException $e){
			$this->conectCentral = 'Error de conexión';
		    echo "ERROR: " . $e->getMessage();
		}
	}

	public function conect(){
		return $this->conect;
	}
	public function conectCentral(){
		return $this->conectCentral;
	}
}
?>