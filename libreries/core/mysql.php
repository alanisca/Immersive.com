<?php

class mysql extends conexion{
    private $conexion;
    private $conexionCentral;
    private $strQuery;
    private $arrValues;

    function __construct(){
        $this->conexion = new conexion();
        $this->conexionCentral = $this->conexion->conectCentral();
        $this->conexion = $this->conexion->conect();
    }
    //Bitacora insertar un registro
    public function bitacora(array $arrValues){
        $this->strQuery = "INSERT INTO bitacora(accion, id_usuario) VALUES(?,?)";
        $this->arrValues = $arrValues;

        $insert = $this->conexion->prepare($this->strQuery);
        
        $resInsert = $insert->execute($this->arrValues);
        if($resInsert){
            $lastInsert = $this->conexion->lastInsertId();
        }
        else{
            $lastInsert = 0;
        }
        return $lastInsert;
    }

    //insertar un registro
    public function insert(bool $DBName, string $query, array $arrValues){
        $this->strQuery = $query;
        $this->arrValues = $arrValues;

        // $insert = $this->conexion->prepare($this->strQuery);
        if($DBName) $insert = $this->conexion->prepare($this->strQuery);
        else $insert = $this->conexionCentral->prepare($this->strQuery);

        $resInsert = $insert->execute($this->arrValues);
        if($resInsert){
            $lastInsert = $this->conexion->lastInsertId();
        }
        else{
            $lastInsert = 0;
        }
        return $lastInsert;
    }

    //Buscar un registro
    public function select(bool $DBName, string $query){
        $this->strQuery = $query;
        // $result = $this->conexion->prepare($this->strQuery);
        if($DBName) $result = $this->conexion->prepare($this->strQuery);
        else $result = $this->conexionCentral->prepare($this->strQuery);

        $result->execute();
        $data = $result->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    //Buscar todos los registros
    public function selectAll(bool $DBName, string $query){
        $this->strQuery = $query;
        // $result = $this->conexion->prepare($this->strQuery);
        if($DBName) $result = $this->conexion->prepare($this->strQuery);
        else $result = $this->conexionCentral->prepare($this->strQuery);

        $result->execute();
        $data = $result->fetchall(PDO::FETCH_ASSOC);
        return $data;
    }

    //Actualizar registro(s)
    public function update(bool $DBName, string $query, array $arrValues){
        $this->strQuery = $query;
        $this->arrValues = $arrValues;
        // $update = $this->conexion->prepare($this->strQuery);
        if($DBName) $update = $this->conexion->prepare($this->strQuery);
        else $update = $this->conexionCentral->prepare($this->strQuery);

        $resExecute = $update->execute($this->arrValues);
        return $resExecute;
    }

    //Eliminar un registro
    public function delete(bool $DBName, string $query){
        $this->strQuery = $query;
        // $result = $this->conexion->prepare($this->strQuery);
        if($DBName) $result = $this->conexion->prepare($this->strQuery);
        else $result = $this->conexionCentral->prepare($this->strQuery);

        $del = $result->execute();
        return $del;
    }
}
?>