<?php
/* Load controllers */
$controllerFile = "Controllers/".$controller.".php";
if(file_exists($controllerFile)){
    require_once($controllerFile);
    $controller = new $controller();
    if(method_exists($controller, $method)){
        $controller->{$method}($params);
    }
    else {
        // echo "no existe el metodo";
        require_once("Controllers/error.php");
    }
}
else{
    // echo("No existe controlador");
    require_once("Controllers/error.php");
}
/* Load controllers */
?>