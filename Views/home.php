<!DOCTYPE html>
<html lang="en">
<head>
    <?= headerAdmin($data); ?>
    <link rel="stylesheet" href="<?= media(); ?>css/home.css">
</head>
<body class="fix-header card-no-border mini-sidebar">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
            <div class="row">
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-info"><i class="mdi mdi-cloud-check"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0 font-light" id="version">-</h3>
                                        <h5 class="text-muted m-b-0">Versión acutual</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-warning"><i class="mdi mdi-cube-outline"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0 font-lgiht" id="tot_recurosos">-</h3>
                                        <h5 class="text-muted m-b-0">Total de recursos</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-primary"><i class="mdi mdi-domain"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0 font-lgiht" id="tot_escuelas">-</h3>
                                        <h5 class="text-muted m-b-0">Total de escuelas</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-danger"><i class="mdi mdi-account"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0 font-lgiht" id="tot_alumnos">-</h3>
                                        <h5 class="text-muted m-b-0">Total de alumnos</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <div class="row">
                    <div class="col-lg-8" id="registros">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Registros del día</h4>
                                <div class="table-responsive m-t-20" style="max-height: 500px;">
                                    <table class="table stylish-table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Registro</th>
                                                <th>Fecha</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                    <div class="card">
                            <div class="m-t-0"></div>
                            <canvas id="myChart-iOS"></canvas>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-xs-12">
                        <div class="card">
                            <div class="d-flex flex-row">
                                <div class="p-10 bg-info">
                                    <h3 class="text-white box m-b-0"><i class="mdi mdi-cube-outline"></i></h3>
                                </div>
                                <div class="align-self-center m-l-20">
                                    <h3 class="m-b-0 text-info" id="tot-recursos"></h3>
                                    <h5 class="text-muted m-b-0">Recursos detonados</h5>
                                </div>
                            </div>
                            <div class="card-body">
                                <ul class="feeds" id="top-recursos" style="overflow: scroll; width: auto; height: 300px;"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="card">
                            <div class="d-flex flex-row">
                                <div class="p-10 bg-success">
                                    <h3 class="text-white box m-b-0"><i class="mdi mdi-account"></i></h3>
                                </div>
                                <div class="align-self-center m-l-20">
                                    <h3 class="m-b-0 text-success" id="tot-usuarios"></h3>
                                    <h5 class="text-muted m-b-0">Usuaros con registros</h5>
                                </div>
                            </div>
                            <div class="card-body">
                                <ul class="feeds" id="top-usuarios" style="overflow: scroll; width: auto; height: 300px;"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="card">
                            <div class="d-flex flex-row">
                                <div class="p-10 bg-primary">
                                    <h3 class="text-white box m-b-0"><i class="mdi mdi-domain"></i></h3>
                                </div>
                                <div class="align-self-center m-l-20">
                                    <h3 class="m-b-0 text-primary" id="tot-escuelas"></h3>
                                    <h5 class="text-muted m-b-0">Escuelas con registros</h5>
                                </div>
                            </div>
                            <div class="card-body">
                                <ul class="feeds" id="top-escuelas" style="overflow: scroll; width: auto; height: 300px;"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>plugins/chartjs/chartjs.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="<?= media(); ?>js/home/main.js"></script>
    <script>
        // $(window).ready(function() {
        //     $(".preloader").fadeOut();
        // });
        $(function() {
        // Handler for .ready() called.
        // $(".preloader").fadeOut();
        });

    </script>
</body>
</html>