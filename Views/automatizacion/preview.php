<!DOCTYPE html>
<html lang="es">
<head>
    <?= headerAdmin($data); ?>
    <!-- Footable CSS -->
    <link href="<?= media(); ?>plugins/footable/css/footable.core.css" rel="stylesheet">
    <link href="<?= media(); ?>plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />

    <link href="<?= media(); ?>css/main.css" rel="stylesheet">
    <link href="<?= media(); ?>css/preview.css" rel="stylesheet">
</head>
<body class="fix-header card-no-border mini-sidebar">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="d-flex flex-wrap">
                                    <div>
                                        <h5 class="card-title"><?= $data['datos']['Recurso']; ?></h5>
                                        <h6 class="card-subtitle" id="codeName" style="margin-bottom:0"></h6> 
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <form action="" class="">
                                    <div class="form-group row" id="opciones">
                                        <div class="demo-switch-title text-center col-md-4 col-sm-6 col-xs-12">Misma imagen:
                                            <div class="switch">
                                                <label><input type="checkbox" id="sameImg" disabled><span class="lever switch-col-indigo"></span></label>
                                            </div>
                                        </div>
                                        <div class="demo-switch-title text-center col-md-4 col-sm-6 col-xs-12">Encabezado:
                                            <div class="switch">
                                                <label><input type="checkbox" id="encabezado" disabled><span class="lever switch-col-indigo"></span></label>
                                            </div>
                                        </div>
                                        <div class="demo-switch-title text-center col-md-4 col-sm-6 col-xs-12">Matemáticas:
                                            <div class="switch">
                                                <label><input type="checkbox" id="math" disabled><span class="lever switch-col-indigo"></span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="" id="datos">
                                        <ul class="nav nav-tabs tabs-vertical" role="tablist" id="preguntas"></ul>
                                        <div class="tab-content tabcontent-border" id="respuestas"></div>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer">
                                <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-contact">Guardar</button>
                            </div>
                        </div>    
                    </div>
                    <!-- <div class="col-lg-4 col-xlg-3 col-md-12">
                        <div class="card blog-widget">
                            <div class="card-body">
                                <div class="blog-image"><img src="http://localhost:8888/knimmersive/assets/interactives/TH/PB/Multiple_Choice.jpg" alt="img" class="img-responsive"></div>
                                <h4 class="card-title"><?= $data['datos']['Tipo']; ?></h4>
                                <p class="card-text">
                                    <label class="label label-rounded label-success"><?= $data['datos']['Version']; ?></label>
                                    <label class="label label-rounded label-warning"><?= $data['datos']['Idioma']; ?></label>
                                    <label class="label label-rounded label-warning">Reto: <?= $data['datos']['Reto']; ?></label>
                                </p>
                                <small class="text-muted">Pathway</small>
                                <h6><?= $data['datos']['Pathway']; ?></h6>
                                <small class="text-muted">Grado</small>
                                <h6><?= $data['datos']['Grado']; ?></h6>
                                <small class="text-muted">Reto</small>
                                <h6><?= $data['datos']['Reto']; ?></h6>
                                <small class="text-muted">Sesion</small>
                                <h6><?= $data['datos']['Sesion']; ?></h6>
                                <small class="text-muted">Actividad</small>
                                <h6><?= $data['datos']['Actividad']; ?></h6>
                                <small class="text-muted">Paso</small>
                                <h6><?= $data['datos']['Paso']; ?></h6>
                                <small class="text-muted">Script</small>
                                <h6><a href="<?= $data['datos']['Script_Link']; ?>" target="_blank"><i class="fa fa-external-link"></i></a></h6>
                                <small class="text-muted">Nomenclatura</small>
                                <h6><?= $data['datos']['nomenclatura']; ?></h6>
                                
                                <div class="d-flex">
                                    <div class="read"><a href="javascript:void(0)" class="link font-medium">Read More</a></div>
                                    <div class="ml-auto">
                                        <a href="javascript:void(0)" class="link m-r-10 " data-toggle="tooltip" title="" data-original-title="Like"><i class="mdi mdi-heart-outline"></i></a> <a href="javascript:void(0)" class="link" data-toggle="tooltip" title="" data-original-title="Share"><i class="mdi mdi-share-variant"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <?php getModal("modalChoice",$data); ?>

    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>plugins/footable/js/footable.min.js" type="text/javascript"></script>
    <script src="<?= media(); ?>plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <!--FooTable init-->
    <script src="<?= media(); ?>js/footable-init.js"></script>
    <script src="<?= media(); ?>interactives/original/<?= $data['datos']['nomenclatura']; ?>/js/data.js"></script>
    <script src="<?= media(); ?>js/automatizacion/previewInteractive.js"></script>
</body>
</html>