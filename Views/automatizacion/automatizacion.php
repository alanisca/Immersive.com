<!DOCTYPE html>
<html lang="en">
<head>
    <?= headerAdmin($data); ?>
    <!-- Footable CSS -->
    <link href="<?= media(); ?>plugins/footable/css/footable.bootstrap.min.css" rel="stylesheet">
    <!-- <link href="<?= media(); ?>plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" /> -->

    <link href="<?= media(); ?>css/main.css" rel="stylesheet">
</head>
<body class="fix-header card-no-border mini-sidebar">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><?= $data['tag_title']; ?></h4>
                                <h6 class="card-subtitle"><?= $data['tag_text']; ?></h6>
                                <table id="automatizacion-toggler" class="table"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
asdf
    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>plugins/footable/js/footable.min.js" type="text/javascript"></script>
    <!-- <script src="<?= media(); ?>plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script> -->
    <!--FooTable init-->
    <script src="<?= media(); ?>js/automatizacion/footable-init.js"></script>
</body>
</html>