<div id="aprobar-requisicion" class="modal fade bs-example-modal-lg show" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="formReq" name="formReq">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Filtrar recursos Immersive</h4>
                    <input type="text" style="display:none;" id="req-id_recurso" name="req-id_recurso">
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-6 col-md-6 col-lg-3">
                            <label for="recipient-name" class="control-label">Requisición: </label>
                            <input type="text" class="form-control" id="req-requisicion" name="req-requisicion">
                        </div>
                        <div class="form-group col-6 col-md-6 col-lg-3">
                            <label for="recipient-name" class="control-label ">Unity-Target: 
                                <!-- <i class="mdi mdi-content-copy" id="copy-unityTarget"></i><i class="mdi mdi-apple unityTarget" id="copy-unityTarget"></i> -->
                            </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="req-unityTarget" name="req-unityTarget">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="button"><i class="mdi mdi-content-copy" id="copy-unityTarget"></i></button>
                                </div>
                                <div class="input-group-btn">
                                    <button class="btn btn-primary unityTarget" type="button"><i class="mdi mdi-login" id="copy-unityTarget"></i></button>
                                </div>
                            </div>
                            <!-- <input type="text" class="form-control" id="req-unityTarget" name="req-unityTarget"> -->
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="recipient-name" class="control-label">Título del Recurso:</label>
                            <input type="text" class="form-control" id="req-titulo" name="req-titulo">
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="recipient-name" class="control-label">Unity-Bundle:</label>
                            <input type="text" class="form-control" id="req-unityBundle" name="req-unityBundle">
                        </div>
                        <div class="form-group col-6 col-md-4 col-lg-3">
                            <label for="message-text" class="control-label">Reto:</label>
                            <select class="form-control custom-select" id="req-reto" name="req-reto">
                                <option value="0"></option>
                                <option value="1">Reto 1</option>
                                <option value="2">Reto 2</option>
                                <option value="3">Reto 3</option>
                                <option value="4">Reto 4</option>
                                <option value="5">Reto 5</option>
                                <option value="6">Reto 6</option>
                                <option value="7">Reto 7</option>
                                <option value="8">Reto 8</option>
                            </select>
                        </div>
                        <div class="form-group col-6 col-md-4 col-lg-3">
                            <label for="message-text" class="control-label">Grado:</label>
                            <select class="form-control custom-select" id="req-grado" name="req-grado">
                                <option value=""></option>
                                <option value="E">E</option>
                                <option value="E1">E1</option>
                                <option value="E2">E2</option>
                                <option value="E3">E3</option>
                                <option value="E4">E4</option>
                                <option value="E5">E5</option>
                                <option value="E6">E6</option>
                                <option value="M">M</option>
                                <option value="M7">M7</option>
                                <option value="M8">M8</option>
                                <option value="M9">M9</option>
                                <option value="K">K</option>
                                <option value="K1">K1</option>
                                <option value="K2">K2</option>
                                <option value="K3">K3</option>
                                <option value="PF">PF</option>
                            </select>
                        </div>
                        <div class="form-group col-6 col-md-4 col-lg-4">
                            <label for="message-text" class="control-label">Tipo de recuso:</label>
                            <select class="form-control custom-select" id="req-tipo_recurso" name="req-tipo_recurso">
                                <option value=""></option>
                                <option value="IMM Augmented Reality">IMM Augmented Reality</option>
                                <option value="IMM Interactive">IMM Interactive</option>
                                <option value="IMM Prime">IMM Prime</option>
                                <option value="IMM Virtual Reality">IMM Virtual Reality</option>
                                <option value="Interactive">Interactive</option>
                            </select>
                        </div>
                        <div class="form-group col-6 col-md-4 col-lg-4">
                            <label for="message-text" class="control-label">Version del recurso:</label>
                            <select class="form-control custom-select" id="req-version_recurso" name="req-version_recurso">
                                <option value=""></option>
                                <option value="360º Photo">360º Photo</option>
                                <option value="360º Stage">360º Stage</option>
                                <option value="3D Model">3D Model</option>
                                <option value="AR Instructions">AR Instructions</option>
                                <option value="Body AR">Body AR</option>
                                <option value="Custom Hologram">Custom Hologram</option>
                                <option value="Custom Interactive">Custom Interactive</option>
                                <option value="Guess it!">Guess it!</option>
                                <option value="Isometric Infographic">Isometric Infographic</option>
                                <option value="Learn & Practice">Learn & Practice</option>
                                <option value="Premium">Premium</option>
                                <option value="Target">Target</option>
                                <option value="Trivia Now!">Trivia Now!</option>
                                <option value="Virtual Museum">Virtual Museum</option>
                                <option value="Voxel Model">Voxel Model</option>
                            </select>
                        </div>
                        <div class="form-group col-6 col-md-4 col-lg-2">
                            <label for="message-text" class="control-label">Idioma:</label>
                            <select class="form-control custom-select" id="req-idioma" name="req-idioma">
                                <option value=""></option>
                                <option value="ES">Español</option>
                                <option value="EN">Ingles</option>
                            </select>
                        </div>
                        <div class="form-group col-6 col-md-4 col-lg-2">
                            <label for="message-text" class="control-label">Sesión:</label>
                            <select class="form-control custom-select" id="req-sesion" name="req-sesion">
                                <option value="0"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                            </select>
                        </div>
                        <div class="form-group col-6 col-md-3 col-lg-4">
                            <label for="message-text" class="control-label">Ciclo:</label>
                            <select class="form-control custom-select" id="req-ciclo" name="req-ciclo">
                                <option value=""></option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="CO 2020">CO 2020</option>
                                <option value="CO 2021">CO 2021</option>
                                <option value="CO 2021 - 2022">CO 2021 - 2022</option>
                                <option value="GT 2019">GT 2019</option>
                                <option value="MX 2016 - 2017">MX 2016 - 2017</option>
                                <option value="MX 2017 - 2018">MX 2017 - 2018</option>
                                <option value="MX 2018 - 2019">MX 2018 - 2019</option>
                                <option value="MX 2019 - 2020">MX 2019 - 2020</option>
                                <option value="MX 2020 - 2021">MX 2020 - 2021</option>
                                <option value="MX 2021 - 2022">MX 2021 - 2022</option>
                                <option value="MX 2022 - 2023">MX 2022 - 2023</option>
                            </select>
                        </div>
                        <div class="form-group col-6 col-md-3 col-lg-4">
                            <label for="message-text" class="control-label">Pathway:</label>
                            <select class="form-control custom-select" id="req-pathway" name="req-pathway">
                                <option value=""></option>
                                <option value="AR">AR: Arte</option>
                                <option value="BI">BI: Biología</option>
                                <option value="CE">CE: Formación Cívica y Ética</option>
                                <option value="CH">CH: Química</option>
                                <option value="CC">CC: Competencias de ciudadanía global</option>
                                <option value="DF">DF: Educación Física</option>
                                <option value="EA">EA: Arte</option>
                                <option value="EF">EF: Educación Física</option>
                                <option value="EL">EL: English Literacy</option>
                                <option value="EN">EN: English</option>
                                <option value="ES">ES: Español</option>
                                <option value="FC">FC: Civics and Ethics</option>
                                <option value="FI">FI: Finanzas</option>
                                <option value="FS">FS: Física</option>
                                <option value="GC">GC: Global Citizenship</option>
                                <option value="GE">GE: Geografía</option>
                                <option value="HE">HE: Heedfulness</option>
                                <option value="HI">HI: Historia de México</option>
                                <option value="LG">LG: Matemáticas Inglés / Kn·light</option>
                                <option value="MA">MA: Matemáticas</option>
                                <option value="MH">MH: Historia</option>
                                <option value="MU">MU: Música</option>
                                <option value="NU">NU: Nutrición</option>
                                <option value="OR">OR: Organik</option>
                                <option value="PH">PH: Physics</option>
                                <option value="SC">SC: Science</option>
                                <option value="TE">TE: Tecnología</option>
                                <option value="TU">TU: Tutoría</option>
                                <option value="WH">WH: Historia Universal</option>
                            </select>
                        </div>
                        <div class="form-group col-6 col-md-3 col-lg-2">
                            <label for="message-text" class="control-label">Estatus:</label>
                            <select class="form-control custom-select" id="req-estatus" name="req-estatus">
                                <option value=""></option>
                                <option value="Produccion">Producción</option>
                                <option value="Activa">Activa</option>
                                <option value="En linea">En linea</option>
                                <option value="Cancelada">Cancelada</option>
                            </select>
                        </div>
                        <div class="form-group col-6 col-md-3 col-lg-2">
                            <label for="recipient-name" class="control-label">Version:</label>
                            <input type="text" class="form-control" id="req-version" name="req-version">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="recipient-name" class="control-label" id="title-liga-script">Liga del script: </label>
                            <input type="text" class="form-control" id="req-url_script" name="req-url_script">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="recipient-name" class="control-label" id="title-liga-workflow">Liga de workflow: </label>
                            <input type="text" class="form-control" id="req-url_workflow" name="req-url_workflow">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="recipient-name" class="control-label" id="title-liga-ra">Liga de RA (ResourceBank): </label>
                            <input type="text" class="form-control" id="req-url_ra" name="req-url_ra" disabled>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="recipient-name" class="control-label" id="title-liga-autoria">Liga de Recurso (ResourceBank): </label>
                            <input type="text" class="form-control" id="req-url_autoria" name="req-url_autoria">
                        </div>
                        <!-- <div class="form-group col-12">
                            <label for="recipient-name" class="control-label ">Adjuntar archivos:</label>
                            <div class="input-group">
                                <select class="form-control col-4" name="req-new_file" id="">
                                    <option value="">Tipo:</option>
                                    <option value="Imagen">Imagen</option>
                                    <option value="Video">Video</option>
                                    <option value="PDF">PDF</option>
                                </select>
                                <input type="text" class="form-control" placeholder="Título del archivo">
                                <input type="text" class="form-control" placeholder="URL del archivo">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary unityTarget" type="button"><i class="mdi mdi-plus" id="add-file"></i></button>
                                </div>
                            </div>
                            <div class="input-group">
                                <select class="form-control col-4" name="req-new_file" id="">
                                    <option value="">Tipo:</option>
                                    <option value="Imagen">Imagen</option>
                                    <option value="Video">Video</option>
                                    <option value="PDF">PDF</option>
                                </select>
                                <input type="text" class="form-control" placeholder="Título del archivo">
                                <input type="text" class="form-control" placeholder="URL del archivo">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary unityTarget" type="button"><i class="mdi mdi-delete" id="delete-file"></i></button>
                                </div>
                            </div>
                            <div class="input-group">
                                <select class="form-control col-4" name="req-new_file" id="">
                                    <option value="">Tipo:</option>
                                    <option value="Imagen">Imagen</option>
                                    <option value="Video">Video</option>
                                    <option value="PDF">PDF</option>
                                </select>
                                <input type="text" class="form-control" placeholder="Título del archivo">
                                <input type="text" class="form-control" placeholder="URL del archivo">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary unityTarget" type="button"><i class="mdi mdi-delete" id="delete-file"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="recipient-name" class="control-label ">Vinculaciones:</label>
                            <div class="input-group">
                                <select class="form-control" name="req-new_file" id="">
                                    <option value="">Ciclo:</option>
                                </select>
                                <select class="form-control" name="req-new_file" id="">
                                    <option value="">Reto:</option>
                                </select>
                                <select class="form-control" name="req-new_file" id="">
                                    <option value="">Grado:</option>
                                </select>
                                <div class="input-group-btn">
                                    <button class="btn btn-primary unityTarget" type="button"><i class="mdi mdi-plus" id="add-vinculacion"></i></button>
                                </div>
                            </div>
                            <div class="input-group">
                                <select class="form-control" name="req-new_file" id="">
                                    <option value="">Ciclo:</option>
                                </select>
                                <select class="form-control" name="req-new_file" id="">
                                    <option value="">Grado:</option>
                                </select>
                                <select class="form-control" name="req-new_file" id="">
                                    <option value="">Reto:</option>
                                </select>
                                <div class="input-group-btn">
                                    <button class="btn btn-primary unityTarget" type="button"><i class="mdi mdi-delete" id="remove-vinculacion"></i></button>
                                </div>
                            </div>
                            <div class="input-group">
                                <select class="form-control" name="req-new_file" id="">
                                    <option value="">Ciclo:</option>
                                </select>
                                <select class="form-control" name="req-new_file" id="">
                                    <option value="">Grado:</option>
                                </select>
                                <select class="form-control" name="req-new_file" id="">
                                    <option value="">Reto:</option>
                                </select>
                                <div class="input-group-btn">
                                    <button class="btn btn-primary unityTarget" type="button"><i class="mdi mdi-delete" id="remove-vinculacion"></i></button>
                                </div>
                            </div>
                        </div> -->
                        <div class="form-group col-lg-12">
                            <label for="message-text" class="control-label">Comentarios:</label>
                            <textarea class="form-control" id="req-comentarios" name="req-comentarios"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-danger waves-effect" id="dalete-req">Eliminar</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light" id="save-req">Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>