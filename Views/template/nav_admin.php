        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">Usuario <?php if(isset($_SESSION['tipo_usuario']) && !empty($_SESSION['tipo_usuario'])) echo $_SESSION['tipo_usuario']; else echo "sin tipo"; ?></li>
                        <?php if(isset($_SESSION['acceso_IMM']) && !empty($_SESSION['acceso_IMM']) && $_SESSION['acceso_IMM'] == 1){ ?>
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="<?= base_url()?>" aria-expanded="false"><i class="mdi mdi-binoculars"></i><span class="hide-menu">IMMERSIVE </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <!-- <li><a href="#">Dashboard</a></li> -->
                                <!-- <li><a href="#">Producción</a></li> -->
                                <!-- <li><a href="<?= base_url()."catalogoImm"; ?>">Catálogo</a></li> -->
                                <li><a href="<?= base_url()."catalogoImm/recursos"; ?>">Recursos</a></li>
                                <li><a href="<?= base_url()."escuelas"; ?>">Escuelas</a></li>
                                <li><a href="<?= base_url()."catalogoImm/version"; ?>">Versión</a></li>
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if(isset($_SESSION['acceso_Int']) && !empty($_SESSION['acceso_Int']) && $_SESSION['acceso_Int'] == 1){ ?>
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-image-album"></i><span class="hide-menu">INTERACTIVOS </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <!-- <li><a href="#">Dashboard</a></li> -->
                                <li><a href="<?= base_url()."automatizacion"; ?>">Automatización</a></li>
                                <!-- <li><a href="#">Catálogo</a></li> -->
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if(isset($_SESSION['tipo_usuario']) && !empty($_SESSION['tipo_usuario']) && $_SESSION['tipo_usuario'] == "Administrador"){ ?>
                        <li>
                            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Configuración </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?= base_url()."users"; ?>">Usuarios</a></li>
                                <!-- <li><a href="#">Roles</a></li> -->
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <!-- End Bottom points-->
        </aside>