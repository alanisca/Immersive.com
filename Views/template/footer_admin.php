    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script>
        var clickHandlerDown = ('ontouchstart' in document.documentElement ? "touchstart" : "mousedown");
        var clickHandlerUp = ('ontouchend' in document.documentElement ? "touchend" : "mouseup");
        var clickHandler = ('ontouchstart' in document.documentElement ? "click" : "click");
    </script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= media(); ?>plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?= media(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= media(); ?>plugins/js/jquery.slimscroll.js"></script>
    <!--Wave Effects efectos al dar click -->
    <script src="<?= media(); ?>plugins/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?= media(); ?>plugins/js/sidebarmenu.js"></script>
    <!--stickey kit funcion del menú para que se abra -->
    <script src="<?= media(); ?>plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?= media(); ?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?= media(); ?>plugins/js/custom.min.js"></script>
    <script src="<?= media(); ?>js/functions.js"></script>
    <script>
        const base_url = "<?= base_url();?>";
    </script>