    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= media(); ?>images/favicon.png">
    <title><?= $data["tag_name"] ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= media(); ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= media(); ?>css/style.css" rel="stylesheet">
    <!-- GENERALES KN -->
    <link href="<?= media(); ?>css/main.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?= media(); ?>css/colors/red-dark.css" id="theme" rel="stylesheet">
    <script src="<?= media(); ?>plugins/jquery/jquery.min.js"></script>
