<!DOCTYPE html>
<html lang="en">
<head>
    <?= headerAdmin($data); ?>
    <!-- Footable CSS -->
    <link href="<?= media(); ?>css/requisiciones.css" rel="stylesheet">    
    <link href="<?= media(); ?>plugins/toast-master/css/jquery.toast.css  " rel="stylesheet">
</head>
<body class="fix-header card-no-border mini-sidebar">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="">
                    <button class=" waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10" id="nuevo-recurso">
                        <i class="mdi mdi-plus"></i>
                    </button>
                    <button class=" waves-effect waves-light btn-warning btn btn-circle btn-sm pull-right m-l-10" id="filtrar-recursos">
                        <i class="mdi mdi-filter"></i>
                    </button>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                            <div class="button-group pull-right">
                                    <!-- <button id="nuevo-recurso" type="button" class="btn waves-effect waves-light btn-outline-primary">Nuevo</button> -->
                                    <!-- <button id="filtrar-recursos" type="button" class="btn waves-effect waves-light btn-outline-success">Filtros</button> -->
                                </div>
                                <h4 class="card-title"><?= $data['tag_title']; ?></h4>
                                <h6 class="card-subtitle"><?= $data['tag_text']; ?></h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover" id="requisiciones">
                                        <thead>
                                            <tr>
                                                <th class="d-none d-sm-none d-md-block"><h6>ID Target</h6></th>
                                                <th><h6>Nombre</h6></th>
                                                <!-- <th><h6>Tipo</h6></th>
                                                <th><h6>Version</h6></th> -->
                                                <th class="d-none d-sm-none d-md-block"><h6>Grado</h6></th>
                                                <th><h6>Reto</h6></th>
                                                <th class="d-none d-sm-none d-md-block"><h6>Estatus</h6></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer">
                                <span class="label label-light-primary"></span>
                                <span class="label label-light-warning">Producción</span>
                                <span class="label label-light-info">Activa</span>
                                <span class="label label-light-success">En linea</span>
                                <span class="label label-light-danger">Cancelada</span>
                            </div>
                        </div>
                    </div>    
                </div>
                <div class="right-sidebar col-sm-10 col-md-8" style="display: block;">
                    <div class="slimScrollDiv" style="position: relative; overflow: scroll; width: auto; height: 100%;"><div class="slimscrollright" >
                        <div class="rpanel-title"> Filtrar recursos <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <div id="formFilterReq" name="">
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="recipient-name" class="control-label">Buscar:</label>
                                        <input type="text" class="form-control" id="req-busqueda" name="req-titulo">
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="message-text" class="control-label expander collapsed" data-toggle="collapse" data-target="#collapseReto" aria-expanded="false" aria-controls="collapseReto">Reto:</label>
                                        <div class="collapse" id="collapseReto">
                                            <div class="col-12">
                                                <input name="radio_reto" type="radio" id="md_checkbox_reto_1" class="radio-col-red" data-type="reto" data-val="1" checked>
                                                <label for="md_checkbox_reto_1">Reto 1</label>
                                            </div>
                                            <div class="col-12">
                                                <input name="radio_reto" type="radio" id="md_checkbox_reto_2" class="radio-col-lime" data-type="reto" data-val="2">
                                                <label for="md_checkbox_reto_2">Reto 2</label>
                                            </div>
                                            <div class="col-12">
                                                <input name="radio_reto" type="radio" id="md_checkbox_reto_3" class="radio-col-light-blue" data-type="reto" data-val="3">
                                                <label for="md_checkbox_reto_3">Reto 3</label>
                                            </div>
                                            <div class="col-12">
                                                <input name="radio_reto" type="radio" id="md_checkbox_reto_4" class="radio-col-deep-purple" data-type="reto" data-val="4">
                                                <label for="md_checkbox_reto_4">Reto 4</label>
                                            </div>
                                            <div class="col-12">
                                                <input name="radio_reto" type="radio" id="md_checkbox_reto_5" class="radio-col-orange" data-type="reto" data-val="5">
                                                <label for="md_checkbox_reto_5">Reto 5</label>
                                            </div>
                                            <div class="col-12">
                                                <input name="radio_reto" type="radio" id="md_checkbox_reto_6" class="radio-col-red" data-type="reto" data-val="6">
                                                <label for="md_checkbox_reto_6">Reto 6</label>
                                            </div>
                                            <div class="col-12">
                                                <input name="radio_reto" type="radio" id="md_checkbox_reto_7" class="radio-col-indigo" data-type="reto" data-val="7">
                                                <label for="md_checkbox_reto_7">Reto 7</label>
                                            </div>
                                            <div class="col-12">
                                                <input name="radio_reto" type="radio" id="md_checkbox_reto_8" class="radio-col-amber" data-type="reto" data-val="8">
                                                <label for="md_checkbox_reto_8">Reto 8</label>
                                            </div>
                                            <div class="col-12">
                                                <input name="radio_reto" type="radio" id="md_checkbox_reto_null" class="radio-col-black" data-type="reto" data-val="null">
                                                <label for="md_checkbox_reto_null">Sin clasificar</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="message-text" class="control-label expander collapsed" data-toggle="collapse" data-target="#collapseGrado" aria-expanded="false" aria-controls="collapseGrado">Grado:</label>
                                        <div class="collapse" id="collapseGrado">
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_K1" class="filled-in chk-col-red col-12" data-type="grado" data-val="K1">
                                                <label for="md_checkbox_K1">K1</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_K2" class="filled-in chk-col-red col-12" data-type="grado" data-val="K2">
                                                <label for="md_checkbox_K2">K2</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_K3" class="filled-in chk-col-red col-12" data-type="grado" data-val="K3">
                                                <label for="md_checkbox_K3">K3</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_PF" class="filled-in chk-col-red col-12" data-type="grado" data-val="PF">
                                                <label for="md_checkbox_PF">PF</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_E1" class="filled-in chk-col-red col-12" data-type="grado" data-val="E1">
                                                <label for="md_checkbox_E1">E1</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_E2" class="filled-in chk-col-red col-12" data-type="grado" data-val="E2">
                                                <label for="md_checkbox_E2">E2</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_E3" class="filled-in chk-col-red col-12" data-type="grado" data-val="E3">
                                                <label for="md_checkbox_E3">E3</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_E4" class="filled-in chk-col-red col-12" data-type="grado" data-val="E4">
                                                <label for="md_checkbox_E4">E4</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_E5" class="filled-in chk-col-red col-12" data-type="grado" data-val="E5">
                                                <label for="md_checkbox_E5">E5</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_E6" class="filled-in chk-col-red col-12" data-type="grado" data-val="E6">
                                                <label for="md_checkbox_E6">E6</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_M7" class="filled-in chk-col-red col-12" data-type="grado" data-val="M7">
                                                <label for="md_checkbox_M7">M7</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_M8" class="filled-in chk-col-red col-12" data-type="grado" data-val="M8">
                                                <label for="md_checkbox_M8">M8</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_M9" class="filled-in chk-col-red col-12" data-type="grado" data-val="M9">
                                                <label for="md_checkbox_M9">M9</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_null" class="filled-in chk-col-black col-12" data-type="grado" data-val="null">
                                                <label for="md_checkbox_null">Sin clasificar</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="message-text" class="control-label expander collapsed" data-toggle="collapse" data-target="#collapseTipoRecurso" aria-expanded="false" aria-controls="collapseTipoRecurso">Tipo de recurso:</label>
                                        <div class="collapse" id="collapseTipoRecurso">
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_IMMAugmentedReality" class="filled-in chk-col-red col-12" data-type="tipo" data-val="IMM Augmented Reality">
                                                <label for="md_checkbox_IMMAugmentedReality">IMM Augmented Reality</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_IMMInteractive" class="filled-in chk-col-red col-12" data-type="tipo" data-val="IMM Interactive">
                                                <label for="md_checkbox_IMMInteractive">IMM Interactive</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_IMMPrime" class="filled-in chk-col-red col-12" data-type="tipo" data-val="IMM Prime">
                                                <label for="md_checkbox_IMMPrime">IMM Prime</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_IMMVirtualReality" class="filled-in chk-col-red col-12" data-type="tipo" data-val="IMM Virtual Reality">
                                                <label for="md_checkbox_IMMVirtualReality">IMM Virtual Reality</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="message-text" class="control-label expander collapsed" data-toggle="collapse" data-target="#collapseVersionRecurso" aria-expanded="false" aria-controls="collapseVersionRecurso">Version del recurso:</label>
                                            <div class="collapse" id="collapseVersionRecurso">
                                                <div class="col-12">
                                                    <input type="checkbox" id="360º_Photo" class="filled-in chk-col-red col-12 360º_Photo" data-type="version" data-val="360º Photo">
                                                    <label for="360º_Photo">360º Photo</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="360º_Stage" class="filled-in chk-col-red col-12 360º_Stage" data-type="version" data-val="360º Stage">
                                                    <label for="360º_Stage">360º Stage</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="3D_Model" class="filled-in chk-col-red col-12 3D_Model" data-type="version" data-val="3D Model">
                                                    <label for="3D_Model">3D Model</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="AR_Instructions" class="filled-in chk-col-red col-12 AR_Instructions" data-type="version" data-val="AR Instructions">
                                                    <label for="AR_Instructions">AR Instructions</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="Body_AR" class="filled-in chk-col-red col-12 Body_AR" data-type="version" data-val="Body AR">
                                                    <label for="Body_AR">Body AR</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="Custom_Hologram" class="filled-in chk-col-red col-12 Custom_Hologram" data-type="version" data-val="Custom Hologram">
                                                    <label for="Custom_Hologram">Custom Hologram</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="Custom_Interactive" class="filled-in chk-col-red col-12 Custom_Interactive" data-type="version" data-val="Custom Interactive">
                                                    <label for="Custom_Interactive">Custom Interactive</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="Guess_it" class="filled-in chk-col-red col-12 Guess_it" data-type="version" data-val="Guess it!">
                                                    <label for="Guess_it">Guess it!</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="Isometric_Infographic" class="filled-in chk-col-red col-12 Isometric_Infographic" data-type="version" data-val="Isometric Infographic">
                                                    <label for="Isometric_Infographic">Isometric Infographic</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="Learn" class="filled-in chk-col-red col-12 Learn & Practice" data-type="version" data-val="Learn & Practice">
                                                    <label for="Learn">Learn & Practice</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="Premium" class="filled-in chk-col-red col-12 Premium" data-type="version" data-val="Premium">
                                                    <label for="Premium">Premium</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="Target" class="filled-in chk-col-red col-12 Target" data-type="version" data-val="Target">
                                                    <label for="Target">Target</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="Trivia_Now" class="filled-in chk-col-red col-12 Trivia_Now" data-type="version" data-val="Trivia Now!">
                                                    <label for="Trivia_Now">Trivia Now!</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="Virtual_Museum" class="filled-in chk-col-red col-12 Virtual_Museum" data-type="version" data-val="Virtual Museum">
                                                    <label for="Virtual_Museum">Virtual Museum</label>
                                                </div>
                                                <div class="col-12">
                                                    <input type="checkbox" id="VoxelModel" class="filled-in chk-col-red col-12 VoxelModel" data-type="version" data-val="Voxel Model">
                                                    <label for="VoxelModel">Voxel Model</label>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="message-text" class="control-label expander collapsed" data-toggle="collapse" data-target="#collapseEstatus" aria-expanded="false" aria-controls="collapseEstatus">Estatus:</label>
                                        <div class="collapse" id="collapseEstatus">
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_Produccion" class="filled-in chk-col-red col-12" data-type="estatus" data-val="Producción">
                                                <label for="md_checkbox_Produccion">Producción</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_Activa" class="filled-in chk-col-red col-12" data-type="estatus" data-val="Activa">
                                                <label for="md_checkbox_Activa">Activa</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_En_linea" class="filled-in chk-col-red col-12" data-type="estatus" data-val="En linea">
                                                <label for="md_checkbox_En_linea">En linea</label>
                                            </div>
                                            <div class="col-12">
                                                <input type="checkbox" id="md_checkbox_Cancelada" class="filled-in chk-col-red col-12" data-type="estatus" data-val="Cancelada">
                                                <label for="md_checkbox_Cancelada">Cancelada</label>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="text" class="btn waves-effect waves-light btn-success" id="success">Filtrar</button>
                                </div>
                            </div>
                        </div>
                    </div><div class="slimScrollBar" style="background: rgb(220, 220, 220); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 876.924px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                </div>
            </div>
        </div>
    </div>
    <?= getModal("modalRequisiciones", $data); ?>
    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>js/recursosIMM/getRequisiciones.js"></script>
    <script src="<?= media(); ?>js/recursosIMM/filterRecursos.js"></script>
    <script src="<?= media(); ?>plugins/toast-master/js/jquery.toast.js"></script>
</body>
</html>