<!DOCTYPE html>
<html lang="es">
<head>
    <?= headerAdmin($data); ?>
    <!-- Footable CSS -->
    <link href="<?= media(); ?>css/catalogoImm.css" rel="stylesheet">
</head>
<body class="fix-header card-no-border mini-sidebar">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card recursos">
                            <div class="card-body">
                                <div class="d-flex flex-wrap">
                                    <div class="col-12 col-sm-5">
                                        <h4 class="card-title"><?= $data['tag_title']; ?></h4>
                                        <h6 class="card-subtitle"><?= $data['tag_text']; ?></h6>
                                    </div>
                                    <div class="ml-auto col-12 col-sm-7">
                                        <div class="row">
                                            <div class="btn-group col-12 col-md-6">
                                                <button type="button" class="col-4 btn btn-outline-info btn-xs  popover-version" data-container="body" title="" data-toggle="popover" data-html="true"  data-placement="bottom">
                                                    Version
                                                </button>
                                                <button type="button" class="col-4 btn btn-outline-info btn-xs  popover-retos" data-container="body" title="" data-toggle="popover" data-html="true"  data-placement="bottom">
                                                    Retos
                                                </button>
                                                <button type="button" class="col-4 btn btn-outline-info btn-xs  popover-grados" data-container="body" title="" data-toggle="popover" data-html="true"  data-placement="bottom">
                                                    Grados
                                                </button>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <input type="text" class="form-control buscador" id="uname" placeholder="Buscar">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row content">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>js/catalogoImm.js"></script>
</body>
</html>