<!DOCTYPE html>
<html lang="en">
<head>
    <?= headerAdmin($data); ?>
    <link rel="stylesheet" href="<?= media(); ?>css/home.css">
    <link href="<?= media(); ?>plugins/toast-master/css/jquery.toast.css  " rel="stylesheet">
</head>
<body class="fix-header card-no-border mini-sidebar">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Versiones de app Immersive</h4>
                                <select name="pais" id="pais" class="form-control">
                                    <option value="1">México</option>
                                    <option value="2">Guatemala</option>
                                    <option value="3">Colombia</option>
                                    <option value="4">Perú</option>
                                    <option value="5">El Salvador</option>
                                    <option value="6">Hoduras</option>
                                    <option value="7">Costa Rica</option>
                                </select>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover" id="requisiciones">
                                        <thead>
                                            <tr>
                                                <!-- <th class="d-none d-sm-none d-md-block"><h6>#</h6></th> -->
                                                <th class=""><h6>Version</h6></th>
                                                <th class="d-none d-sm-none d-md-block "><h6>Descripción</h6></th>
                                                <th class=""><h6></h6></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <!-- <th>1</th> -->
                                                <form action="/catalogoImm/saveVersion" method="post" type="submit" id="formVersion">
                                                    <th><input type="text" id="txtVersion" placeholder="0.0.0" class="form-control form-control-line" name="version" maxlength="8" required></th>
                                                    <th><input type="text" id="txtDescripcion" placeholder="Escribir descripción" class="form-control form-control-line" name="descripcion" maxlength="100" required></th>
                                                    <th><button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Guardar</button></th>
                                                </form>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>js/recursosIMM/version.js"></script>
    <script src="<?= media(); ?>plugins/toast-master/js/jquery.toast.js"></script>
</body>
</html>