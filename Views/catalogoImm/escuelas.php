<!DOCTYPE html>
<html lang="en">
<head>
    <?= headerAdmin($data); ?>
    <!-- Footable CSS -->
    <link href="<?= media(); ?>css/catalogoImm.css" rel="stylesheet">
</head>
<body class="fix-header card-no-border mini-sidebar">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> 
                                    <h4 class="card-title m-t-10">Nombre de la escuela</h4>
                                    <h6 class="card-subtitle">Pais de la escuela</h6>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-people"></i> <font class="font-medium">254 usuarios</font></a></div>
                                        <div class="col-4"><a href="javascript:void(0)" class="link"><i class="icon-picture"></i> <font class="font-medium">54 recursos</font></a></div>
                                    </div>
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-body"> 
                                <small class="text-muted">Grafica de aprovechamiento por reto </small>
                                    <h6>hannagover@gmail.com</h6> 
                                <small class="text-muted p-t-30 db">Phone</small>
                                    <h6>+91 654 784 547</h6> 
                                <small class="text-muted p-t-30 db">Address</small>
                                    <h6>71 Pilgrim Avenue Chevy Chase, MD 20815</h6>
                                <small class="text-muted p-t-30 db">Social Profile</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>js/catalogoImm.js"></script>
</body>
</html>