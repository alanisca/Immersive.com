<!DOCTYPE html>
<html lang="en">
<head>
    <?= headerAdmin($data); ?>
    <link rel="stylesheet" href="<?= media(); ?>css/home.css">
    <link rel="stylesheet" href="<?= media(); ?>plugins/chartist-js/dist/chartist.min.css">
    <link rel="stylesheet" href="<?= media(); ?>css/plugins/chartist-js/dist/chartist-init.css">
    <link href="<?= media(); ?>plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
</head>
<body class="fix-header card-no-border mini-sidebar" id_kn_usuario="<?= $data["id_kn_usuario"] ?>">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <h3 id="usuario">Nombre del usuario</h3>
                                <h4 id="nombre">Nombre del usuario</h4>
                                <label class="label label-rounded label-success" id="escuela">Escuela</label>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <h1>20</h1>
                                            <h6 class="text-muted">Recursos detonados</h6>
                                        </div>
                                        <div class="col-6">
                                            <h1>99</h1>
                                            <h6 class="text-muted">Registro</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- <div class="row"> -->
                                <!-- <div class="col-lg-4 col-md-4"> -->
                                    <div class="card-body">
                                        <h3>Por Reto</h3>
                                        <h6 class="card-subtitle m-b-0">Recursos detonador por reto</h6>
                                    </div>
                                <!-- </div> -->
                            <!-- </div> -->
                            <div>
                                <div class="col-lg-12 col-md-12 align-self-center">
                                    <div class="m-t-0"></div>
                                    <canvas id="myChart"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <select class="custom-select pull-right">
                                    <option selected="">Periodo</option>
                                    <option value="1">2021</option>
                                    <option value="2">2022</option>
                                    <option value="3">2023</option>
                                </select>
                                <h4 class="card-title">Registros del usuario</h4>
                                <div class="table-responsive m-t-20" id="registrosTable">
                                    <table class="table stylish-table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Recurso</th>
                                                <th>Dispositivo</th>
                                                <th>Fecha</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- <tr>
                                                <td style="width:50px;"><span class="round" id="reg-id_recurso">S</span></td>
                                                <td>
                                                    <h6 id="reg-recurso">Sunil Joshi</h6><small class="text-muted" id="reg-unityTarget">Web Designer</small>
                                                </td>
                                                <td>
                                                    <h6 id="reg-dispositivo-ios">Sunil Joshi</h6><small class="text-muted" id="reg-version_app">Web Designer</small>
                                                </td>
                                                <td id="reg-fecha_registro">$3.9K</td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>plugins/toast-master/js/jquery.toast.js"></script>
    <script src="<?= media(); ?>plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="<?= media(); ?>plugins/chartjs/chartjs.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="<?= media(); ?>js/usuarios/usuario.js"></script>
</body>
</html>