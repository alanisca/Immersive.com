<?php 
    class escuelas extends Controllers{
        public function __construct(){
            parent::__construct();
            /* Valida si ya ha iniciado sesión y lo redirige a HOME */
            define('DURACION_SESION','86400');//24 horas
            ini_set("session.cookie_lifetime",DURACION_SESION);
            ini_set("session.gc_maxlifetime",DURACION_SESION); 
            session_cache_expire(DURACION_SESION);
            session_start();
            session_regenerate_id(true);
            // session_start();
            if(!isset($_SESSION['acceso_IMM']) || empty($_SESSION['acceso_IMM']) || $_SESSION['acceso_IMM'] != 1)
                header("Location:".base_url()."error");
            if(!isset($_SESSION['access_token']))
                header("Location:".base_url()."login");
        }
        public function usuarios(){
            $data['tag_page']="Usuarios";
            $data['tag_title']="Usuarios";
            $data['tag_name']="IMMERSIVE - Usuarios";
            $data['tag_text']="Catálogo de usuarios con total de recursos usados";
            $this->views->getView($this,"escuelas",$data);
        }
        public function usuario($id_usuario){
            $data['tag_page']  = "Escuela";
            $data['tag_title'] = "Escuela";
            $data['tag_name']  = "IMMERSIVE - Escuela";
            $data['tag_text']  = "información de la escuela";
            $data['id_escuela'] = $id_escuela;
            $this->views->getView($this,"escuela",$data);
        }
        public function getUsuario($id_usuario){
            $arrData = array();
            array_push($arrData,
                $this->model->getEscuela($id_escuela);
            );
            echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        }
?>