<!DOCTYPE html>
<html lang="en">
<head>
    <?= headerAdmin($data); ?>
    <!-- Footable CSS -->
    <link href="<?= media(); ?>plugins/footable/css/footable.core.css" rel="stylesheet">
    <link href="<?= media(); ?>plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
</head>
<body class="fix-header card-no-border mini-sidebar">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><?= $data['tag_title']; ?></h4>
                                <h6 class="card-subtitle"><?= $data['tag_text']; ?></h6>
                                <div class="table-responsive">
                                    <table id="table-users" class="table table-hover footable-loaded footable default breakpoint" data-paging="true" >
                                        <thead>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <div id="edit-user" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <form action="" name="editUser" id="editUser">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h4 class="modal-title" id="myModalLabel">Editar usuario</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="m-t-30 text-center"> <img src="https://lh3.googleusercontent.com/a-/AOh14Gh69QfHzmKjGX4GFpbJsnpGTe9uJzWA3AftWA_p=s96-c" id="imgUser" class="img-circle" width="150">
                                                                        <h4 class="card-title m-t-10" id="userName">Juan Raul Sánchez Villa</h4>
                                                                        <h6 class="card-subtitle" id="userMail">juan.sanchez@knotion.com</h6>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <input type="text" name="id_user" id="id_user" style="display:none;" >
                                                                                <div class="form-group has-success">
                                                                                    <label class="control-label">Acceso a IMM</label>
                                                                                    <select class="form-control custom-select" id="accesoImm" name="accesoImm">
                                                                                        <option value="0">Denegado</option>
                                                                                        <option value="1">Permitido</option>
                                                                                    </select>
                                                                                    <small class="form-control-feedback"> ¿Con acceso a Immersive? </small>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group has-success">
                                                                                    <label class="control-label">Acceso a Interactivos</label>
                                                                                    <select class="form-control custom-select" id="accesoInt" name="accesoInt">
                                                                                        <option value="0">Denegado</option>
                                                                                        <option value="1">Permitido</option>
                                                                                    </select>
                                                                                    <small class="form-control-feedback"> ¿Con acceso a Interactivos? </small>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group has-success">
                                                                                    <label class="control-label">Tipo de usuario</label>
                                                                                    <select class="form-control custom-select" id="tipoUsuario" name="tipoUsuario">
                                                                                        <option value="Invitado">Invitado</option>
                                                                                        <option value="Administrador">Administrador</option>
                                                                                        <option value="Productor">Productor</option>
                                                                                        <option value="Lider">Lider</option>
                                                                                        <option value="Arte">Arte</option>
                                                                                        <option value="Programador">Programador</option>
                                                                                    </select>
                                                                                    <small class="form-control-feedback"> Selecciona tipo de usuario </small>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-info waves-effect" >Guardar</button>
                                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <!-- /.modal-content -->
                                                    </div>
                                                    <!-- /.modal-dialog -->
                                                </div>
                                                <td colspan="7">
                                                    <div class="text-right">
                                                        <ul class="pagination"> </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>plugins/footable/js/footable.min.js" type="text/javascript"></script>
    <script src="<?= media(); ?>plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <!--FooTable init-->
    <script src="<?= media(); ?>js/users/users.js"></script>
</body>
</html>