<!DOCTYPE html>
<html lang="en">
<head>
    <?= headerAdmin($data); ?>
    <link rel="stylesheet" href="<?= media(); ?>css/home.css">
    <link rel="stylesheet" href="<?= media(); ?>plugins/chartist-js/dist/chartist.min.css">
    <link rel="stylesheet" href="<?= media(); ?>css/plugins/chartist-js/dist/chartist-init.css">
    <link href="<?= media(); ?>plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
</head>
<body class="fix-header card-no-border mini-sidebar" id_escuela="<?= $data["id_escuela"] ?>">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="card-body">
                                        <h3 id="escuela">Nombe de la escuela</h3>
                                        <h6 class="card-subtitle m-b-0" id="pais">México</h6>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 b-r align-self-center">
                                    <div class="card-body">
                                        <div class="d-flex flex-row">
                                            <div class="col-8 p-0 align-self-center">
                                                <h3 class="m-b-0" id="tot_alumnos">-</h3>
                                                <h5 class="text-muted m-b-0">Alumnos</h5> </div>
                                            <div class="col-4 text-right">
                                                <div class="round align-self-center round-success"><i class="mdi mdi-account"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 align-self-center">
                                    <div class="card-body">
                                        <div class="d-flex flex-row">
                                            <div class="col-8 p-0 align-self-center">
                                                <h3 class="m-b-0" id="tot_recursos">-</h3>
                                                <h5 class="text-muted m-b-0">Recursos</h5>
                                            </div>
                                            <div class="col-4 text-right">
                                                <div class="round align-self-center bg-inverse"><i class="mdi mdi-binoculars"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 align-self-center">
                                    <div class="card-body row">
                                        <div class="col-md-3">
                                            <select class="form-control custom-select" id="select-periodos">
                                                <option value="">Todos los periodos</option>
                                                <option value="3">MX 2020 - 2021</option>
                                                <option value="1">MX 2021 - 2022</option>
                                                <option value="2">MX 2022 - 2023</option>
                                            </select>
                                            <small class="form-control-feedback">Periodos </small>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control custom-select" id="select-retos">
                                                <option value="">Todos los retos</option>
                                                <option value="1">Reto 1</option>
                                                <option value="2">Reto 2</option>
                                                <option value="3">Reto 3</option>
                                                <option value="4">Reto 4</option>
                                                <option value="5">Reto 5</option>
                                                <option value="6">Reto 6</option>
                                                <option value="7">Reto 7</option>
                                                <option value="8">Reto 8</option>
                                            </select>
                                            <small class="form-control-feedback">Retos </small>
                                        </div>
                                        <div class="col-md-3">
                                            <select class="form-control custom-select" id="select-grados">
                                                <option value="">Todos los grados</option>
                                                <option value="K">KINDER</option>
                                                <option value="1">K1</option>
                                                <option value="2">K2</option>
                                                <option value="3">K3</option>
                                                <option value="3">PF</option>
                                                <option value="4">PRIMARIA BAJA</option>
                                                <option value="4">E1</option>
                                                <option value="5">E2</option>
                                                <option value="6">E3</option>
                                                <option value="PA">PRIMARIA ALTA</option>
                                                <option value="7">E4</option>
                                                <option value="8">E5</option>
                                                <option value="8">E6</option>
                                                <option value="M">SECUNDARIA</option>
                                                <option value="M7">M7</option>
                                                <option value="M8">M8</option>
                                                <option value="M9">M9</option>
                                            </select>
                                            <small class="form-control-feedback">Grados </small>
                                        </div>
                                        <div class="col-md-3">
                                            <button id="btn-filtrar" type="button" class="btn waves-effect waves-light btn-block btn-info">Filtrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="m-t-0"></div>
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="card">
                            <div class="m-t-0"></div>
                            <canvas id="myChartGrado"></canvas>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Versión</h3>
                                <h6 class="card-subtitle">Versión de la app de Immersive</h6>
                                <canvas id="versionApp"></canvas>
                            </div>
                            <div>
                                <hr class="m-t-0 m-b-0">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Dispositivos</h3>
                                <h6 class="card-subtitle">Dispositivos utilizados</h6>
                                <canvas id="DispositivosApp"></canvas>
                            </div>
                            <div>
                                <hr class="m-t-0 m-b-0">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">iOS</h3>
                                <h6 class="card-subtitle">Versión de iOS utilizada en los Dispositivos</h6>
                                <canvas id="iosApp"></canvas>
                            </div>
                            <div>
                                <hr class="m-t-0 m-b-0">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card card_alumnos">
                            <div class="card-header">
                                <h4 class="card-title">¡Alumnos!</h4>
                                <h6 class="card-subtitle">Alumnos con recursos detonados por grado</h6>
                                <div class="ml-auto">
                                    <ul class="list-inline">
                                        <li>
                                            <h6 class="text-muted text-success"><i class="fa fa-circle font-10 m-r-10 "></i>Usuarios</h6> </li>
                                        <li>
                                            <h6 class="text-muted  text-danger"><i class="fa fa-circle font-10 m-r-10"></i>Maestros</h6> </li>
                                        <li>
                                            <h6 class="text-muted  text-secondary"><i class="fa fa-circle font-10 m-r-10"></i>otros</h6> </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="sl-item">
                                    <div class="sl-right" id="u_k1">
                                        <div>
                                            <a href="#" class="link">K1 - Prescolar 1</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" ></div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_k2">
                                        <div>
                                            <a href="#" class="link">K2: Prescolar 2</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_k3">
                                        <div>
                                            <a href="#" class="link">K3: Prescolar 3</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_pf">
                                        <div>
                                            <a href="#" class="link">PF: Prefirst</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_e1">
                                        <div>
                                            <a href="#" class="link">E1: 1º de Primaria</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_e2">
                                        <div>
                                            <a href="#" class="link">E2: 2º de Primaria</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_e3">
                                        <div>
                                            <a href="#" class="link">E3: 3º de Primaria</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_e4">
                                        <div>
                                            <a href="#" class="link">E4: 4º de Primaria</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_e5">
                                        <div>
                                            <a href="#" class="link">E5: 5º de Primaria</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_e6">
                                        <div>
                                            <a href="#" class="link">E6: 6º de Primaria</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_m7">
                                        <div>
                                            <a href="#" class="link">M7: 7º de Secundaria</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_m8">
                                        <div>
                                            <a href="#" class="link">M8: 8º de Secundaria</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_m9">
                                        <div>
                                            <a href="#" class="link">M9: 9º de Secundaria</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="sl-right" id="u_Otros">
                                        <div>
                                            <a href="#" class="link">Sin grado asignado</a> <span class="sl-date"></span>
                                            <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card card_recursos">
                            <div class="card-header">
                                <h4 class="card-title">¡Recursos!</h4>
                                <h6 class="card-subtitle">Recursos por reto y por grado</h6>
                            </div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#reto1" role="tab">Reto 1</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#reto2" role="tab">Reto 2</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#reto3" role="tab">Reto 3</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#reto4" role="tab">Reto 4</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#reto5" role="tab">Reto 5</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#reto6" role="tab">Reto 6</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#reto7" role="tab">Reto 7</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#reto8" role="tab">Reto 8</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#reto0" role="tab">General</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="reto1" role="tabpanel">
                                    <div class="card-body">
                                        <div class="sl-item">
                                            <div class="sl-right" id="r_k1">
                                                <div>
                                                    <a href="#" class="link">K1 - Prescolar 1</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" ></div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k2">
                                                <div>
                                                    <a href="#" class="link">K2: Prescolar 2</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k3">
                                                <div>
                                                    <a href="#" class="link">K3: Prescolar 3</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_pf">
                                                <div>
                                                    <a href="#" class="link">PF: Prefirst</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e1">
                                                <div>
                                                    <a href="#" class="link">E1: 1º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e2">
                                                <div>
                                                    <a href="#" class="link">E2: 2º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e3">
                                                <div>
                                                    <a href="#" class="link">E3: 3º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e4">
                                                <div>
                                                    <a href="#" class="link">E4: 4º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e5">
                                                <div>
                                                    <a href="#" class="link">E5: 5º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e6">
                                                <div>
                                                    <a href="#" class="link">E6: 6º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m7">
                                                <div>
                                                    <a href="#" class="link">M7: 7º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m8">
                                                <div>
                                                    <a href="#" class="link">M8: 8º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m9">
                                                <div>
                                                    <a href="#" class="link">M9: 9º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_Otros">
                                                <div>
                                                    <a href="#" class="link">Sin grado asignado</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="reto2" role="tabpanel">
                                    <div class="card-body">
                                        <div class="sl-item">
                                            <div class="sl-right" id="r_k1">
                                                <div>
                                                    <a href="#" class="link">K1 - Prescolar 1</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" ></div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k2">
                                                <div>
                                                    <a href="#" class="link">K2: Prescolar 2</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k3">
                                                <div>
                                                    <a href="#" class="link">K3: Prescolar 3</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_pf">
                                                <div>
                                                    <a href="#" class="link">PF: Prefirst</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e1">
                                                <div>
                                                    <a href="#" class="link">E1: 1º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e2">
                                                <div>
                                                    <a href="#" class="link">E2: 2º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e3">
                                                <div>
                                                    <a href="#" class="link">E3: 3º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e4">
                                                <div>
                                                    <a href="#" class="link">E4: 4º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e5">
                                                <div>
                                                    <a href="#" class="link">E5: 5º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e6">
                                                <div>
                                                    <a href="#" class="link">E6: 6º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m7">
                                                <div>
                                                    <a href="#" class="link">M7: 7º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m8">
                                                <div>
                                                    <a href="#" class="link">M8: 8º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m9">
                                                <div>
                                                    <a href="#" class="link">M9: 9º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_Otros">
                                                <div>
                                                    <a href="#" class="link">Sin grado asignado</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="reto3" role="tabpanel">
                                    <div class="card-body">
                                        <div class="sl-item">
                                            <div class="sl-right" id="r_k1">
                                                <div>
                                                    <a href="#" class="link">K1 - Prescolar 1</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" ></div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k2">
                                                <div>
                                                    <a href="#" class="link">K2: Prescolar 2</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k3">
                                                <div>
                                                    <a href="#" class="link">K3: Prescolar 3</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_pf">
                                                <div>
                                                    <a href="#" class="link">PF: Prefirst</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e1">
                                                <div>
                                                    <a href="#" class="link">E1: 1º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e2">
                                                <div>
                                                    <a href="#" class="link">E2: 2º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e3">
                                                <div>
                                                    <a href="#" class="link">E3: 3º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e4">
                                                <div>
                                                    <a href="#" class="link">E4: 4º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e5">
                                                <div>
                                                    <a href="#" class="link">E5: 5º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e6">
                                                <div>
                                                    <a href="#" class="link">E6: 6º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m7">
                                                <div>
                                                    <a href="#" class="link">M7: 7º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m8">
                                                <div>
                                                    <a href="#" class="link">M8: 8º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m9">
                                                <div>
                                                    <a href="#" class="link">M9: 9º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_Otros">
                                                <div>
                                                    <a href="#" class="link">Sin grado asignado</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="reto4" role="tabpanel">
                                    <div class="card-body">
                                        <div class="sl-item">
                                            <div class="sl-right" id="r_k1">
                                                <div>
                                                    <a href="#" class="link">K1 - Prescolar 1</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" ></div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k2">
                                                <div>
                                                    <a href="#" class="link">K2: Prescolar 2</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k3">
                                                <div>
                                                    <a href="#" class="link">K3: Prescolar 3</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_pf">
                                                <div>
                                                    <a href="#" class="link">PF: Prefirst</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e1">
                                                <div>
                                                    <a href="#" class="link">E1: 1º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e2">
                                                <div>
                                                    <a href="#" class="link">E2: 2º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e3">
                                                <div>
                                                    <a href="#" class="link">E3: 3º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e4">
                                                <div>
                                                    <a href="#" class="link">E4: 4º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e5">
                                                <div>
                                                    <a href="#" class="link">E5: 5º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e6">
                                                <div>
                                                    <a href="#" class="link">E6: 6º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m7">
                                                <div>
                                                    <a href="#" class="link">M7: 7º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m8">
                                                <div>
                                                    <a href="#" class="link">M8: 8º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m9">
                                                <div>
                                                    <a href="#" class="link">M9: 9º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_Otros">
                                                <div>
                                                    <a href="#" class="link">Sin grado asignado</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="reto5" role="tabpanel">
                                    <div class="card-body">
                                        <div class="sl-item">
                                            <div class="sl-right" id="r_k1">
                                                <div>
                                                    <a href="#" class="link">K1 - Prescolar 1</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" ></div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k2">
                                                <div>
                                                    <a href="#" class="link">K2: Prescolar 2</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k3">
                                                <div>
                                                    <a href="#" class="link">K3: Prescolar 3</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_pf">
                                                <div>
                                                    <a href="#" class="link">PF: Prefirst</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e1">
                                                <div>
                                                    <a href="#" class="link">E1: 1º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e2">
                                                <div>
                                                    <a href="#" class="link">E2: 2º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e3">
                                                <div>
                                                    <a href="#" class="link">E3: 3º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e4">
                                                <div>
                                                    <a href="#" class="link">E4: 4º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e5">
                                                <div>
                                                    <a href="#" class="link">E5: 5º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e6">
                                                <div>
                                                    <a href="#" class="link">E6: 6º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m7">
                                                <div>
                                                    <a href="#" class="link">M7: 7º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m8">
                                                <div>
                                                    <a href="#" class="link">M8: 8º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m9">
                                                <div>
                                                    <a href="#" class="link">M9: 9º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_Otros">
                                                <div>
                                                    <a href="#" class="link">Sin grado asignado</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="reto6" role="tabpanel">
                                    <div class="card-body">
                                        <div class="sl-item">
                                            <div class="sl-right" id="r_k1">
                                                <div>
                                                    <a href="#" class="link">K1 - Prescolar 1</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" ></div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k2">
                                                <div>
                                                    <a href="#" class="link">K2: Prescolar 2</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k3">
                                                <div>
                                                    <a href="#" class="link">K3: Prescolar 3</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_pf">
                                                <div>
                                                    <a href="#" class="link">PF: Prefirst</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e1">
                                                <div>
                                                    <a href="#" class="link">E1: 1º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e2">
                                                <div>
                                                    <a href="#" class="link">E2: 2º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e3">
                                                <div>
                                                    <a href="#" class="link">E3: 3º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e4">
                                                <div>
                                                    <a href="#" class="link">E4: 4º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e5">
                                                <div>
                                                    <a href="#" class="link">E5: 5º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e6">
                                                <div>
                                                    <a href="#" class="link">E6: 6º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m7">
                                                <div>
                                                    <a href="#" class="link">M7: 7º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m8">
                                                <div>
                                                    <a href="#" class="link">M8: 8º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m9">
                                                <div>
                                                    <a href="#" class="link">M9: 9º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_Otros">
                                                <div>
                                                    <a href="#" class="link">Sin grado asignado</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="reto7" role="tabpanel">
                                    <div class="card-body">
                                        <div class="sl-item">
                                            <div class="sl-right" id="r_k1">
                                                <div>
                                                    <a href="#" class="link">K1 - Prescolar 1</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" ></div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k2">
                                                <div>
                                                    <a href="#" class="link">K2: Prescolar 2</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k3">
                                                <div>
                                                    <a href="#" class="link">K3: Prescolar 3</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_pf">
                                                <div>
                                                    <a href="#" class="link">PF: Prefirst</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e1">
                                                <div>
                                                    <a href="#" class="link">E1: 1º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e2">
                                                <div>
                                                    <a href="#" class="link">E2: 2º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e3">
                                                <div>
                                                    <a href="#" class="link">E3: 3º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e4">
                                                <div>
                                                    <a href="#" class="link">E4: 4º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e5">
                                                <div>
                                                    <a href="#" class="link">E5: 5º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e6">
                                                <div>
                                                    <a href="#" class="link">E6: 6º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m7">
                                                <div>
                                                    <a href="#" class="link">M7: 7º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m8">
                                                <div>
                                                    <a href="#" class="link">M8: 8º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m9">
                                                <div>
                                                    <a href="#" class="link">M9: 9º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_Otros">
                                                <div>
                                                    <a href="#" class="link">Sin grado asignado</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="reto8" role="tabpanel">
                                    <div class="card-body">
                                        <div class="sl-item">
                                            <div class="sl-right" id="r_k1">
                                                <div>
                                                    <a href="#" class="link">K1 - Prescolar 1</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" ></div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k2">
                                                <div>
                                                    <a href="#" class="link">K2: Prescolar 2</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k3">
                                                <div>
                                                    <a href="#" class="link">K3: Prescolar 3</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_pf">
                                                <div>
                                                    <a href="#" class="link">PF: Prefirst</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e1">
                                                <div>
                                                    <a href="#" class="link">E1: 1º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e2">
                                                <div>
                                                    <a href="#" class="link">E2: 2º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e3">
                                                <div>
                                                    <a href="#" class="link">E3: 3º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e4">
                                                <div>
                                                    <a href="#" class="link">E4: 4º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e5">
                                                <div>
                                                    <a href="#" class="link">E5: 5º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e6">
                                                <div>
                                                    <a href="#" class="link">E6: 6º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m7">
                                                <div>
                                                    <a href="#" class="link">M7: 7º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m8">
                                                <div>
                                                    <a href="#" class="link">M8: 8º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m9">
                                                <div>
                                                    <a href="#" class="link">M9: 9º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_Otros">
                                                <div>
                                                    <a href="#" class="link">Sin grado asignado</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="reto0" role="tabpanel">
                                    <div class="card-body">
                                        <div class="sl-item">
                                            <div class="sl-right" id="r_k1">
                                                <div>
                                                    <a href="#" class="link">K1 - Prescolar 1</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" ></div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k2">
                                                <div>
                                                    <a href="#" class="link">K2: Prescolar 2</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_k3">
                                                <div>
                                                    <a href="#" class="link">K3: Prescolar 3</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_pf">
                                                <div>
                                                    <a href="#" class="link">PF: Prefirst</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e1">
                                                <div>
                                                    <a href="#" class="link">E1: 1º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e2">
                                                <div>
                                                    <a href="#" class="link">E2: 2º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e3">
                                                <div>
                                                    <a href="#" class="link">E3: 3º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e4">
                                                <div>
                                                    <a href="#" class="link">E4: 4º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e5">
                                                <div>
                                                    <a href="#" class="link">E5: 5º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_e6">
                                                <div>
                                                    <a href="#" class="link">E6: 6º de Primaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m7">
                                                <div>
                                                    <a href="#" class="link">M7: 7º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m8">
                                                <div>
                                                    <a href="#" class="link">M8: 8º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_m9">
                                                <div>
                                                    <a href="#" class="link">M9: 9º de Secundaria</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-right" id="r_Otros">
                                                <div>
                                                    <a href="#" class="link">Sin grado asignado</a> <span class="sl-date"></span>
                                                    <div class="row usuarios" style="overflow: scroll; width: 100%; flex-wrap: initial;" > </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>plugins/toast-master/js/jquery.toast.js"></script>
    <script src="<?= media(); ?>plugins/chartist-js/dist/chartist.min.js"></script>
    <script src="<?= media(); ?>plugins/chartjs/chartjs.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@2.0.0"></script>
    <script src="<?= media(); ?>js/escuelas/escuela.js"></script>
</body>
</html>