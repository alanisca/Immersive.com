<!DOCTYPE html>
<html lang="es">
<head>
    <?= headerAdmin($data); ?>
    <!-- Footable CSS -->
    <link href="<?= media(); ?>plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
    <link href="<?= media(); ?>plugins/footable/css/footable.bootstrap.css" rel="stylesheet">
    <link href="<?= media(); ?>css/fonts/fontawesome.min.css" rel="stylesheet">
    <!-- <link href="<?= media(); ?>css/catalogoImm.css" rel="stylesheet"> -->
</head>
<body class="fix-header card-no-border mini-sidebar">
    <?= preloadAdmin(); ?>
    <div id="main-wrapper">
        <?= topBarAdmin(); ?>
        <?= navAdmin($data); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card recursos">
                            <div class="card-body">
                                <input class="custom-select pull-right" type="text" placeholder="Filtrar escuela" id="escuelasFilter">
                                <h4 class="card-title"><?= $data['tag_title']; ?></h4>
                                <h6 class="card-subtitle"><?= $data['tag_text']; ?></h6>
                                <div class="row content">
                                <table id="escuelas-foo-addrow" class="table m-t-30 table-hover contact-list footable-loaded footable" data-sorting="true">
                                        <thead>
                                            <tr>
                                                <th class="footable-sortable" data-type="number">#</th>
                                                <th class="footable-sortable" data-type="text">Escuela</th>
                                                <th class="footable-sortable" data-type="number">1</th>
                                                <th class="footable-sortable" data-type="number">2</th>
                                                <th class="footable-sortable" data-type="number">3</th>
                                                <th class="footable-sortable" data-type="number">4</th>
                                                <th class="footable-sortable" data-type="number">5</th>
                                                <th class="footable-sortable" data-type="number">6</th>
                                                <th class="footable-sortable" data-type="number">7</th>
                                                <th class="footable-sortable" data-type="number">8</th>
                                                <th class="footable-sortable" data-type="number">Multigrado</th>
                                                <th class="footable-sortable" data-type="number">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2">
                                                    <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-contact">Add New Contact</button>
                                                </td>
                                                
                                                <td colspan="7">
                                                    <div class="text-right">
                                                        <ul class="pagination"> <li class="footable-page-arrow disabled"><a data-page="first" href="#first">«</a></li><li class="footable-page-arrow disabled"><a data-page="prev" href="#prev">‹</a></li><li class="footable-page active"><a data-page="0" href="#">1</a></li><li class="footable-page"><a data-page="1" href="#">2</a></li><li class="footable-page-arrow"><a data-page="next" href="#next">›</a></li><li class="footable-page-arrow"><a data-page="last" href="#last">»</a></li></ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= footerAdmin($data); ?>
    <script src="<?= media(); ?>plugins/toast-master/js/jquery.toast.js"></script>
    <script src="<?= media(); ?>plugins/footable/js/moment.js"></script>
    <script src="<?= media(); ?>plugins/footable/js/footable.min.js"></script>
    <script src="<?= media(); ?>js/escuelas/escuelas.js"></script>
</body>
</html>