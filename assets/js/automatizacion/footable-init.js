$(window).on('load', function() {
	/* Automatización */
	addLoad();
	$('#automatizacion-toggler').footable({
		// "expandFirst": true,
		"columns": [
			{"name":"ID_Recurso","title":"ID Recurso","style":{"min-width": "120px"}},
			{"name":"Recurso","title":"Nombre"},
			{"name":"Periodo","title":"Periodo","breakpoints": "all"},
			{"name":"Pathway","title":"Pathway","breakpoints": "all", "visible": false},
			{"name":"Grado","title":"Grado","breakpoints": "all"},
			{"name":"Reto","title":"Reto","breakpoints": "all"},
			{"name":"Sesion","title":"Sesion","breakpoints": "all", "visible": false},
			{"name":"Actividad","title":"Actividad","breakpoints": "all", "visible": false},
			{"name":"Paso","title":"Paso","breakpoints": "all", "visible": false},
			{"name":"Tipo","title":"Tipo"},
			{"name":"Version","title":"Version"},
			{"name":"ID_RB","title":"ID","formatter":function(value, options,rowdata){
				if(rowdata['descargado'] == '1')
				// 	return '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#add-contact" data-nomenclatura="'+rowdata['nomenclatura']+'" data-id="'+value+'"><i class="fa fa-edit"></i></button>';
					return '<a class="btn waves-effect waves-light btn-xs btn-warning" href="'+base_url+'automatizacion/ver/'+value+'" data-toggle="preview" target="_blank"><i class="fa fa-edit"></i></a>';
				else
				return '<a class="btn waves-effect waves-light btn-xs btn-info" href="'+base_url+'automatizacion/ver/'+value+'" data-toggle="preview" target="_blank"><i class="fa fa-edit"></i></a>';
				// return '<a class="btn waves-effect waves-light btn-xs btn-info" data-toggle="modal" target="_blank"><i class="fa fa-edit"></i></a>';
			}},
			{"name":"ID_RB", "visible": false,"title":"ID","formatter":function(value, options,rowdata){
				return '<a class="btn waves-effect waves-light btn-xs btn-info" href="'+value+'"><i class="fa fa-edit"></i></a>';
				// return '<button type="button" class="btn waves-effect waves-light btn-xs btn-info">Ver</button>'
			}},
			{"name":"Idioma","title":"Idioma","breakpoints": "all"},
			{"name":"Traduccion","title":"Traduccion","breakpoints": "all"},
			{"name":"Script_Link","title":"Script","breakpoints": "all","formatter": function(value, options, rowData){
				return '<a class="btn default btn-outline" href="'+value+'"><i class="fa fa-link"></i></a>';
			}},
			{"name":"RB_id","title":"id Resourcebank","breakpoints": "all"},
			{"name":"RB_Tipo","title":"RB Tipo","breakpoints": "all"},
			{"name":"RB_Link_Archivo","title":"Archivo","breakpoints": "all","formatter": function(value, options, rowData){
				return '<a class="btn default btn-outline" href="'+value+'"><i class="fa fa-cloud-download"></i></a>';
			}},
			{"name":"nomenclatura","breakpoints": "all", "visible": false},
			{"name":"descargado","title":"","breakpoints": "all", "visible": false},
			{"name":"digitalizado","title":"","breakpoints": "all", "visible": false}
		],
		"rows":$.getJSON( base_url+'automatizacion/getInteractives'),
		"on": {
            'postinit.ft.table': function(e, ft) {
                /*
                 * e: The jQuery.Event object for the event.
                 * ft: The instance of the plugin raising the event.
                 */
                // all initialized - do stuff here
				removeLoad();
			}
            }
	});
});