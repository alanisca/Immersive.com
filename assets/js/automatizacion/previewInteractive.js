var clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");
$(window).on('load', function() {
    addLoad();
    /* GENERALES */
    $("#codeName").html(codeName);
    if(sameImg) $("#samImg").attr('checked', true);
    if(encabezado) $("#encabezado").attr('checked', true);
    if(math) $("#math").attr('checked', true);

    if(!textArray[0].title) {
        console.log("no existe");
        var textArray2 = [...textArray];
        textArray = [];
        var incorrect = [];
        for (let index = 0; index < textArray2.length; index++) {
            incorrect = [];
            for (let index2 = 0; index2 < elementsByScreen; index2++) {
                incorrect.push(wrongWords[0]);
                wrongWords.shift();
            }
            textArray.push({
                title: textArray2[index],
                correct: correctWords[index],
                incorrect: [wrongWords[0],wrongWords[1]]
            });
        }
    }
    // else {
       
        /* PREGUNTAS */
        for (let index = 0; index < textArray.length; index++) {
            var incorrect = "";
            for (let index2 = 0; index2 < textArray[index].incorrect.length; index2++) {
                incorrect +='<tr><td style="width: 10px;"><i class="text-danger fa fa-times"></i></td><td> <h5>'+textArray[index].incorrect[index2]+'</h5><td><td><button type="button" class="edit-item btn btn-warning btn-xs" data-toggle="modal" data-target="#edit-text" data-type="incorrect" data-id="'+(index+1)+'" data-incorrect="'+(index2+1)+'"><i class="fa fa-edit" style="width: 10px;"></i></button></td></tr>';
            }
            $("#datos #preguntas").append('<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#paso'+index+'" role="tab">'+(index+1)+'</a> </li>');
            $("#datos #respuestas").append('<div class=" tab-pane" id="paso'+index+'" role="tabpanel"></div>');
            // $("#datos #respuestas #paso"+index).append(index);
            $("#datos #respuestas #paso"+index).append(''+
                                                '<div class="row" style="margin:0; padding-top:0; ">'+
                                                    '<div class="col-xs-12 col-sm-4 imgOption img-rounded" style="background-image: url('+base_url+"assets/interactives/original/"+codeName.toLowerCase()+"/img/widget/img"+((index+1) < 10 ? '0'+(index+1) : (index+1))+'@2x.jpg)" alt="user"></div>'+
                                                    '<table id="preview-interactive paso'+index+'" class="col-xs-12 col-sm-8 table table-hover automatizacion">'+
                                                        '<tr><td style="width: 10px;"><i class="text-primary fa fa-question"></i></td><td> <h4>'+textArray[index].title+'</4><td><td><button type="button" class="edit-item btn btn-warning btn-xs" data-toggle="modal" data-target="#edit-text" data-type="question" data-id="'+(index+1)+'"><i class="fa fa-edit" style="width: 10px;"></i></button></td></tr>'+
                                                        '<tr><td style="width: 10px;"><i class="text-success fa fa-check"></i></td><td> <h5>'+textArray[index].correct+'</h5><td><td><button type="button" class="edit-item btn btn-warning btn-xs" data-toggle="modal" data-target="#edit-text" data-type="correct" data-id="'+(index+1)+'"><i class="fa fa-edit" style="width: 10px;"></i></button></td></tr>'+
                                                        incorrect+
                                                    '</table>'+
                                                '</div>');
        }
        $("#datos #preguntas a").first().addClass("active");
        $("#datos #respuestas #paso0").addClass("active");
    
        // $("#preview iframe").attr("src",base_url+"assets/interactives/original/"+codeName+"/index_st.html");
        $('.edit-item').on(clickHandler,function(){
            console.log($(this).data("type"), $(this).data("id"));
            var text = "";
            var tipo = $(this).data("type");
            var index = $(this).data("id");
            switch (tipo) {
                case "question":
                    text = textArray[index-1].title;
                    break;
                case "correct":
                    text = textArray[index-1].correct;
                    break;
                case "incorrect":
                    text = textArray[index-1].incorrect[$(this).data("incorrect")-1];
                    break;
                default:
                    break;
            }
            $(".modal-body textarea.edit-control").val(text);
        });
    removeLoad();
    // }
});