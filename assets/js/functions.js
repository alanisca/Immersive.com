function isiPad(){
    var userAgent = window.navigator.userAgent;
    return (/iPad/).test(userAgent);
}
function getGrado(grado){
    var vGrade;
    switch (grado) {
        case '1st Grade Elementary School':
            vGrade = "E1";
            break;
        case '2nd Grade Elementary School':
            vGrade = "E2";
            break;
        case '3rd Grade Elementary School':
            vGrade = "E3";
            break;
        case '4th Grade Elementary School':
            vGrade = "E4";
            break;
        case '5th Grade Elementary School':
            vGrade = "E5";
            break;
        case '6th Grade Elementary School':
            vGrade = "E6";
            break;
        case '7th Grade Middle School':
            vGrade = "M7";
            break;
        case '8th Grade Middle School':
            vGrade = "M8";
            break;
        case '9th Grade Middle School':
            vGrade = "M9";
            break;
        case 'Kindergarten 1':
            vGrade = "K1";
            break;
        case 'Kindergarten 2':
            vGrade = "K2";
            break;
        case 'Kindergarten 3':
            vGrade = "K3";
            break;
        case 'Pre-First':
            vGrade = "PF";
            break;
        default:
            break;
    }
    return vGrade;
}
var arrload = [];
function addLoad(){
    console.log("addLoad");
    $(".preloader").fadeIn();
    arrload.push(false)
}
function removeLoad(){
    console.log("removeLoad");
    arrload.pop();
    loadedPage();
}
function loadedPage(){
    console.log("loadedPage");
    var loaded = true;
    $.each(arrload, function(key,val){
        if(!arrload[key]) loaded = false;
    });
    if(loaded) $(".preloader").fadeOut(2000);
}
function getSrcImg(grado, idioma, tipo){
    var ready = true;
    var vtipo = "";
    var vidioma = "";
    var imgTH = "";
    switch (tipo) {
        case "IMM Prime":
            vtipo = "Prime.jpg"
            break;
        case "IMM Augmented Reality":
            vtipo = "RealidadVirtual.jpg"
            break;
        case "IMM Interactive":
            vtipo = "Interactivo.jpg"
            break;
        case "IMM Virtual Reality":
            vtipo = "RealidadVirtual.jpg"
            break;
        default:
            ready = false;
            break;
    }
    switch (idioma) {
        case "EN":
            vidioma = "EN"
            break;
        case "ES":
            vidioma = "ES"
            break;
        default:
            ready = false;
            break;
    }
    switch (grado) {
        case '1st Grade Elementary School':
        case 'E1':
            imgTH +="PB/"+vidioma+"/"+vtipo;
            break;
        case '2nd Grade Elementary School':
        case 'E2':
            imgTH +="PB/"+vidioma+"/"+vtipo;
            break;
        case '3rd Grade Elementary School':
        case 'E3':
            imgTH +="PB/"+vidioma+"/"+vtipo;
            break;
        case '4th Grade Elementary School':
        case 'E4':
            imgTH +="PA/"+vidioma+"/"+vtipo;
            break;
        case '5th Grade Elementary School':
        case 'E5':
            imgTH +="PA/"+vidioma+"/"+vtipo;
            break;
        case '6th Grade Elementary School':
        case 'E6':
            imgTH +="PA/"+vidioma+"/"+vtipo;
            break;
        case '7th Grade Middle School':
        case 'M7':
            imgTH +="SEC/"+vidioma+"/"+vtipo;
            break;
        case '8th Grade Middle School':
        case 'M8':
            imgTH +="SEC/"+vidioma+"/"+vtipo;
            break;
        case '9th Grade Middle School':
        case 'M9':
            imgTH +="SEC/"+vidioma+"/"+vtipo;
            break;
        case 'Kindergarten 1':
        case 'K1':
            imgTH +="K/"+vidioma+"/"+vtipo;
            break;
        case 'Kindergarten 2':
        case 'K2':
            imgTH +="K/"+vidioma+"/"+vtipo;
            break;
        case 'Kindergarten 3':
        case 'K3':
            imgTH +="K/"+vidioma+"/"+vtipo;
            break;
        case 'Pre-First':
        case 'PF':
            imgTH +="K/"+vidioma+"/"+vtipo;
            break;
        default:
            break;
    }
    return {ready:ready,liga:base_url+"assets/IMMTH/"+imgTH};
}