var objData;

document.addEventListener('DOMContentLoaded', function(){
    function getReqReto(){
        var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        var ajaxUrl  = base_url+'catalogoImm/getRequisiciones?'+filterReq();

        request.open("GET",ajaxUrl,true);
        request.send();
        addLoad();

        request.onreadystatechange = function(){
            if(request.readyState == 4 && request.status == 200){
                removeLoad();
                objData = JSON.parse(request.responseText);
                if(objData.status){
                    $("table#requisiciones tbody").html("");
                    $.each(objData.data, function(key,value) {
                        addRow(key,value);
                    });
                }
                else{
                    $.toast({
                        heading: 'Error: Problemas con el servidor',
                        text: "No se pudieron cargar los recursos de este reto",
                        position: 'top-right',
                        loaderBg:'#fc4b6c',
                        icon: 'error',
                        hideAfter: 3000, 
                        stack: 1
                    });
                }
            }
            else{
                removeLoad();
            }
        }
    }
    $("#formFilterReq button#success").on("click",function(){
        getReqReto();//Recarga los recursos al presionar cualquier botón
        $(".right-sidebar").removeClass("shw-rside");
    });
    function addRow(key,value){
        var estatus = "";
        var estatus_text = "";
        var registros = ""
        if (value.tot_instanciado > 0) registros = `<span class="label label-light-info">${value.tot_instanciado}</span>`;
        if (value.estatus == "Produccion") {estatus = "label-light-warning";estatus_text="text-warning"}
        if (value.estatus == "Activa") {estatus = "label-light-info";estatus_text="text-info"}
        if (value.estatus == "En linea") {estatus = "label-light-success";estatus_text="text-success"}
        if (value.estatus == "Cancelada") {estatus = "label-light-danger";estatus_text="text-danger"}
        // <td id="tipo_recurso">${value.tipo_recurso}</td>
        // <td id="version_recurso">${value.version_recurso}</td>
        var reg = `<tr id="${value.unityTarget}" data-id="${key}" data-id_recurso="${value.id_recurso} " class="recurso-imm ${(value.estatus == "En linea" ? "linea" : value.estatus)}" data-reto="${value.reto}" data-grado="${value.grado}" data-tipo="${value.tipo_recurso}" data-version="${value.version_recurso}" data-estatus="${value.estatus}">
                            <td id="unityTarget" class="d-none d-sm-none d-md-block">${value.unityTarget}</td>
                            <td id="recurso">
                                <p>
                                    <i class="fa fa-circle font-10 m-r-10 ${estatus_text}" data-toggle="tooltip" data-placement="top" title="${value.estatus}"></i>
                                    ${registros} ${(value.recurso != "" && value.recurso != "null" && value.recurso != null ? value.recurso : value.unityBundle)}
                                </p>
                            </td>
                            <td id="grado" class="d-none d-sm-none d-md-block">${value.grado}</td>
                            <td id="reto">${value.reto}</td>
                            <td id="estatus" class="d-none d-sm-none d-md-block">
                                <i class="mdi mdi-calendar-check" data-toggle="tooltip" data-placement="top" title="${value.fecha_creacion}"></i>
                                <i class="mdi mdi-calendar-clock" data-toggle="tooltip" data-placement="top" title="${value.fecha_modificacion}"></i>
                                <!--<span class="label ${estatus}">${value.estatus}</span>-->
                            </td>
                    </tr>`;
        $("table#requisiciones tbody").append(reg);
        $('[data-toggle="tooltip"]').tooltip()
        $(".recursos .card-subtitle").html(`Recursos de producción ${$("#requisiciones .recurso-imm").length}`);

        $(".card-footer .label-light-primary").html(`Total <strong>${$("#requisiciones .recurso-imm").length}</strong>`);
        $(".card-footer .label-light-warning").html(`Producción <strong>${$("#requisiciones .recurso-imm.Produccion").length}</strong>`);
        $(".card-footer .label-light-info").html(`Activa <strong>${$("#requisiciones .recurso-imm.Activa").length}</strong>`);
        $(".card-footer .label-light-success").html(`En linea <strong>${$("#requisiciones .recurso-imm.linea").length}</strong>`);
        $(".card-footer .label-light-danger").html(`Cancelada <strong>${$("#requisiciones .recurso-imm.Cancelada").length}</strong>`);
    }
    $("table#requisiciones tbody").on(clickHandler,'tr.recurso-imm #recurso p',function(){
        $("#formReq")[0].reset();
        $("#formReq select option").each(function() { 
            $(this).selectedIndex = 0 
            $(this).removeAttr("selected");
        });
        /* Generar modal para ver/modificar recurso */
        $("#formReq").removeClass("newRecurso");
        $("#aprobar-requisicion .modal-title").html("Recurso Immersive");
        $("button#save-req").html("Modificar");
        /* Generar modal para ver/modificar recurso */
        var keyArr = $(this).parents(".recurso-imm").data("id");
        $("#aprobar-requisicion select option").attr("selected",false);
        $('#aprobar-requisicion').modal('show');
        var changeReto = false;
        var changeGrado = false;
        var changeIdioma = false;
        console.log(objData.data[keyArr].reto, objData.data[keyArr].grado)
        if(objData.data[keyArr].reto == null || objData.data[keyArr].reto == 0) {changeReto = true;console.log("reto null");}
        else{$(`#aprobar-requisicion #req-reto option[value='${objData.data[keyArr].reto}']`).attr("selected", true);}

        if(objData.data[keyArr].grado == null || objData.data[keyArr].grado == 0) {changeGrado = true;console.log("grado null");}
        else{$(`#aprobar-requisicion #req-grado option[value='${(objData.data[keyArr].grado)}']`).attr("selected", true);}

        if(objData.data[keyArr].reto >= 0) {console.log("Es numero");}
        if(changeReto || changeGrado || changeIdioma) setRetoYgrado(objData.data[keyArr].unityTarget, changeReto, changeGrado, changeIdioma);
        $("#aprobar-requisicion #req-id_recurso").val(objData.data[keyArr].id_recurso);
        $("#aprobar-requisicion #req-requisicion").val(objData.data[keyArr].id_requisicion);
        $("#aprobar-requisicion #req-unityTarget").val(objData.data[keyArr].unityTarget);
        $("#aprobar-requisicion #req-titulo").val(objData.data[keyArr].recurso);
        $("#aprobar-requisicion #req-unityBundle").val(objData.data[keyArr].unityBundle);
        versionesTipo(objData.data[keyArr].tipo_recurso);
        $(`#aprobar-requisicion #req-tipo_recurso option[value='${objData.data[keyArr].tipo_recurso}']`).attr("selected", true);
        $(`#aprobar-requisicion #req-version_recurso option[value='${objData.data[keyArr].version_recurso}']`).attr("selected", true);

        if(objData.data[keyArr].idioma == null || objData.data[keyArr].idioma == '') {changeGrado = true;console.log("Idioma"+objData.data[keyArr].idioma);}
        else $(`#aprobar-requisicion #req-idioma option[value='${objData.data[keyArr].idioma}']`).attr("selected", true);
        $(`#aprobar-requisicion #req-pathway option[value='${objData.data[keyArr].pathw}']`).attr("selected", true);
        $(`#aprobar-requisicion #req-estatus option[value='${objData.data[keyArr].estatus}']`).attr("selected", true);
        $("#aprobar-requisicion #req-version").val(objData.data[keyArr].version);
        $(`#aprobar-requisicion #req-sesion option[value='${objData.data[keyArr].sesion}']`).attr("selected", true);
        $(`#aprobar-requisicion #req-ciclo option[value='${objData.data[keyArr].ciclo}']`).attr("selected", true);
        
        $("#aprobar-requisicion #req-comentarios").val(objData.data[keyArr].comentarios);

        if (objData.data[keyArr].liga_script == "" || objData.data[keyArr].liga_script == null ) {
            $("#aprobar-requisicion #req-url_script").val();
            $("#title-liga-script").html(`Liga del script:`);
        }
        else{
            $("#aprobar-requisicion #req-url_script").val(objData.data[keyArr].liga_script);
            $("#title-liga-script").html(`Liga del script: <i class="link-recurso mdi mdi-link"></i>`);
        }
        if (objData.data[keyArr].liga_autoria == "" || objData.data[keyArr].liga_autoria == null ) {
            $("#aprobar-requisicion #req-url_autoria").val();
            $("#title-liga-autoria").html(`Liga de recurso (ResourceBank):`);
        }
        else{
            $("#aprobar-requisicion #req-url_autoria").val(objData.data[keyArr].liga_autoria);
            $("#title-liga-autoria").html(`Liga de Recurso (ResourceBank): <i class="link-recurso mdi mdi-link"></i>`);
        }
        if (objData.data[keyArr].liga_wf == "" || objData.data[keyArr].liga_wf == null ) {
            $("#aprobar-requisicion #req-url_workflow").val();
            $("#title-liga-workflow").html(`Liga de workflow:`);
        }
        else{
            $("#aprobar-requisicion #req-url_workflow").val(objData.data[keyArr].liga_wf);
            $("#title-liga-workflow").html(`Liga de workflow: <i class="link-recurso mdi mdi-link"></i>`);
        }
    });
    getReqReto();
    // $("select#reto").change(function(){
    //     getReqReto($(this).val());
    // });
    var formReq = document.querySelector("#formReq");
    formReq.onsubmit = function(e){
        e.preventDefault();
        var reqreto = $(`#aprobar-requisicion #req-reto`).val();
        var reqgrado = $(`#aprobar-requisicion #req-grado`).val();
        var reqtipo = $(`#aprobar-requisicion #req-tipo_recurso`).val();
        var reqversion = $(`#aprobar-requisicion #req-version_recurso`).val();
        var reqidioma = $(`#aprobar-requisicion #req-idioma`).val();
        
        var reqUnityTarget = $(`#aprobar-requisicion #req-unityTarget`).val();
        var reqtitulo = $("#aprobar-requisicion #req-titulo").val();
        var requnityBundle = $("#aprobar-requisicion #req-unityBundle").val();
        var requnityGrado   = $("#aprobar-requisicion #req-grado").val();
        console.log(reqUnityTarget == '' , requnityBundle == '' , (reqtitulo == '' || requnityGrado == '' ));
        if(reqUnityTarget == '' && requnityBundle == '' && (reqtitulo == '' || requnityGrado == '' )){
            $.toast({
                heading: 'Error: Datos faltantes',
                text: 'Es necesario ingresar unityTarget, unityBundle o nombre del recurso y grado',
                position: 'top-right',
                loaderBg:'#fc4b6c',
                icon: 'error',
                hideAfter: 5000, 
                stack: 2
            });
            return false;
        }
        var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        /*--------> Guardar o modificar <--------*/
        if ($("#formReq").hasClass("newRecurso")) var ajaxUrl = base_url+'/catalogoImm/newRecurso'; 
        else var ajaxUrl = base_url+'/catalogoImm/updateRecurso';
        /*--------> Guardar o modificar <--------*/
        var formData = new FormData(formReq);
        request.open("POST",ajaxUrl,true);
        request.send(formData);
        request.onreadystatechange = function(){
            if(request.readyState == 4 && request.status == 200){
                var objData = JSON.parse(request.responseText);
                if(objData.status){
                    $('#aprobar-requisicion').modal("hide");
                    formReq.reset();
                    getReqReto();
                    if ($("#formReq").hasClass("newRecurso")){
                        $.toast({
                            heading: 'Nuevo: Se creó un nuevo recurso',
                            text: "Se actualizó correctamente",
                            position: 'top-right',
                            loaderBg:'#21c1d6',
                            icon: 'success',
                            hideAfter: 3000, 
                            stack: 1
                        });
                    }
                    else{
                        $.toast({
                            heading: 'Modificación: Cambios realizados',
                            text: "Se actualizó correctamente",
                            position: 'top-right',
                            loaderBg:'#21c1d6',
                            icon: 'success',
                            hideAfter: 3000, 
                            stack: 1
                        });
                        //Actualizar los datos si se hizo un cambio 
                        $(`#requisiciones tr[data-id_recurso=${objData.id_recurso}] td[id="unityTarget"]`).html(objData.unityTarget);
                        $(`#requisiciones tr[data-id_recurso=${objData.id_recurso}] td[id="recurso"]`).html(objData.recurso);
                        $(`#requisiciones tr[data-id_recurso=${objData.id_recurso}] td[id="tipo_recurso"]`).html(objData.tipo_recurso);
                        $(`#requisiciones tr[data-id_recurso=${objData.id_recurso}] td[id="version_recurso"]`).html(objData.version_recurso);
                        $(`#requisiciones tr[data-id_recurso=${objData.id_recurso}] td[id="idioma"]`).html(objData.idioma);
                        $(`#requisiciones tr[data-id_recurso=${objData.id_recurso}] td[id="grado"]`).html(objData.grado);
                        var estatus = "";
                        if (objData.estatus == "En produccion") {estatus = "label-light-warning";}
                        if (objData.estatus == "Activa") {estatus = "label-light-info";}
                        if (objData.estatus == "En linea") {estatus = "label-light-success";}
                        if (objData.estatus == "Cancelada") {estatus = "label-light-danger";}
                        
                        $(`#requisiciones tr[data-id_recurso=${objData.id_recurso}] td[id="estatus"]`).html(`<span class="label ${estatus}">${objData.estatus}</span>`);
                    }
                }else{
                    $.toast({
                        heading: 'Error: Datos faltantes',
                        text: "Problemas al guardar",
                        position: 'top-right',
                        loaderBg:'#fc4b6c',
                        icon: 'error',
                        hideAfter: 3000, 
                        stack: 1
                    });
                }              
            }
            else{
                removeLoad();
                $.toast({
                    heading: 'Error: Problemas con el servidor',
                    text: "No se pudo guardar el recurso",
                    position: 'top-right',
                    loaderBg:'#fc4b6c',
                    icon: 'error',
                    hideAfter: 3000, 
                    stack: 1
                });
            }
        }
    };
    

    var intervalDelete
    $("#dalete-req").on(clickHandlerDown,function(){
        // console.log("Eliminando");
        var n = 5;
        $("#dalete-req").html("Eliminando en: "+n)
        intervalDelete = setInterval(function(){
            n--;
            $("#dalete-req").html("Eliminando en: "+n)
            if (n <= 0) {
                clearInterval(intervalDelete);
                deleteRecurso();
            }
        },1000);
    });
    
    $("#dalete-req").mouseout(function(){
        clearInterval(intervalDelete);
        $(this).html("Eliminar");
    })
    $("#dalete-req").on(clickHandlerUp,function(){
        clearInterval(intervalDelete);
        $(this).html("Eliminar");
    });

    function deleteRecurso(){
        // console.log("Eliminado");
        var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        var ajaxUrl  = base_url+'catalogoImm/deleteRecurso/'+$("#req-id_recurso").val();
        console.log(ajaxUrl);
    
        request.open("GET",ajaxUrl,true);
        request.send();
        request.onreadystatechange = function(){
            if(request.readyState == 4 && request.status == 200){
                objData = JSON.parse(request.responseText);
                if(objData.status){
                    $('#aprobar-requisicion').modal("hide");
                    $.toast({
                        heading: 'Eliminado: Se eliminó el recurso',
                        text: "Se eliminó correctamente",
                        position: 'top-right',
                        loaderBg:'#21c1d6',
                        icon: 'success',
                        hideAfter: 3000, 
                        stack: 1
                    });
                    getReqReto();
                }
            }
            else{
                $.toast({
                    heading: 'Error: Problemas con el servidor',
                    text: "No se pudo borrar el recurso",
                    position: 'top-right',
                    loaderBg:'#fc4b6c',
                    icon: 'error',
                    hideAfter: 3000, 
                    stack: 1
                });
            }
        }
    }
});
$("#copy-unityTarget").on(clickHandler,function(){
    var unityTargetC = $("input#req-unityTarget").val();
    $("input#req-unityTarget").val("action=openActivity&description=Interactive&activity="+unityTargetC+"&userId={{userid}}&username={{username}}&password={{password}}&reto={{reto}}&grado={{grado}}");
    $("input#req-unityTarget").select();
    document.execCommand("copy");
    $("input#req-unityTarget").val(unityTargetC);
    $.toast({
        heading: 'Copiado al portapapeles',
        text: "Se copió la URL de: "+unityTargetC,
        position: 'top-right',
        loaderBg:'#fc4b6c',
        icon: 'info',
        hideAfter: 3000, 
        stack: 1
    });
});
$(".form-group").on(clickHandler,".link-recurso", function(){
    window.open($(this).parent().next("input").val(), '_blank');
});

// const listTipos;
$("#req-tipo_recurso").change(function(){
    var tipo = $(this).val();
    versionesTipo(tipo);
});
function versionesTipo(tipo){
    if(!tipo) return 0;
    const listTipos = {
        "IMM Augmented Reality": ["360° Photo","360° Stage","3D Model","AR Instructions","Body AR","Color &amp; Explore","Custom Hologram","Custom Interactive","Guess it!","Isometric Infographic","Learn &amp; Practice","Premium","Target","The Gate","Trivia Now!","Virtual Museum","Voxel Model"],
        "IMM Interactive": ["3D Model","Guess it!","Isometric Infographic","Learn &amp; Practice","Trivia Now!","Voxel Model"],
        "IMM Prime":["Custom Hologram","Custom Interactive","Premium"],
        "IMM Virtual Reality":["360º Photo", "360º Stage", "Virtual Museum"]
    }
    $("#req-version_recurso").html(`<option value=""></option>`);      
    for (let index = 0; index < listTipos[tipo].length; index++) {
        $("#req-version_recurso").append(`<option value="${listTipos[tipo][index]}">${listTipos[tipo][index]}</option>`);        
    }
}
$(`#aprobar-requisicion #req-unityBundle`).change(function(){
    $("#formReq select#req-idioma option").each(function() { 
        $(this).selectedIndex = 0 
        $(this).removeAttr("selected");
    });
    if($(`#aprobar-requisicion #req-idioma`).val() != null){
        if ($(this).val().includes("_en_")) {
            $(`#aprobar-requisicion #req-idioma option[value=EN]`).attr("selected", true);
        }
        if ($(this).val().includes("_es_")) {
            $(`#aprobar-requisicion #req-idioma option[value=ES]`).attr("selected", true);
        }
    }
});
// $(`#aprobar-requisicion #req-unityTarget`).on("click",function(){
function setRetoYgrado(UTargetVal,retoC,gradoC,idiomaC){
    if($(`#aprobar-requisicion #req-reto`).val()=="0")
        $("#formReq select#req-reto option").each(function() { 
            $(this).selectedIndex = 0 
            $(this).removeAttr("selected");
        });
    
    if($(`#aprobar-requisicion #req-grado`).val()=="")
        $("#formReq select#req-grado option").each(function() { 
            $(this).selectedIndex = 0 
            $(this).removeAttr("selected");
        });
    var unityTarget = UTargetVal;
    var reto = unityTarget.substring(1,2);
    var GrupoGrado = unityTarget.substring(2,3);
    var Grado = unityTarget.substring(3,4);
    var GradoFinal = "";
    
    switch (GrupoGrado) {
        case "1":
            GradoFinal = "K"+Grado;
            break;
        case "2":
            GradoFinal = "PF";
            break;
        case "3":
            GradoFinal = "E"+Grado;
            break;
        case "4":
            GradoFinal = "M"+(parseInt(Grado)+6);
            break;
        case "5":
            GradoFinal = "PREMIUM"+Grado;
            break;
        default:
            break;
    }
    console.log("unityTarget:"+unityTarget,"Reto: "+reto,"grupo: "+GrupoGrado, "Grado: "+GradoFinal);
    // if($(`#aprobar-requisicion #req-reto`).val()=="0")
    if(retoC)
        $(`#aprobar-requisicion #req-reto option[value='${reto}']`).prop("selected", true);
    // if($(`#aprobar-requisicion #req-grado`).val()=="")
    if(gradoC)
        $(`#aprobar-requisicion #req-grado option[value='${GradoFinal}']`).prop("selected", true);
    
    /* IDIOMA */
    console.log(idiomaC);
    if(idiomaC){
        $("#formReq select#req-idioma option").each(function() { 
            $(`#aprobar-requisicion #req-unityBundle`).selectedIndex = 0 
            $(`#aprobar-requisicion #req-unityBundle`).removeAttr("selected");
        });
        console.log($(`#aprobar-requisicion #req-idioma`).val());
        // if($(`#aprobar-requisicion #req-idioma`).val() != null){
            if ($(`#aprobar-requisicion #req-unityBundle`).val().includes("_en_")) {
                $(`#aprobar-requisicion #req-idioma option[value=EN]`).attr("selected", true);
            }
            if ($(`#aprobar-requisicion #req-unityBundle`).val().includes("_es_")) {
                $(`#aprobar-requisicion #req-idioma option[value=ES]`).attr("selected", true);
            }
        // }
    }
}


function getGrado(grado){
    var vGrade;
    var username;
    var password;
    var idUser;
    var imgTH;
    switch (grado) {
        case '1st Grade Elementary School':
        case 'E1':
            vGrade = "E1";
            // imgTH +="PB/"+vIdioma+vTipo;
            username = "kac.coachp1b";
            password = "a35dd6";
            idUser = "19667dd6-5813-47f1-867f-f7e225f57157";
            break;
        case '2nd Grade Elementary School':
        case 'E2':
            vGrade = "E2";
            // imgTH +="PB/"+vIdioma+vTipo;
            username = "kac.coachp2b";
            password = "3805ab";
            idUser = "b46f4e4e-c664-4ca1-bfa0-d12e09c8cf23";
            break;
        case '3rd Grade Elementary School':
        case 'E3':
            vGrade = "E3";
            // imgTH +="PB/"+vIdioma+vTipo;
            username = "kac.coachp3b";
            password = "dbc7f5";
            idUser = "b618a8e4-b93c-47d8-82a2-fd2bdc60b4c3";
            break;
        case '4th Grade Elementary School':
        case 'E4':
            vGrade = "E4";
            // imgTH +="PA/"+vIdioma+vTipo;
            username = "kac.coachp4b";
            password = "7e6388";
            idUser = "035d45b0-29be-4b27-9e23-5fd20db005d0";
            break;
        case '5th Grade Elementary School':
        case 'E5':
            vGrade = "E5";
            // imgTH +="PA/"+vIdioma+vTipo;
            username = "kac.coachp5b";
            password = "88848b";
            idUser = "870e2935-efdb-46f1-a1c9-f5b0e23523fe";
            break;
        case '6th Grade Elementary School':
        case 'E6':
            vGrade = "E6";
            // imgTH +="PA/"+vIdioma+vTipo;
            username = "kac.coachp6b";
            password = "2b23df";
            idUser = "ce9df0de-95b9-4084-9a64-067b2095dcfb";
            break;
        case '7th Grade Middle School':
        case 'M7':
            vGrade = "M7";
            // imgTH +="SEC/"+vIdioma+vTipo;
            username = "kac.coachs1b";
            password = "08c8b6";
            idUser = "cc62d13d-10d1-4ac1-9464-e4696870396c";
            break;
        case '8th Grade Middle School':
        case 'M8':
            vGrade = "M8";
            // imgTH +="SEC/"+vIdioma+vTipo;
            username = "kac.coachs2b";
            password = "ee587d";
            idUser = "bd2f1abf-72d7-452b-95c4-1ec2ebbf80cc";
            break;
        case '9th Grade Middle School':
        case 'M9':
            vGrade = "M9";
            // imgTH +="SEC/"+vIdioma+vTipo;
            username = "kac.coachs3b";
            password = "34f794";
            idUser = "cf5453f4-dd52-458f-99a7-3d966fbdad98";
            break;
        case 'Kindergarten 1':
        case 'K1':
            vGrade = "K1";
            // imgTH +="K/"+vIdioma+vTipo;
            username = "kac.coachk1b";
            password = "deb2f6";
            idUser = "25364fdd-219c-4a5a-a74f-ff8614a95a4b";
            break;
        case 'Kindergarten 2':
        case 'K2':
            vGrade = "K2";
            // imgTH +="K/"+vIdioma+vTipo;
            username = "kac.coachk2b";
            password = "c27294";
            idUser = "c9cd5d6a-cd98-4016-9f19-df3d5a33ac88";
            break;
        case 'Kindergarten 3':
        case 'K3':
            vGrade = "K3";
            // imgTH +="K/"+vIdioma+vTipo;
            username = "kac.coachk3b";
            password = "9f83d5";
            idUser = "fc9f9ad8-ea24-4694-9faf-a38ab98dc372";
            break;
        case 'Pre-First':
        case 'PF':
            vGrade = "PF";
            // imgTH +="K/"+vIdioma+vTipo;
            username = "kac.coachpfb";
            password = "961b9d";
            idUser = "3cbd2d63-4625-4f0f-8eda-c09e229d3df8";
            break;
        default:
            break;
    }
    return {
        vGrade,
        username,
        password,
        idUser,
    }
}
$(".unityTarget").on(clickHandler, function(){
    /* iPad */
    $(".unityTarget").css("display","inline");
    var arrUnityTarget = $("#aprobar-requisicion #req-unityTarget").val();
    var arrGrado = $(`#aprobar-requisicion #req-grado`).val();
    var arrReto = $(`#aprobar-requisicion #req-reto`).val();
    console.log("Grado: "+arrGrado,"UTarget:"+arrUnityTarget);
    var objUrl = getGrado(arrGrado);
    console.log(objUrl);
    console.log(`immersive://action=openActivity&description=Interactive&activity=${arrUnityTarget}&userId=${objUrl.idUser}&username=${objUrl.username}&password=${objUrl.password}&reto=${arrReto}&grado=${arrGrado}`);
    if(arrUnityTarget != "" && arrGrado != "")
    alert(`immersive://action=openActivity&description=Interactive&activity=${arrUnityTarget}&userId=${objUrl.idUser}&username=${objUrl.username}&password=${objUrl.password}&reto=${arrReto}&grado=${arrGrado}`);
    window.open(`immersive://action=openActivity&description=Interactive&activity=${arrUnityTarget}&userId=${objUrl.idUser}&username=${objUrl.username}&password=${objUrl.password}&reto=${arrReto}&grado=${arrGrado}`, '_blank');
});