$("#filtrar-recursos").on(clickHandler, function(){
    $(".right-sidebar").toggleClass("shw-rside");
});
$("#nuevo-recurso").click(function(){
    $("#formReq").addClass("newRecurso");
    $("#aprobar-requisicion .modal-title").html("Nuevo recurso de Immersive");
    $('#aprobar-requisicion').modal("show");
    $("button#save-req").html("Crear");
    $("#formReq")[0].reset();
    $("#formReq select option").each(function() { 
        $(this).selectedIndex = 0;
        $(this).removeAttr("selected");
    });
});
function filterReq(){
    var filterReq = "";
    var filterReqReto = "";
    var filterReqGrado = "";
    var filterReqTipo = "";
    var filterReqVersion = "";
    var filterReqEstatus = "";
    if($("#formFilterReq input#req-busqueda").val() != "") filterReq += $("#formFilterReq input#req-busqueda").val();
    $("#formFilterReq input:checked").each(function(){
        switch ($(this).attr("data-type")) {
            case "reto":
                filterReqReto += $(this).attr("data-val")+",";
                break;
            case "grado":
                filterReqGrado += $(this).attr("data-val")+",";
                break;
            case "tipo":
                filterReqTipo += $(this).attr("data-val")+",";
                break;
            case "version":
                filterReqVersion += $(this).attr("data-val")+",";
                break;
            case "estatus":
                filterReqEstatus += $(this).attr("data-val")+",";
                break;
            default:
                break;
        } 
    });
    filterReqReto = filterReqReto.substring(0, filterReqReto.length -1);
    filterReqGrado = filterReqGrado.substring(0, filterReqGrado.length -1);
    filterReqTipo = filterReqTipo.substring(0, filterReqTipo.length -1);
    filterReqVersion = filterReqVersion.substring(0, filterReqVersion.length -1);
    filterReqEstatus = filterReqEstatus.substring(0, filterReqEstatus.length -1);
    return `Texto=${filterReq}&Reto=${filterReqReto}&Grado=${filterReqGrado}&Tipo=${filterReqTipo}&Version=${filterReqVersion}&Estatus=${filterReqEstatus}`;
}