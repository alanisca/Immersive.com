AFRAME.registerComponent('do-something-once-loaded', {
    init: function () {
        setTimeout(() => {
            $(".load").animate({"opacity":0},1000,function(){
                $(".load").css("display","none");
                $(".logo, .circle").css("display","block");
                $(".logo").animate({"opacity":"1"},1000,function(){
                    $(".logo").animate({"top":"-150px"},1000,function(){
                        $(".circle").animate({"opacity":"1"},1000);
                    });
                });
            });
        }, 1000);
    }
});