$(window).on('load', function() {
    addLoad();
	$('#table-users').footable({
		"columns": [
			{
				"name":"imagen",
				"title":"",
				"formatter": function(value, options, rowData){
				removeLoad();
				if(value === null)
					return "<img src='" + base_url + "assets/images/users/default.webp' width='25' class='img-circle' />";
				else
					return "<img src='" + value + "' width='25' class='img-circle' />";
				
			},"style":{"width":30,"maxWidth":30}},
			{"name":"usuario","title":"Usuario"},
			{"name":"correo","title":"Correo"},
			{"name":"rol","title":"Cuenta","formatter": function(value, options, rowData){
				var color = "";
				var rol = "";
				switch (value) {
					case "Administrador":
						rol = "Administrador";
						color = "danger";
						break;
					case "Productor":
						rol = "Productor";
						color = "warning";
						break;
					case "Lider":
						rol = "Lider";
						color = "success";
						break;
					case "Arte":
						rol = "arte";
						color = "primary";
						break;
					case "Programador":
						rol = "programador";
						color = "info";
						break;
					case "Invitado":
						rol = "Invitado";
						color = "info";
						break;
					default:
						rol = "Sin asignar";
						color = "primary";
						break;
				  }
				return "<span class='label label-"+color+"'>"+rol+"</span>";
			}},
			{"name":"id_usuario","title":"Editar","formatter": function(value, options, rowData){
				return '<button type="button" class="btn btn-warning btn-xs userData" data-toggle="modal" data-target="#edit-user" data-id="'+value+'"><i class="fa fa-edit"></i></button>';
				removeLoad();
			}},

		],
		"rows": $.getJSON( base_url+'users/getUsers')
	});
    $('#table-users').on('click','button.userData',function(){
        var idUser = $(this).attr('data-id');
        console.log(idUser+"get datos de usuario");
        var userData;
        if(idUser > 0){
            $.ajax({
                type: 'GET',
                url: base_url+'users/getUser/'+idUser,
                dataType: 'JSON',
                success: function (data) {
                    if(data.status){
                        userData = data.data;
                        $('#id_user').val(idUser);
                        $('#imgUser').attr('src',userData.imagen)
                        $('#userName').html(userData.usuario);
                        $('#userMail').html(userData.correo);
                        $(`#accesoImm option[value='${userData.rol_Immersive}']`).prop("selected", true);
                        if(userData.rol_Immersive == 0) $(`#accesoImm`).parent(".form-group").removeClass("has-success").addClass("has-danger");
                        else $(`#accesoImm`).parent(".form-group").removeClass("has-danger").addClass("has-success");

                        $(`#accesoInt option[value='${userData.rol_Interactives}']`).prop("selected", true);
                        if(userData.rol_Interactives == 0) $(`#accesoInt`).parent(".form-group").removeClass("has-success").addClass("has-danger");
                        else $(`#accesoInt`).parent(".form-group").removeClass("has-danger").addClass("has-success");

                        $(`#tipoUsuario option[value='${userData.rol}']`).prop("selected", true);
                    }
                }
            });
        }
    });
    var formReq = document.querySelector("#editUser");
    formReq.onsubmit = function(e){
        e.preventDefault();
        var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        var ajaxUrl = base_url+'users/editUser';
        console.log(ajaxUrl);
        var formData = new FormData(formReq);
        request.open("POST",ajaxUrl,true);
        request.send(formData);
        request.onreadystatechange = function(){
            var objData = JSON.parse(request.responseText);
            if(request.readyState == 4 && request.status == 200){
                var objData = JSON.parse(request.responseText);
                if(objData.status){
                    $('#edit-user').modal("hide");
                    if ($("#formReq").hasClass("newRecurso")){
                        $.toast({
                            heading: 'Nuevo: Se creó un nuevo recurso',
                            text: "Se actualizó correctamente",
                            position: 'top-right',
                            loaderBg:'#21c1d6',
                            icon: 'success',
                            hideAfter: 3000, 
                            stack: 1
                        });
                    }
                    else{
                        $.toast({
                            heading: 'Modificación: Cambios realizados',
                            text: "Se actualizó correctamente",
                            position: 'top-right',
                            loaderBg:'#21c1d6',
                            icon: 'success',
                            hideAfter: 3000, 
                            stack: 1
                        });
                        //Actualizar los datos si se hizo un cambio 
                    }
                }else{
                    $.toast({
                        heading: 'Error: Datos faltantes',
                        text: "Problemas al editar",
                        position: 'top-right',
                        loaderBg:'#fc4b6c',
                        icon: 'error',
                        hideAfter: 3000, 
                        stack: 1
                    });
                }              
            }
            else{
                removeLoad();
                $.toast({
                    heading: 'Error: Problemas con el servidor',
                    text: "No se pudo editar el usuario",
                    position: 'top-right',
                    loaderBg:'#fc4b6c',
                    icon: 'error',
                    hideAfter: 3000, 
                    stack: 1
                });
            }
        }
    }
});