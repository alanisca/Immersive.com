function isiPad(){
    var userAgent = window.navigator.userAgent;
    return (/iPad/).test(userAgent);
}
function goIMM(){
    fetch(base_url+'catalogoImm/getIMM')
    .then((resp) => resp.json())
    .then((json) => {
        json.map(function(resp){
            var imgTH="";
            var vGrade;
            var URLSheme = resp.urlsheme;
            if(resp.idioma == "Spanish") var vIdioma = "ES/";
            else var vIdioma = "EN/";
            if (resp.tipo_recurso == "Interactive") var vTipo = "Interactivo.jpg"
            if (resp.tipo_recurso == "IMM Interactive") var vTipo = "Interactivo.jpg"
            if (resp.tipo_recurso == "IMM Virtual Reality") var vTipo = "RealidadVirtual.jpg"
            if (resp.tipo_recurso == "IMM Augmented Reality") var vTipo = "RealidadVirtual.jpg"
            if (resp.tipo_recurso == "IMM Prime") var vTipo = "Prime.jpg"
            switch (resp.grado) {
                case '1st Grade Elementary School':
                case 'E1':
                    vGrade = "E1";
                    imgTH +="PB/"+vIdioma+vTipo;
                    username = "kac.alu1p1b";
                    password = "0fb952";
                    idUser = "55661d82-6646-43ac-af49-bc27a3ba3865";
                    break;
                case '2nd Grade Elementary School':
                case 'E2':
                    vGrade = "E2";
                    imgTH +="PB/"+vIdioma+vTipo;
                    username = "kac.alu1p2b";
                    password = "6b7bae";
                    idUser = "c2a94bcd-e693-4396-aa46-7560d91fa21a";
                    break;
                case '3rd Grade Elementary School':
                case 'E3':
                    vGrade = "E3";
                    imgTH +="PB/"+vIdioma+vTipo;
                    username = "kac.alu1p3b";
                    password = "98af42";
                    idUser = "a76bab3a-ff4d-49e8-8791-fea256eacab7";
                    break;
                case '4th Grade Elementary School':
                case 'E4':
                    vGrade = "E4";
                    imgTH +="PA/"+vIdioma+vTipo;
                    username = "kac.alu1p4b";
                    password = "c47ed4";
                    idUser = "8b341e99-612c-4045-8153-f21d37ac5d28";
                    break;
                case '5th Grade Elementary School':
                case 'E5':
                    vGrade = "E5";
                    imgTH +="PA/"+vIdioma+vTipo;
                    username = "kac.alu1p5b";
                    password = "66eb8b";
                    idUser = "9d902d4e-894f-4720-8d47-ec709b73d90c";
                    break;
                case '6th Grade Elementary School':
                case 'E6':
                    vGrade = "E6";
                    imgTH +="PA/"+vIdioma+vTipo;
                    username = "kac.alu1p6b";
                    password = "8aab1d";
                    idUser = "7a9760ac-65e6-471f-a763-a347c6513c5a";
                    break;
                case '7th Grade Middle School':
                case 'M7':
                    vGrade = "M7";
                    imgTH +="SEC/"+vIdioma+vTipo;
                    username = "kac.alu1s1b";
                    password = "7bdc5b";
                    idUser = "a46963c1-959b-480d-b6b1-cea5e3e32e0c";
                    break;
                case '8th Grade Middle School':
                case 'M8':
                    vGrade = "M8";
                    imgTH +="SEC/"+vIdioma+vTipo;
                    username = "kac.alu1s2b";
                    password = "cb747c";
                    idUser = "5016c3c0-a8b8-4796-b642-6b1d554f8b1c";
                    break;
                case '9th Grade Middle School':
                case 'M9':
                    vGrade = "M9";
                    imgTH +="SEC/"+vIdioma+vTipo;
                    username = "kac.alu1s3b";
                    password = "f3cb1f";
                    idUser = "15fdf25-1c1e-41eb-82e5-b9c52e593c77";
                    break;
                case 'Kindergarten 1':
                case 'K1':
                    vGrade = "K1";
                    imgTH +="K/"+vIdioma+vTipo;
                    username = "kac.alu1k1b";
                    password = "4165";
                    idUser = "04a99b69-ed10-44bd-847d-92ec6c91e55c";
                    break;
                case 'Kindergarten 2':
                case 'K2':
                    vGrade = "K2";
                    imgTH +="K/"+vIdioma+vTipo;
                    username = "kac.alu1k2b";
                    password = "9025";
                    idUser = "";
                    break;
                case 'Kindergarten 3':
                case 'K3':
                    vGrade = "K3";
                    imgTH +="K/"+vIdioma+vTipo;
                    username = "kac.alu1k3b";
                    password = "1761";
                    idUser = "7baa2cd8-8c08-4fdb-8228-5feddc299679";
                    break;
                case 'Pre-First':
                case 'PF':
                    vGrade = "PF";
                    imgTH +="K/"+vIdioma+vTipo;
                    username = "kac.alu1pfb";
                    password = "5709";
                    idUser = "49d98de-7a86-486f-a6ed-6900617ae120";
                    break;
                default:
                    break;
            }
            $(".card-body .row.content").append(
                `<div class="col-12 col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 knTarjetas" data-reto="R${resp.reto}" data-grado="${vGrade}" data-version="${resp.Version}" data-titulo="${resp.titulo}" data-url="${(URLSheme != "" ? URLSheme : "-")}">`+
                    '<div class="card blog-widget">'+
                        '<div class="card-body">'+
                            '<div class="blog-image">'+
                                '<img src="'+base_url+'assets/IMMTH/'+imgTH+'" alt="img" class="img-fluid blog-img-height w-100">'+
                                '<h6 class="knTitulo w-100">'+resp.titulo+'</h6>'+
                            '</div>'+
                            `<div class="contain">
                                    <label class="badge badge-pill badge-primary py-1 px-2">${vGrade}</label>
                                    <label class="badge badge-pill BCR${resp.reto} py-1 px-2">R${resp.reto}</label>
                                    <label class="badge badge-pill badge-secondary py-1 px-2">S${resp.sesion}</label>
                                    <label class="badge badge-pill badge-secondary py-1 px-2">${resp.tipo_recurso}</label>
                                    <label class="badge badge-pill badge-secondary py-1 px-2">${resp.version_recurso}</label>
                                </div>`+
                            (isiPad() ?
                            '<div class="row text-center data">'+
                                (URLSheme != '' ? `<div class="col-lg-12 col-md-12 "><a class="link" href="immersive://action=openActivity&description=Interactive&activity=${URLSheme}&userId=${idUser}&username=${username}&password=${password}"> <h6 class="mb-0 font-weight-light"><u>${URLSheme}</u></h6></a><small>URL</small></div>` : 
                                '<div class="col-lg-12 col-md-12 "><h6 class="mb-0 font-weight-light">-</h6><small>Sin URL</small></div>')+
                                '<div class="col-lg-6 col-md-6 ">'+
                                '</div>'+
                            '</div>' : 
                            '<div class="row text-center data">'+
                                (URLSheme != '' ? `<div class="col-lg-12 col-md-12 "> <h6 class="mb-0 font-weight-light">${URLSheme}</h6><small>URL</small></div>` : 
                                '<div class="col-lg-12 col-md-12 "><h6 class="mb-0 font-weight-light">-</h6><small>Sin URL</small></div>')+
                                '<div class="col-lg-6 col-md-6 ">'+
                                '</div>'+
                            '</div>')+
                        '</div>'+
                    '</div>'+
                '</div>'
            );
        }
        );
        $(".card-subtitle").html("Catálogo de producción - "+($(".knTarjetas").length - $(".versionHide, .retoHide, .gradoHide").length)+" de "+$(".knTarjetas").length);
        // console.log(json[0].titulo)
    });
}
var versionCB = [
{
    title: "360º Photo",
    status: true
},
{
    title: "360º Stage",
    status: true
},
{
    title: "3D Model",
    status: true
},
{
    title: "AR Instructions",
    status: true
},
{
    title: "Body AR",
    status: true
},
{
    title: "Custom Hologram",
    status: true
},
{
    title: "Custom Interactive",
    status: true
},
{
    title: "Guess it!",
    status: true
},
{
    title: "Isometric Infographic",
    status: true
},
{
    title: "Learn & Practice",
    status: true
},
{
    title: "Premium",
    status: true
},
{
    title: "Target",
    status: true
},
{
    title: "Trivia Now!",
    status: true
},
{
    title: "Virtual Museum",
    status: true
},
{
    title: "Voxel Model",
    status: true
}
];
var gradoCB = [
    {
        title: "E1",
        status: true
    },
    {
        title: "E2",
        status: true
    },
    {
        title: "E3",
        status: true
    },
    {
        title: "E4",
        status: true
    },
    {
        title: "E5",
        status: true
    },
    {
        title: "E6",
        status: true
    },
    {
        title: "M7",
        status: true
    },
    {
        title: "M8",
        status: true
    },
    {
        title: "M9",
        status: true
    },
    {
        title: "K1",
        status: true
    },
    {
        title: "K2",
        status: true
    },
    {
        title: "K3",
        status: true
    },
    {
        title: "PF",
        status: true
}];
var retosCB = [{
        title: "R1",
        status: true
    },
    {
        title: "R2",
        status: true
    },
    {
        title: "R3",
        status: true
    },
    {
        title: "R4",
        status: true
    },
    {
        title: "R5",
        status: true
    },
    {
        title: "R6",
        status: true
    },
    {
        title: "R7",
        status: true
    },
    {
        title: "R8",
        status: true
    }
];
function initVersionPopover(){
    var versionString = "";
    for (let index = 0; index < versionCB.length; index++) {
        versionString += `<div class="col-12"><input class="versionCB" data-id="${index}" id="${versionCB[index].title}" type="checkbox" ${(versionCB[index].status ? "checked" : "")}><label class="popVersion" for="${versionCB[index].title}"> ${versionCB[index].title} </label></div>`;   
    }
    filtrarVersion();
    return versionString;
}
$('.popover-version').popover({content:initVersionPopover()}).click(function () {
    $('.popover-retos, .popover-grados').popover('hide');
    $("input.versionCB").change(function(){
        versionCB[parseInt($(this).attr("data-id"))].status = $(this).prop("checked");
        $('.popover-version').attr("data-content",initVersionPopover());
    }); 
});

function initRetosPopover(){
    var retosString = "";
    for (let index = 0; index < retosCB.length; index++) {
        retosString += `<div class="col-12"><input class="retosCB" data-id="${index}" id="${retosCB[index].title}" type="checkbox" ${(retosCB[index].status ? "checked" : "")}><label class="popReto" for="${retosCB[index].title}"> ${retosCB[index].title} </label></div>`;   
    }
    filtrarReto();
    return retosString;
}
$('.popover-retos').popover({content:initRetosPopover()}).click(function () {
    $('.popover-version, .popover-grados').popover('hide');
    $("input.retosCB").change(function(){
        retosCB[parseInt($(this).attr("data-id"))].status = $(this).prop("checked");
        $('.popover-retos').attr("data-content",initRetosPopover());
    }); 
});

function initGradoPopover(){
    var gradoString = "";
    for (let index = 0; index < gradoCB.length; index++) {
        gradoString += `<div class="col-12"><input id="${gradoCB[index].title}" class="gradoCB" data-id="${index}" type="checkbox" ${(gradoCB[index].status ? "checked" : "")}><label class="popGrado" for="${gradoCB[index].title}"> ${gradoCB[index].title} </label></div>`;   
    }
    filtrarGrado();
    return gradoString;
}
$('.popover-grados').popover({content: initGradoPopover()}).click(function () {
    $('.popover-retos, .popover-version').popover('hide');
    $("input.gradoCB").change(function(){
        gradoCB[parseInt($(this).attr("data-id"))].status = $(this).prop("checked");
        $('.popover-grados').attr("data-content",initGradoPopover());
    });    
});

function filtrarVersion(){
    for (let index = 0; index < versionCB.length; index++) {
        if (versionCB[index].status) {
            $(".knTarjetas[data-version='"+versionCB[index].title+"']").removeClass("showCard versionHide");
        }
        else{
            $(".knTarjetas[data-version='"+versionCB[index].title+"']").addClass("showCard versionHide");
        }
    }
    $(".card-subtitle").html("Catálogo de producción - "+($(".knTarjetas").length - $(".versionHide, .retoHide, .gradoHide").length+" de "+$(".knTarjetas").length));
}
function filtrarReto(){
    for (let index = 0; index < retosCB.length; index++) {
        if (retosCB[index].status) {
            $(".knTarjetas[data-reto='"+retosCB[index].title+"']").removeClass("showCard retoHide");
        }
        else{
            $(".knTarjetas[data-reto='"+retosCB[index].title+"']").addClass("showCard retoHide");
        }
    }
    $(".card-subtitle").html("Catálogo de producción - "+($(".knTarjetas").length - $(".versionHide, .retoHide, .gradoHide").length+" de "+$(".knTarjetas").length));
}
function filtrarGrado(){
    for (let index = 0; index < gradoCB.length; index++) {
        if (gradoCB[index].status) {
            $(".knTarjetas[data-grado='"+gradoCB[index].title+"']").removeClass("showCard gradoHide");
        }
        else{
            $(".knTarjetas[data-grado='"+gradoCB[index].title+"']").addClass("showCard gradoHide");
        }
    }
    $(".card-subtitle").html("Catálogo de producción - "+($(".knTarjetas").length - $(".versionHide, .retoHide, .gradoHide").length+" de "+$(".knTarjetas").length));
}
$("input.buscador").keypress(function(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if(code==13){
	    buscarInteractivo($(this).val());
    }
});
// $('input.buscador').keyup(function(){
// 	buscarInteractivo($(this).val());
// });
function buscarInteractivo(txt){
    console.log("buscando");
	// if (txt.trim() == '') return false;
	var nombres = $('.knTarjetas');
	var buscando = txt.toLowerCase();
	var item='';
	var item2='';
	for( var i = 0; i < nombres.length; i++ ){
	    item  = $(nombres[i]).data('titulo').toLowerCase();
	    item2 = $(nombres[i]).data('url').toString().toLowerCase();
		for(var x = 0; x < item.length; x++ ){
			if( buscando.length == 0 || item.indexOf( buscando ) > -1 || item2.indexOf( buscando ) > -1 ){
				$(nombres[i]).removeClass("tituloHide"); 
			}else{
			   	$(nombres[i]).addClass("tituloHide");
			}
		}
	}
}
goIMM();