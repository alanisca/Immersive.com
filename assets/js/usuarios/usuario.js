var ArrayData;
var ArrayDataUsuarios;
var ArrayDataRecursos;
const Retos = [
    'Reto 1',
    'Reto 2',
    'Reto 3',
    'Reto 4',
    'Reto 5',
    'Reto 6',
    'Reto 7',
    'Reto 8',
    'Sin reto'
];
const Colors = [
    {
        backgroundColor: 'rgb(30 136 229)',
        borderColor: 'rgb(30 136 229)'
    },
        {
        backgroundColor: 'rgb(116 96 238)',
        borderColor: 'rgb(116 96 238)'
    },
        {
        backgroundColor: 'rgb(38 198 218)',
        borderColor: 'rgb(38 198 218)'
    },
        {
        backgroundColor: 'rgb(255 177 43)',
        borderColor: 'rgb(255 177 43)'
    },
    {
        backgroundColor: 'rgb(30 136 229)',
        borderColor: 'rgb(30 136 229)'
    }
];
var datasets = [];
document.addEventListener('DOMContentLoaded', function(){
    $.ajax({
        type: 'GET',
        url: base_url+'usuarios/getUsuario/'+$("body").attr("id_kn_usuario"),
        dataType: 'html',
        success: function (data) {
            ArrayData = JSON.parse(data);
            if(ArrayData[0].status){
                $('#usuario').text(ArrayData[0].data.usuario);
                $('#nombre').text(ArrayData[0].data.nombre + ' ' + ArrayData[0].data.apellido);
                $('#escuela').html(`<a style="color:#fff;" target="_blank" href="${base_url+'escuelas/escuela/'+ArrayData[0].data.id_escuela}">${ArrayData[0].data.escuela.toLowerCase()}</a>`);
                
                removeLoad();
            }
            if(ArrayData[1].status){   
                datasets = [];
                datasets.push({
                    label: ArrayData[1].data.id_kn_usuario,
                    backgroundColor: Colors[0].backgroundColor,
                    borderColor: Colors[0].borderColor,
                    data: [
                        ArrayData[1].data.reto1,
                        ArrayData[1].data.reto2,
                        ArrayData[1].data.reto3,
                        ArrayData[1].data.reto4,
                        ArrayData[1].data.reto5,
                        ArrayData[1].data.reto6,
                        ArrayData[1].data.reto7,
                        ArrayData[1].data.reto8,
                        ArrayData[1].data.reto0,
                        // ArrayData[3].data[index].reto0
                    ],
                    type: 'line',
                    order: 1
                });
                const data = {
                    labels: Retos,
                    datasets: datasets
                };
                const config = {
                    type: 'line',
                    data: data
                };
                const myChart = new Chart(
                    document.getElementById('myChart'),
                    config
                );
            }
            if(ArrayData[2].status){   
                $.each(ArrayData[2].data, function( index, value ) {
                    $('#registrosTable tbody').append(`
                    <tr>
                        <td style="width:50px;"><span class="round" id="reg-id_recurso">${value.id_recurso}</span></td>
                        <td>
                            <h6 id="reg-recurso">${value.recurso}</h6><small class="text-muted" id="reg-unityTarget">${value.unityTarget} Periodo: ${value.period}</small>
                        </td>
                        <td>
                            <h6 id="reg-dispositivo-ios">Versión de IMM: ${value.version_app}</h6><small class="text-muted" id="reg-version_app">iPad: ${value.dispositivo} con iOS ${value.ios}</small>
                        </td>
                        <td id="reg-fecha_registro">${value.fecha_registro}</td>
                    </tr>
                    `);
                });
                $('#tot_recursos').text(ArrayData[2].data.length);
            }
        }
    });
});
var version_app = [];
var version_app_p = [];