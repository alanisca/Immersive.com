var objData;
var objEscuelas = [];
var totEscuelas = 0;
var objUsuarios = [];
var totUsuarios = 0;
var objRecursos = [];
var totRecursos = 0;


var objDataRecursos;
var arrDataRecursos = [];
var arrDataRecursosReg = [];
var arrDataRecursosP = [];
var arrDataRecursosRegP = [];
var totRecursos = 0;
document.addEventListener('DOMContentLoaded', function(){
    function getDashboard(){
        var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        var ajaxUrl  = base_url+'home/getRegistros';
        
        request.open("GET",ajaxUrl,true);
        request.send();
        addLoad();
        
        request.onreadystatechange = function(){
            if(request.readyState == 4 && request.status == 200){
                objData = JSON.parse(request.responseText);
                if(objData.status){
                    // $("table#requisiciones tbody").html("");
                    registros(objData.data);
                    $.each(objData.data, function(key,value) {
                        // console.log(objData.data);
                        var findUsuario = objUsuarios.find(objUsuarios => objUsuarios.usuario === objData.data[key].usuario);
                        if(findUsuario){
                            objUsuarios[findUsuario.index].totRecursos = objUsuarios[findUsuario.index].totRecursos + 1;
                        }
                        else{
                            objUsuarios.push({
                                usuario: objData.data[key].usuario,
                                escuela: objData.data[key].escuela,
                                grado: objData.data[key].u_grado,
                                totRecursos: 1,
                                index:totUsuarios
                            });
                            totUsuarios++;
                        }

                        var findEscuelas = objEscuelas.find(objEscuelas => objEscuelas.escuela === objData.data[key].escuela);
                        if(findEscuelas){
                            objEscuelas[findEscuelas.index].totRecursos = objEscuelas[findEscuelas.index].totRecursos + 1;
                        }
                        else{
                            objEscuelas.push({
                                escuela: objData.data[key].escuela,
                                totRecursos: 1,
                                index:totEscuelas
                            });
                            totEscuelas++;
                        }

                        var findRecursos = objRecursos.find(objRecursos => objRecursos.recurso === objData.data[key].recurso);
                        if(findRecursos){
                            objRecursos[findRecursos.index].totRecursos = objRecursos[findRecursos.index].totRecursos + 1;
                        }
                        else{
                            objRecursos.push({
                                recurso: objData.data[key].recurso,
                                totRecursos: 1,
                                grado: objData.data[key].rec_grado,
                                index:totRecursos
                            });
                            totRecursos++;
                        }
                    });
                    objUsuarios = objUsuarios.sort((c1, c2) => (c1.totRecursos < c2.totRecursos) ? 1 : (c1.totRecursos > c2.totRecursos) ? -1 : 0);
                    objEscuelas = objEscuelas.sort((c1, c2) => (c1.totRecursos < c2.totRecursos) ? 1 : (c1.totRecursos > c2.totRecursos) ? -1 : 0);
                    objRecursos = objRecursos.sort((c1, c2) => (c1.totRecursos < c2.totRecursos) ? 1 : (c1.totRecursos > c2.totRecursos) ? -1 : 0);
                    $("#tot-recursos").html(totRecursos);
                    $("#tot-usuarios").html(totUsuarios);
                    $("#tot-escuelas").html(totEscuelas);
                    $.each(objUsuarios, function(key,value){
                        $("#top-usuarios").append(`<li><span class="round top5 bg-success">${key+1}</span><h6 style="margin-bottom: 0px;"><a href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario.toLowerCase()}</a></h6><small class="text-muted">${value.grado} - Registros: ${value.totRecursos}</small></li>`);
                    });
                    $.each(objEscuelas, function(key,value){
                        $("#top-escuelas").append(`<li><span class="round top5 bg-primary">${key+1}</span><h6 style="margin-bottom: 0px;">${(value.escuela == "" ? "-" : value.escuela)}</h6><small class="text-muted">Registros: ${value.totRecursos}</small></li>`);
                    });
                    $.each(objRecursos, function(key,value){
                        $("#top-recursos").append(`<li><span class="round top5">${key+1}</span><h6 style="margin-bottom: 0px;">${value.recurso}</h6><small class="text-muted">${value.grado} - Registros: ${value.totRecursos}</small></li>`);
                    });
                }
                removeLoad();
                loadedPage();
            }
        }
    }
    getDashboard();

    function registros(data){
        $.each(data,function(key,value){
            var imgTHF = getSrcImg(value.rec_grado, value.rec_idioma, value.rec_tipo);
            $('#registros table tbody').append(`
            <tr>
                <td>${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="round">'+(key+1)+'</span>')}</td>
                <td>
                    <h6><a href="${base_url+'usuarios/usuario/'+value.u_id_kn_usuario}">${value.usuario}</a></h6>
                    <small class="text-muted">${value.recurso}</small>
                </td>
                <td>${value.fecha_hora}</td>
            </tr>
            `);
        });
    }
    function headerData(){
        var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        var ajaxUrl  = base_url+'record/getHeader';

        request.open("GET",ajaxUrl,true);
        request.send();
        addLoad();
        
        request.onreadystatechange = function(){
            if(request.readyState == 4 && request.status == 200){
                removeLoad();
                objDataRecursos = JSON.parse(request.responseText);
                // console.log(objDataRecursos);
                $('#version').text(objDataRecursos[0].version);
                $('#tot_recurosos').text(objDataRecursos[1].total_recursos);
                $('#tot_escuelas').text(objDataRecursos[2].total_escuelas);
                $('#tot_alumnos').text(objDataRecursos[3].tot_usuarios);

                /* Grafica */
                var dataIOS_labels = []; 
                var dataIOS_data = [];
                $.each(objDataRecursos[4], function(key,value) {
                    dataIOS_labels.push(value.version_app);
                    dataIOS_data.push(value.porciento);
                });
                const ctx = document.getElementById('myChart-iOS').getContext('2d');
                const myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: dataIOS_labels,
                        datasets: [{
                            label: 'Versiones',
                            data: dataIOS_data,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        plugins: {
                            datalabels: {
                                color: '#fff',
                                formatter: (dato) => Math.trunc(dato) + "%",
                                anchor: "center"
                            }
                        }
                    },
                    plugins: [ChartDataLabels]
                });
            }
        }
    }
    headerData();
    function getVersionIMM(){
        var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        var ajaxUrl  = base_url+'home/getHeader';

        request.open("GET",ajaxUrl,true);
        request.send();
        addLoad();
        
        request.onreadystatechange = function(){
            if(request.readyState == 4 && request.status == 200){
                removeLoad();
                objDataRecursos = JSON.parse(request.responseText);
                if(objDataRecursos.status){
                    $.each(objDataRecursos.data, function(key,value) {
                        if(arrDataRecursos[objDataRecursos.data[key].reto]) arrDataRecursos[objDataRecursos.data[key].reto]++;
                        else arrDataRecursos[objDataRecursos.data[key].reto] = 1;

                        if(objDataRecursos.data[key].reg_accion == "instanciar")
                            if(arrDataRecursosReg[objDataRecursos.data[key].reto]) arrDataRecursosReg[objDataRecursos.data[key].reto]++;
                            else arrDataRecursosReg[objDataRecursos.data[key].reto] = 1;
                    });
                    $.each(arrDataRecursos, function(key,value){
                        arrDataRecursosP[key] = 100;
                        arrDataRecursosRegP[key] = Math.trunc(((arrDataRecursosReg[key]*100)/arrDataRecursos[key]));
                    });
                    var option = {
                        title: {
                            text: 'Versiones de IMMERSIVE'
                        },
                        tooltip: {},
                        legend: {
                            data:['Disponibles','Registrados']
                        },
                        xAxis: {
                            data: ["-","R1","R3","R3","R4","R5","R6","R7","R8"]
                        },
                        yAxis: {},
                        series: [
                        // {
                        //     name: 'Disponibles',
                        //     type: 'bar',
                        //     data: arrDataRecursosP
                        // },
                        {
                            name: 'Actualizaciones',
                            type: 'bar',
                            data: arrDataRecursosRegP
                        }]
                    };
                    var myChart = echarts.init(document.getElementById('grafica-progreso'));
                
                    myChart.setOption(option);
                }
                else{
                    console.log("falla");
                }
            }
        }
    }
    // grafica()
});