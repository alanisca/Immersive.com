document.addEventListener('DOMContentLoaded', function(){
    console.log("init candy");
    function getReqReto(){
        var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
        var ajaxUrl  = base_url+'escuelas/getEscuelas';

        request.open("GET",ajaxUrl,true);
        request.send();
        addLoad();

        request.onreadystatechange = function(){
            if(request.readyState == 4 && request.status == 200){
                removeLoad();
                objData = JSON.parse(request.responseText);
                console.log(objData);
                if(objData.status){
                    $("table#requisiciones tbody").html("");
                    $.each(objData.data, function(key,value) {
                        addRow(key, value);
                    });
                    $('#escuelas-foo-addrow').footable();
                }
                else{
                    $.toast({
                        heading: 'Error: Problemas con el servidor',
                        text: "No se pudieron cargar los recursos de este reto",
                        position: 'top-right',
                        loaderBg:'#fc4b6c',
                        icon: 'error',
                        hideAfter: 3000, 
                        stack: 1
                    });
                }
            }
            else{
                removeLoad();
            }
        }
    }
    getReqReto();
    function addRow(key, value){
        $('table#escuelas-foo-addrow tbody').append(`
            <tr class="" style="">
                <td>${key}</td>
                <td><a href="${base_url+'escuelas/escuela/'+value.id_escuela}">${value.escuela}</a></td>
                <td>${value.reto1}</td>
                <td>${value.reto2}</td>
                <td>${value.reto3}</td>
                <td>${value.reto4}</td>
                <td>${value.reto5}</td>
                <td>${value.reto6}</td>
                <td>${value.reto7}</td>
                <td>${value.reto8}</td>
                <td>${value.reto0}</td>
                <td>${value.total}</td>
            </tr>
        `);
    }
    $("#escuelasFilter").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        setTimeout(() => {
            $("#escuelas-foo-addrow tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        }, 1000);
    });
});