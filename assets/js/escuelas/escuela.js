var ArrayData;
var ArrayDataUsuarios;
var ArrayDataRecursos;
console.log("asdf1");
document.addEventListener('DOMContentLoaded', function(){
    $.ajax({
        type: 'GET',
        url: base_url+'escuelas/getEscuela/'+$("body").attr("id_escuela"),
        dataType: 'html',
        success: function (data) {
            ArrayData = JSON.parse(data);
            if(ArrayData[0].status){
                $("#escuela").text(ArrayData[0].data.escuela);
                $("#pais").text(`${ArrayData[0].data.prefijo} - ${ArrayData[0].data.pais}`);
                var tipoUsuario = "";
                $.each(ArrayData[1].data, function( index, value ) {
                    switch (value.tipo) {
                        case '1':
                            tipoUsuario = "success";
                            break;
                            
                        case '2':
                            tipoUsuario = "danger";
                            break;
                        default:
                            tipoUsuario = "secondary";
                            break;
                    }
                    switch (value.grado) {
                        case "1º de Preescolar":
                            $('#u_k1 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "2º de Preescolar":
                            $('#u_k2 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "3º de Preescolar":
                            $('#u_k3 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "Prefirst":
                            $('#u_pf .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "1º de Primaria":
                            $('#u_e1 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "2º de Primaria":
                            $('#u_e2 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "3º de Primaria":
                            $('#u_e3 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "4º de Primaria":
                            $('#u_e4 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "5º de Primaria":
                            $('#u_e5 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "6º de Primaria":
                            $('#u_e6 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "1º de Secundaria":
                            $('#u_m7 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "2º de Secundaria":
                            $('#u_m8 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        case "3º de Secundaria":
                            $('#u_m9 .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                        default:
                            $('#u_Otros .usuarios').append(`<div class="usuario col-2 text-center"><div class="round align-self-center btn-${tipoUsuario}">${value.recursos}</div><h6 style="font-size: 10px;"><a target="_blank" href="${base_url+'usuarios/usuario/'+value.id_kn_usuario}">${value.usuario}</a></h6></div>`);
                            break;
                    }
                    $('#u_k1 .sl-date').text($('#u_k1 .usuario').length+" usuarios");
                    $('#u_k2 .sl-date').text($('#u_k2 .usuario').length+" usuarios");
                    $('#u_k3 .sl-date').text($('#u_k3 .usuario').length+" usuarios");
                    $('#u_pf .sl-date').text($('#u_pf .usuario').length+" usuarios");
                    $('#u_e1 .sl-date').text($('#u_e1 .usuario').length+" usuarios");
                    $('#u_e2 .sl-date').text($('#u_e2 .usuario').length+" usuarios");
                    $('#u_e3 .sl-date').text($('#u_e3 .usuario').length+" usuarios");
                    $('#u_e4 .sl-date').text($('#u_e4 .usuario').length+" usuarios");
                    $('#u_e5 .sl-date').text($('#u_e5 .usuario').length+" usuarios");
                    $('#u_e6 .sl-date').text($('#u_e6 .usuario').length+" usuarios");
                    $('#u_m7 .sl-date').text($('#u_m7 .usuario').length+" usuarios");
                    $('#u_m8 .sl-date').text($('#u_m8 .usuario').length+" usuarios");
                    $('#u_m9 .sl-date').text($('#u_m9 .usuario').length+" usuarios");
                    $('#u_Otros .sl-date').text($('#u_Otros .usuario').length+" usuarios");
                    $('#tot_alumnos').text($('.card_alumnos .usuario').length);
                });
                $.each(ArrayData[2].data,function(key,value){
                    var imgTHF = getSrcImg(value.rec_grado, value.rec_idioma, value.rec_tipo);
                    switch (value.rec_grado) {
                        case "1º de Preescolar":
                        case "K1":
                            $('#reto'+value.rec_reto+' #r_k1 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "2º de Preescolar":
                        case "K2":
                            $('#reto'+value.rec_reto+' #r_k2 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "3º de Preescolar":
                        case "K3":
                            $('#reto'+value.rec_reto+' #r_k3 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "Prefirst":
                        case "PF":
                            $('#reto'+value.rec_reto+' #r_pf .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "1º de Primaria":
                        case "E1":
                            $('#reto'+value.rec_reto+' #r_e1 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "2º de Primaria":
                        case "E2":
                            $('#reto'+value.rec_reto+' #r_e2 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "3º de Primaria":
                        case "E3":
                            $('#reto'+value.rec_reto+' #r_e3 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "4º de Primaria":
                        case "E4":
                            $('#reto'+value.rec_reto+' #r_e4 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "5º de Primaria":
                        case "E5":
                            $('#reto'+value.rec_reto+' #r_e5 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "6º de Primaria":
                        case "E6":
                            $('#reto'+value.rec_reto+' #r_e6 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "1º de Secundaria":
                        case "M7":
                            $('#reto'+value.rec_reto+' #r_m7 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "2º de Secundaria":
                        case "M8":
                            $('#reto'+value.rec_reto+' #r_m8 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                        case "3º de Secundaria":
                        case "M9":
                            $('#reto'+value.rec_reto+' #r_m9 .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                    
                        default:
                            $('#reto'+value.rec_reto+' #r_Otros .usuarios').append(`<div class="usuario col-2 text-center">${(imgTHF.ready ? '<img src="'+imgTHF.liga+'" alt="user" width="50" class="rounded">' : '<span class="rounded">'+(value.rec_unityTarget)+'</span>')}<h6 style="font-size: 10px;">${value.recurso}</h6></div>`);
                            break;
                    }
                });
                $('#tot_recursos').text($('.card_recursos .usuario').length);
                const Retos = [
                    'Reto 1',
                    'Reto 2',
                    'Reto 3',
                    'Reto 4',
                    'Reto 5',
                    'Reto 6',
                    'Reto 7',
                    'Reto 8'
                ];
                Colors = [
                        {
                        backgroundColor: 'rgb(30 136 229)',
                        borderColor: 'rgb(30 136 229)'
                    },
                        {
                        backgroundColor: 'rgb(116 96 238)',
                        borderColor: 'rgb(116 96 238)'
                    },
                        {
                        backgroundColor: 'rgb(38 198 218)',
                        borderColor: 'rgb(38 198 218)'
                    },
                        {
                        backgroundColor: 'rgb(255 177 43)',
                        borderColor: 'rgb(255 177 43)'
                    },
                    {
                        backgroundColor: 'rgb(30 136 229)',
                        borderColor: 'rgb(30 136 229)'
                    }
                ];
                var datasets = [];
                $.each(ArrayData[3].data, function( index, value ) {
                    var typeChart = 'line';
                    var orderChart = 0;
                    if(ArrayData[3].data[index].id_escuela == $("body").attr("id_escuela")){
                        typeChart = 'bar';
                        orderChart = 1;
                    } 
                    if(index > 0 || $("body").attr("id_escuela") == 1)
                        datasets.push({
                            label: ArrayData[3].data[index].escuela,
                            backgroundColor: Colors[index].backgroundColor,
                            borderColor: Colors[index].borderColor,
                            data: [
                                Math.trunc((ArrayData[3].data[index].reto1*100)/ArrayData[3].data[0].reto1),
                                Math.trunc((ArrayData[3].data[index].reto2*100)/ArrayData[3].data[0].reto2),
                                Math.trunc((ArrayData[3].data[index].reto3*100)/ArrayData[3].data[0].reto3),
                                Math.trunc((ArrayData[3].data[index].reto4*100)/ArrayData[3].data[0].reto4),
                                Math.trunc((ArrayData[3].data[index].reto5*100)/ArrayData[3].data[0].reto5),
                                Math.trunc((ArrayData[3].data[index].reto6*100)/ArrayData[3].data[0].reto6),
                                Math.trunc((ArrayData[3].data[index].reto7*100)/ArrayData[3].data[0].reto7),
                                Math.trunc((ArrayData[3].data[index].reto8*100)/ArrayData[3].data[0].reto8),
                                // ArrayData[3].data[index].reto0
                            ],
                            type: typeChart,
                            order: orderChart
                        });
                });
                const data = {
                    labels: Retos,
                    datasets: datasets
                };
                const config = {
                    type: 'line',
                    data: data,
                    options: {
                        plugins: {
                            datalabels: {
                                color: '#fff',
                                formatter: (dato) => dato + "%",
                                anchor: "center"
                            }
                        }
                    },
                    plugins: [ChartDataLabels]
                };
                const myChart = new Chart(
                    document.getElementById('myChart'),
                    config
                );
                /* Versiones, ios y Dispositivo */
                
                $.each(ArrayData[4].data, function( index, value ) {
                    version_app.push(ArrayData[4].data[index].version_app);
                    version_app_p.push(ArrayData[4].data[index].porciento);
                });
                var dispositivo_app = [];
                var dispositivo_app_p = [];
                $.each(ArrayData[5].data, function( index, value ) {
                    dispositivo_app.push(ArrayData[5].data[index].dispositivo);
                    dispositivo_app_p.push(ArrayData[5].data[index].porciento);
                });
                var ios_app = [];
                var ios_app_p = [];
                $.each(ArrayData[6].data, function( index, value ) {
                    ios_app.push(ArrayData[6].data[index].ios);
                    ios_app_p.push(ArrayData[6].data[index].porciento);
                });
                const data2 = {
                    labels: version_app,
                    datasets: [{
                        label: 'Datos',
                        backgroundColor: [
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                        ],
                        data: version_app_p
                    }]
                };
                const config2 = {
                    type: 'doughnut',
                    data: data2,
                    options: {
                        plugins:{
                            legend: {
                                display: true,
                                position: 'bottom'
                            },
                            tooltip: {
                                callbacks: {
                                    label: function(context) {
                                        let label = context.label;
                                        let value = context.formattedValue;
                        
                                        if (!label)
                                            label = 'Unknown'
                        
                                        let sum = 0;
                                        let dataArr = context.chart.data.datasets[0].data;
                                        dataArr.map(data => {
                                            sum += Number(data);
                                        });
                        
                                        let percentage = (value * 100 / sum).toFixed(0) + '%';
                                        return label + ": " + percentage;
                                    }
                                }
                              } 
                        }
                    }
                };
                const myChart2 = new Chart(
                    document.getElementById('versionApp'),
                    config2
                );

                const data3 = {
                    labels: dispositivo_app,
                    datasets: [{
                        label: 'Datos',
                        backgroundColor: [
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                        ],
                        data: dispositivo_app_p
                    }]
                };
                const config3 = {
                    type: 'doughnut',
                    data: data3,
                    options: {
                        plugins:{
                            legend: {
                                display: true,
                                position: 'bottom'
                            },
                            tooltip: {
                                callbacks: {
                                    label: function(context) {
                                        let label = context.label;
                                        let value = context.formattedValue;
                        
                                        if (!label)
                                            label = 'Unknown'
                        
                                        let sum = 0;
                                        let dataArr = context.chart.data.datasets[0].data;
                                        dataArr.map(data => {
                                            sum += Number(data);
                                        });
                        
                                        let percentage = (value * 100 / sum).toFixed(0) + '%';
                                        return label + ": " + percentage;
                                    }
                                }
                            } 
                        }
                    }
                };
                const myChart3 = new Chart(
                    document.getElementById('DispositivosApp'),
                    config3
                );

                const data4 = {
                    labels: ios_app,
                    datasets: [{
                        label: 'Datos',
                        backgroundColor: [
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                            'rgb(30 136 229)',
                            'rgb(116 96 238)',
                            'rgb(38 198 218)',
                            'rgb(255 177 43)',
                        ],
                        data: ios_app_p
                    }]
                };
                const config4 = {
                    type: 'pie',
                    data: data4,
                    options: {
                        plugins:{
                            legend: {
                                display: true,
                                position: 'bottom'
                            },
                            tooltip: {
                                callbacks: {
                                    label: function(context) {
                                        let label = context.label;
                                        let value = context.formattedValue;
                        
                                        if (!label)
                                            label = 'Unknown'
                        
                                        let sum = 0;
                                        let dataArr = context.chart.data.datasets[0].data;
                                        dataArr.map(data => {
                                            sum += Number(data);
                                        });
                        
                                        let percentage = (value * 100 / sum).toFixed(0) + '%';
                                        return label + ": " + percentage;
                                    }
                                }
                            } 
                        }
                    }
                };
                const myChart4 = new Chart(
                    document.getElementById('iosApp'),
                    config4
                );

                /* Graficos por grado */
                datasets = [];
                $.each(ArrayData[7].data, function( index, value ) {
                    var typeChart = 'line';
                    var orderChart = 0;
                    if(ArrayData[7].data[index].id_escuela == $("body").attr("id_escuela")){
                        typeChart = 'bar';
                        orderChart = 1;
                    } 
                    if(index > 0 || $("body").attr("id_escuela") == 1)
                        datasets.push({
                            label: ArrayData[7].data[index].escuela,
                            backgroundColor: Colors[index].backgroundColor,
                            borderColor: Colors[index].borderColor,
                            data: [
                                Math.trunc((ArrayData[7].data[index].gradoK*100)/ArrayData[7].data[0].gradoK),
                                Math.trunc((ArrayData[7].data[index].gradoK1*100)/ArrayData[7].data[0].gradoK1),
                                Math.trunc((ArrayData[7].data[index].gradoK2*100)/ArrayData[7].data[0].gradoK2),
                                Math.trunc((ArrayData[7].data[index].gradoK3*100)/ArrayData[7].data[0].gradoK3),
                                Math.trunc((ArrayData[7].data[index].gradoE*100)/ArrayData[7].data[0].gradoE),
                                Math.trunc((ArrayData[7].data[index].gradoE1*100)/ArrayData[7].data[0].gradoE1),
                                Math.trunc((ArrayData[7].data[index].gradoE2*100)/ArrayData[7].data[0].gradoE2),
                                Math.trunc((ArrayData[7].data[index].gradoE3*100)/ArrayData[7].data[0].gradoE3),
                                Math.trunc((ArrayData[7].data[index].gradoE4*100)/ArrayData[7].data[0].gradoE4),
                                Math.trunc((ArrayData[7].data[index].gradoE5*100)/ArrayData[7].data[0].gradoE5),
                                Math.trunc((ArrayData[7].data[index].gradoE6*100)/ArrayData[7].data[0].gradoE6),
                                Math.trunc((ArrayData[7].data[index].gradoM*100)/ArrayData[7].data[0].gradoM),
                                Math.trunc((ArrayData[7].data[index].gradoM7*100)/ArrayData[7].data[0].gradoM7),
                                Math.trunc((ArrayData[7].data[index].gradoM8*100)/ArrayData[7].data[0].gradoM8),
                                Math.trunc((ArrayData[7].data[index].gradoM9*100)/ArrayData[7].data[0].gradoM9),
                                Math.trunc((ArrayData[7].data[index].gradoPF*100)/ArrayData[7].data[0].gradoPF),
                                Math.trunc((ArrayData[7].data[index].gradoNull*100)/ArrayData[7].data[0].gradoNull)
                                // ArrayData[3].data[index].reto0
                            ],
                            type: typeChart,
                            order: orderChart
                        });
                });
                const Grados = [
                    'K',
                    'K1',
                    'K2',
                    'K3',
                    'E',
                    'E1',
                    'E2',
                    'E3',
                    'E4',
                    'E5',
                    'E6',
                    'M',
                    'M7',
                    'M8',
                    'M9',
                    'PF',
                    'null'
                ];
                const dataGrado = {
                    labels: Grados,
                    datasets: datasets
                };
                const configGrado = {
                    type: 'line',
                    data: dataGrado,
                    options: {
                        plugins: {
                            datalabels: {
                                color: '#fff',
                                formatter: (dato) => dato + "%",
                                anchor: "center"
                            }
                        }
                    },
                    plugins: [ChartDataLabels]
                };
                const myChartGrado = new Chart(
                    document.getElementById('myChartGrado'),
                    configGrado
                );
                removeLoad();
            }
        }
    });
    function getAllData(periodo = '', reto = '', grado = ''){

    }
    document.getElementById('btn-filtrar').click( () => {
        console.log(document.getElementById("select-periodos").value);
        console.log(document.getElementById("select-retos").value);
        console.log(document.getElementById("select-grados").value);
    });
    $.ajax({
		type: 'GET',
		url: '/quiz/obtener/'+$('.mainContainer').attr('data-id'),
		dataType: 'html',
		success: function (data) {
			quizArray = JSON.parse(data);
			initGame();
		}
	});
});
var version_app = [];
var version_app_p = [];