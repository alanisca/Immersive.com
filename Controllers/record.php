<?php
class record extends Controllers{
    public $userName;
    public $unityTarget;
    public $accion;
    public $latitude;
    public $longitude;
    public $date;
    public $nombre;
    public $apellido;
    public $escuela;
    public $pais;
    public $grado;
    public $tipo;
    public $unitybundle;
    public $version_app;
    public function __construct(){
        parent::__construct();
    }
    public function record(){
        $arrRegistro = array(
                    "userName" => ((isset($_GET["userName"]) && !empty(($_GET["userName"])) ) ? $_GET["userName"] : null),
                    "unityTarget" => ((isset($_GET["unityTarget"]) && !empty(($_GET["unityTarget"])) ) ? $_GET["unityTarget"] : null),
                    "accion" => ((isset($_GET["accion"]) && !empty(($_GET["accion"])) ) ? $_GET["accion"] : null),
                    "date" => ((isset($_GET["date"]) && !empty(($_GET["date"])) ) ? $_GET["date"] : null),
                    "version_app" => ((isset($_GET["version_app"]) && !empty(($_GET["version_app"])) ) ? $_GET["version_app"] : null),
                    "dispositivo" => ((isset($_GET["dispositivo"]) && !empty(($_GET["dispositivo"])) ) ? $_GET["dispositivo"] : null),
                    "escuela" => ((isset($_GET["school"]) && !empty(($_GET["school"])) ) ? $_GET["school"] : null),
                    "ios" => ((isset($_GET["ios"]) && !empty(($_GET["ios"])) ) ? $_GET["ios"] : null),
                    "idUserInt" => ((isset($_GET["idUserInt"]) && !empty(($_GET["idUserInt"])) ) ? $_GET["idUserInt"] : null),
                    "idPeriodInt" => ((isset($_GET["idPeriodInt"]) && !empty(($_GET["idPeriodInt"])) ) ? $_GET["idPeriodInt"] : null),
                    "Period" => ((isset($_GET["Period"]) && !empty(($_GET["Period"])) ) ? $_GET["Period"] : null),
                    "Campus" => ((isset($_GET["Campus"]) && !empty(($_GET["Campus"])) ) ? $_GET["Campus"] : null),
                    "Challenge" => ((isset($_GET["Challenge"]) && !empty(($_GET["Challenge"])) ) ? $_GET["Challenge"] : null)
                    );
        $arrData = $this->model->newRecord($arrRegistro);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    
    public function newUser(string $data){
        $arrNewUser = array("userName" => $_GET["userName"],
                            "nombre" => $_GET["nombre"],
                            "apellido" => $_GET["apellido"],
                            "escuela"=>$_GET["escuela"],
                            "pais"=>$_GET["pais"],
                            "grado" => $_GET["grado"],
                            "tipo" => $_GET["tipo"]);
        $arrData = $this->model->selectUser($arrNewUser);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function newunitytarget(string $data){
        $arrNewTarget = array("unitytarget"=>$_GET["unitytarget"],
                            "unitybundle"=>$_GET["unitybundle"]);
        $arrData = $this->model->selectUnity($arrNewTarget);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function newTarget(){
        $arrDataTarget = array($_GET["unityTarget"],
            $_GET["recurso"],
            $_GET["hoja"],
            $_GET["unityBundle"],
            $_GET["reto"],
            $_GET["grado"],
            $_GET["tipo_recurso"],
            $_GET["version_recurso"],
            $_GET["idioma"],
            $_GET["pathw"],
            $_GET["sesion"],
            $_GET["ciclo"],
            "Target creado desde la aplicación",
            "Aplicación"
        );
        $arrData = $this->model->newTargetModel($arrDataTarget);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();

    }
    public function getfileimm($fileName){
        echo getGrado($_GET['hola']);
        // echo $fileName;
    }
    public function prueba_servicioPost(){
        $arrRegistro = array(
            "contador" => $_POST["contador"],
            "cadena" => $_POST["servicio"]
        );
        $arrData = $this->model->pruebaRecord($arrRegistro);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function imm_version(){
        if( isset($_GET['id_pais']) && !empty($_GET['id_pais']) ){
            $id_pais_version = $_GET['id_pais'];
        }
        else{
            $id_pais_version = 1;
        }
        $arrData = $this->model->imm_version_model($id_pais_version);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function prueba_servicio(){
        $arrRegistro = array(
            "contador" => $_GET["contador"],
            "cadena" => $_GET["servicio"]
        );
        $arrData = $this->model->pruebaRecord($arrRegistro);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function getRegistros(){
        $arrData = $this->model->getRegistros_model();
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function getHeader(){
        $arrData = array();
        array_push($arrData,
            $this->model->get_versionIMM(),
            $this->model->get_totRecursos(),
            $this->model->get_totEscuelas(),
            $this->model->get_totAlumnos(),
            $this->model->get_graficaVersionIMM()
        );
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
}
?>