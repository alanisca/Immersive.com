<?php 
    class usuarios extends Controllers{
        public function __construct(){
            parent::__construct();
            /* Valida si ya ha iniciado sesión y lo redirige a HOME */
            define('DURACION_SESION','86400');//24 horas
            ini_set("session.cookie_lifetime",DURACION_SESION);
            ini_set("session.gc_maxlifetime",DURACION_SESION); 
            session_cache_expire(DURACION_SESION);
            session_start();
            session_regenerate_id(true);
            if(!isset($_SESSION['acceso_IMM']) || empty($_SESSION['acceso_IMM']) || $_SESSION['acceso_IMM'] != 1)
                header("Location:".base_url()."error");
            if(!isset($_SESSION['access_token']))
                header("Location:".base_url()."login");
        }
        public function usuarios(){
            $data['tag_page']="Usuarios";
            $data['tag_title']="Usuarios";
            $data['tag_name']="IMMERSIVE - Usuarios";
            $data['tag_text']="Catálogo de usuarios con total de recursos usados";
            $this->views->getView($this,"usuarios",$data);
        }
        public function getUsuarios(){
            $arrData = $this->model->getEscuelas();
            echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
            die();
        }
        public function usuario($id_kn_usuario){
            $data['tag_page']  = "Usuario";
            $data['tag_title'] = "Usuario";
            $data['tag_name']  = "IMMERSIVE - Usuario";
            $data['tag_text']  = "Información del usuario";
            $data['id_kn_usuario'] = $id_kn_usuario;
            $this->views->getView($this,"usuario",$data);
        }
        public function getUsuario($id_kn_usuario){
            $arrData = array();
            array_push($arrData,
                $this->model->getUsuario($id_kn_usuario),
                $this->model->getRegistrosUsuarioReto($id_kn_usuario),
                $this->model->getRegistrosUsuario($id_kn_usuario)
            );
            echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        }
        // public function getEscuelaUsuarios($id_escuela){
        //     $arrData = $this->model->getEscuelaUsuarios($id_escuela);
        //     echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        // }
        // public function getEscuelaRecursos($id_escuela){
        //     $arrData = $this->model->getEscuelaRecursos($id_escuela);
        //     echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        // }
        // public function getDisVerIos($id_escuela){
        //     $arrData = array();
        //     array_push($arrData,
        //         $this->model->getVersionApp($id_escuela),
        //         $this->model->getDispositivo($id_escuela),
        //         $this->model->getios($id_escuela)
        //     );
        //     echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        // }
    }
?>