<?php 
class automatizacion extends Controllers{

    public function __construct(){
        parent::__construct();
        /* Valida si ya ha iniciado sesión y lo redirige a HOME */
        define('DURACION_SESION','86400'); //2 horas
        ini_set("session.cookie_lifetime",DURACION_SESION);
        ini_set("session.gc_maxlifetime",DURACION_SESION); 
        session_cache_expire(DURACION_SESION);
        session_start();
        // session_regenerate_id(true); 
        // session_start();
        if(!isset($_SESSION['acceso_Int']) || empty($_SESSION['acceso_Int']) || $_SESSION['acceso_Int'] != 1)
            header("Location:".base_url()."error");
        if(!isset($_SESSION['access_token']))
            header("Location:".base_url()."login");
    }

    public function automatizacion(){
        $data['tag_name']="IMMERSIVE - INTERACTIVOS: Automatizar";
        $data['tag_page']="Automatizar";
        $data['tag_title']="¡Automatizar!";
        $data['tag_text']="Catálogo de producción para su digitalizacón y automatización";
        $this->views->getView($this,"automatizacion",$data);
    }
    public function getInteractives(){
        $arrData = $this->model->selectInteractives();
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function getInteractive($id){
        $intId = intval($id);
        $arrData = $this->model->selectInteractive($intId);
        $uploadZip = $this->model->uploadZIP($arrData["RB_Link_Archivo"], $intId);
        $arrUpDate = $this->model->updateInteractive($intId,$uploadZip);
        return $arrData;
        // echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        // die();
    }
    public function ver($id){
        $data['tag_name']="IMMERSIVE - INTERACTIVOS: Automatizar";
        $data['tag_page']="Automatizar";
        $data['tag_title']="¡Automatizar!";
        $data['tag_text']="Catálogo de producción para su digitalizacón y automatización";
        $data['id']=$id;
        $data['datos']=$this->getInteractive($id);
        $this->views->getView($this,"preview",$data);
    }
    public function saveOrigianlZIP(){
        print_r($this->model->uploadZIP());
    }
}
?>