<?php 
    class home extends Controllers{
        public function __construct(){
            parent::__construct();
            /* Valida si ya ha iniciado sesión y lo redirige a HOME */
            // /Applications/MAMP/bin/php/php7.4.21/conf
            define('DURACION_SESION','86400'); //2 horas
            ini_set("session.cookie_lifetime",DURACION_SESION);
            ini_set("session.gc_maxlifetime",DURACION_SESION); 
            session_cache_expire(DURACION_SESION);
            session_start();
            session_regenerate_id(true); 
            if(!isset($_SESSION['access_token']))
                header("Location:".base_url()."login");
        }
        public function home(){
            $data['tag_page']="Home";
            $data['tag_title']="Immersive";
            $data['tag_name']="IMMERSIVE - Página principal";
            $this->views->getView($this,"home",$data);
        }
        public function getRegistros(){
            $arrData = $this->model->getRegistros();
            if(empty($arrData)){
                $arrResponse = array('status' => false, 'msg' => 'Datos no encontrados');
            }
            else{
                $arrResponse = array('status' => true, 'data' => $arrData,'msg' => 'Datos encontrados');
            }
                echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
            die();
        }
        public function getRecursos(){
            $arrData = $this->model->getRecursos();
            if(empty($arrData)){
                $arrResponse = array('status' => false, 'msg' => 'Datos no encontrados');
            }
            else{
                $arrResponse = array('status' => true, 'data' => $arrData,'msg' => 'Datos encontrados');
            }
            echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
            die();
        }
        
    }
?>