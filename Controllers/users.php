<?php 
class users extends Controllers{
    public function __construct(){
        parent::__construct();
        /* Valida si ya ha iniciado sesión y lo redirige a HOME */
        define('DURACION_SESION','86400'); //2 horas
        ini_set("session.cookie_lifetime",DURACION_SESION);
        ini_set("session.gc_maxlifetime",DURACION_SESION); 
        session_cache_expire(DURACION_SESION);
        session_start();
        session_regenerate_id(true); 
        session_start();
        if(!isset($_SESSION['access_token']))
            header("Location:".base_url()."login");
    }

    public function users(){
        $data['tag_name']="IMMERSIVE - Usuarios";
        $data['tag_page']="Usuarios";
        $data['tag_title']="¡Usuarios!";
        $data['tag_text']="Administrar usuarios";
        $this->views->getView($this,"users",$data);
    }
    public function getUsers(){
        $arrData = $this->model->selectUsers();
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function getUser($id){
        $intId = intval($id);
        $arrData = $this->model->selectUser($intId);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function editUser(){
        $arrayUser = array(
            "accesoImm"=>$_POST['accesoImm'],
            "accesoInt"=>$_POST['accesoInt'],
            "tipoUsuario"=>$_POST['tipoUsuario'],
            "idUsuario"=>$_POST['id_user']
        );
        $arrData = $this->model->editUser($arrayUser);
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
}
?>