<?php 
class login extends Controllers{

    public function __construct(){
        parent::__construct();
        /* Valida si ya ha iniciado sesión y lo redirige a HOME */
        define('DURACION_SESION','86400'); //2 horas
        ini_set("session.cookie_lifetime",DURACION_SESION);
        ini_set("session.gc_maxlifetime",DURACION_SESION); 
        session_cache_expire(DURACION_SESION);
        session_start();
        session_regenerate_id(true); 
        if(isset($_SESSION['access_token']))
            header("Location:".base_url());
        else{
            $this->model->logoutSession();
        }
    }
    public function login(){
        $data['tag_name']="IMMERSIVE - Iniciar sesión";
        $data['tag_page']="Iniciar sesión";
        $data['tag_title']="¡Pagina no encontrada!";
        $data['tag_text']="Es necesario ingresar con tu correo KNOTION";
        $data["google_client"] = $this->model->goSesion();
        $this->views->getView($this,"login",$data);
    }
    public function correctLogin(){
        $login = $this->model->checkLogin($_GET["code"]);
        if($login)
            header("Location:".base_url());
        else{
            header("Location:".base_url()."login");
        }
    }
    public function logout(){
        if($this->model->logoutSession())
            header("Location:".base_url()."login");
    }
}
?>