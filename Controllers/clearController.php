<?php
class nombre extends Controllers{

    public function __construct(){
        parent::__construct();
        /* Valida si ya ha iniciado sesión y lo redirige a HOME */
        define('DURACION_SESION','86400'); //2 horas
        ini_set("session.cookie_lifetime",DURACION_SESION);
        ini_set("session.gc_maxlifetime",DURACION_SESION); 
        session_cache_expire(DURACION_SESION);
        session_start();
        session_regenerate_id(true); 
        // session_start();
        if(!isset($_SESSION['access_token']))
            header("Location:".base_url()."login");
    }

    public function notFound(){
        define('DURACION_SESION','86400'); //2 horas
        ini_set("session.cookie_lifetime",DURACION_SESION);
        ini_set("session.gc_maxlifetime",DURACION_SESION); 
        ini_set("session.save_path","/tmp");
        session_cache_expire(DURACION_SESION);
        session_start();
        session_regenerate_id(true); 
        // session_start();
        $data['tag_name']="IMMERSIVE - Error";
        $data['tag_page']="Error";
        $data['tag_title']="¡Pagina no encontrada!";
        $data['tag_text']="No fue posible encontrar la pagina";
        $this->views->getView($this,"error",$data);
    }

}
?>