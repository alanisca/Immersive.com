<?php 
class catalogoImm extends Controllers{
    public function __construct(){
        parent::__construct();
        /* Valida si ya ha iniciado sesión y lo redirige a HOME */
        define('DURACION_SESION','86400'); //2 horas
        ini_set("session.cookie_lifetime",DURACION_SESION);
        ini_set("session.gc_maxlifetime",DURACION_SESION);
        session_cache_expire(DURACION_SESION);
        session_start();
        session_regenerate_id(true); 
        // session_start();
        if(!isset($_SESSION['acceso_IMM']) || empty($_SESSION['acceso_IMM']) || $_SESSION['acceso_IMM'] != 1)
            header("Location:".base_url()."error");
        if(!isset($_SESSION['access_token']))
            header("Location:".base_url()."login");
    }
    public function catalogoImm(){
        $data['tag_name']="IMMERSIVE - Catálogo de productos";
        $data['tag_page']="Catálogo IMMERSIVE";
        $data['tag_title']="¡Catálogo!";
        $data['tag_text']="Catálogo de producción";
        $this->views->getView($this,"catalogoImm",$data);
    }
    public function getIMM(){
        $arrData = $this->model->getIMMS();
        echo json_encode($arrData, JSON_UNESCAPED_UNICODE);
        die();
    }
    /* requisiciones */
    public function recursos(){
        $data['tag_name']="IMMERSIVE - Recursos";
        $data['tag_page']="Recursos pendientes";
        $data['tag_title']="¡Recursos!";
        $data['tag_text']="Recursos de producción";
        $this->views->getView($this,"requisiciones",$data);
    }
    public function recurso(){
        $data['tag_name']="IMMERSIVE - Recurso";
        $data['tag_page']="Recurso";
        $data['tag_title']="¡Recurso!";
        $data['tag_text']="Recursos";
        $this->views->getView($this,"recurso",$data);
    }
    public function getRequisiciones(){
        $arrayRecurso = array(
            "Texto"=>$_GET["Texto"],
            "Reto"=>$_GET["Reto"],
            "Grado"=>$_GET["Grado"],
            "Tipo"=>$_GET["Tipo"],
            "Version"=>$_GET["Version"],
            "Estatus"=>$_GET["Estatus"]
        );
        // echo dep($arrayRecurso);
        $arrData = $this->model->selectReqReto($arrayRecurso);
        // echo $arrData;
        if(empty($arrData)){
            $arrResponse = array('status' => false, 'msg' => 'Datos no encontrados');
        }
        else{
            $arrResponse = array('status' => true, 'data' => $arrData,'msg' => 'Datos encontrados');
        }
        echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function newRecurso(){
        $arrayRecurso = array(
            "id_recurso"=>$_POST['req-id_recurso'],
            "requisicion"=>$_POST['req-requisicion'],
            "unityTarget"=>$_POST['req-unityTarget'],
            "titulo"=>$_POST['req-titulo'],
            "unityBundle"=>$_POST['req-unityBundle'],
            "reto"=>$_POST['req-reto'],
            "grado"=>$_POST['req-grado'],
            "tipo_recurso"=>$_POST['req-tipo_recurso'],
            "version_recurso"=>$_POST['req-version_recurso'],
            "idioma"=>$_POST['req-idioma'],
            "pathway"=>$_POST['req-pathway'],
            "sesion"=>$_POST['req-sesion'],
            "ciclo"=>$_POST['req-ciclo'],
            "url_script"=>$_POST['req-url_script'],
            "url_autoria"=>$_POST['req-url_autoria'],
            "url_workflow"=>$_POST['req-url_workflow'],
            "comentarios"=>$_POST['req-comentarios'],
            "version"=>$_POST['req-version'],
            "estatus"=>$_POST['req-estatus']
        );
        $request_rol = $this->model->newRecurso($arrayRecurso);
        echo json_encode($request_rol,JSON_UNESCAPED_UNICODE);
        die();
    }
    public function deleteRecurso($id_recurso){
        $request_rol = $this->model->deleteRecurso(intval($id_recurso));
        echo json_encode($request_rol,JSON_UNESCAPED_UNICODE);
        die();
    }
    public function updateRecurso(){
        $arrayRecurso = array(
            "id_recurso"=>$_POST['req-id_recurso'],
            "requisicion"=>$_POST['req-requisicion'],
            "unityTarget"=>$_POST['req-unityTarget'],
            "titulo"=>$_POST['req-titulo'],
            "unityBundle"=>$_POST['req-unityBundle'],
            "reto"=>$_POST['req-reto'],
            "grado"=>$_POST['req-grado'],
            "tipo_recurso"=>$_POST['req-tipo_recurso'],
            "version_recurso"=>$_POST['req-version_recurso'],
            "idioma"=>$_POST['req-idioma'],
            "pathway"=>$_POST['req-pathway'],
            "sesion"=>$_POST['req-sesion'],
            "ciclo"=>$_POST['req-ciclo'],
            "url_script"=>$_POST['req-url_script'],
            "url_autoria"=>$_POST['req-url_autoria'],
            "url_workflow"=>$_POST['req-url_workflow'],
            "comentarios"=>$_POST['req-comentarios'],
            "version"=>$_POST['req-version'],
            "estatus"=>$_POST['req-estatus']
        );
        $request_rol = $this->model->updateRecurso($arrayRecurso);
        echo json_encode($request_rol,JSON_UNESCAPED_UNICODE);
        die();
    }

    public function escuela(){
        $data['tag_name']="IMMERSIVE - Escuela";
        $data['tag_page']="Escuela IMMERSIVE";
        $data['tag_title']="Escuela!";
        $data['tag_text']="Datos de escuela";
        $this->views->getView($this,"escuelas",$data);
    }
    /* version */
    public function version(){
        $data['tag_name']="IMMERSIVE - version";
        $data['tag_page']="Version pendientes";
        $data['tag_title']="¡Version!";
        $data['tag_text']="Version de producción";
        $this->views->getView($this,"version",$data);
    }
    public function getVersions(){
        $id_pais_version = $_GET['id_pais'];
        $arrData = $this->model->getVersions($id_pais_version);
        // if(empty($arrData) && !empty($arrData)){
        //     $arrResponse = array('status' => false, 'msg' => 'Problemas al cargar versiones');
        // }
        // else{
        $arrResponse = array('status' => true, 'data' => $arrData,'msg' => 'Datos encontrados');
        // }
        echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function saveVersion(){
        if($_POST){
            if(empty($_POST['version']) || empty($_POST['descripcion'])){
                $arrResponse = array("status" => false, "msg" => "Datos incorrectos o vacios");
            }
            else{
                $version = strClean($_POST['version']);
                $descripcion = strClean($_POST['descripcion']);
                $id_pais_version = $_GET['id_pais'];
                $arrData = $this->model->saveVersion($version, $descripcion, $id_pais_version);
                $arrResponse = array("status" => $arrData["status"], "msg" => $arrData["msg"]);
            }
        }
        else{
            $arrResponse = array("status" => false, "msg" => "Datos incorrectos");
        }
        echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
        die();
    }
    public function changeVersions(){
        if(empty($_GET["v"])){
            $arrResponse = array("status" => false, "msg" => "Datos incorrectos o vacios");
        }
        else{
            $version = strClean($_GET["v"]);
            $id_pais_version = $_GET['id_pais'];
            $arrData = $this->model->changeVersion($version, $id_pais_version);
            $arrResponse = array("status" => $arrData["status"], "msg" => $arrData["msg"]);
        }
        echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
        die();
    }
}
?>