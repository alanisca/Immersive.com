<?php 
class errors extends Controllers{

    public function __construct(){
        parent::__construct();
        /* Valida si ya ha iniciado sesión y lo redirige a HOME */
        // session_start();
        // if(!isset($_SESSION['access_token']))
        //     header("Location:".base_url()."login");
    }

    public function notFound(){
        $data['tag_name']="IMMERSIVE - Error";
        $data['tag_page']="Error";
        $data['tag_title']="¡Pagina no encontrada!";
        $data['tag_text']="No fue posible encontrar la pagina";
        $this->views->getView($this,"error",$data);
    }
}
$notFound = new errors();
$notFound->notFound();
?>