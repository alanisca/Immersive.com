<?php

class homeModel extends mysql{
    public function __construct(){
        // echo "Mensaje desde el modelo Home<br>";
        parent::__construct();
    }
    public function getRegistros(){
        $query_select = 'SELECT 
            kn_registros.id_registro as "id_registro",
            kn_registros.id_kn_usuario as "reg_id_usuario",
            kn_registros.ios as "u_ios",
            kn_registros.dispositivo as "u_dispositivo",
            kn_registros.version_app as "u_version_app",
            kn_registros.latitud as "latitud",
            kn_registros.longitud as "longitud",
            kn_registros.version_app as "reg_version_app",
            kn_registros.fecha_registro as "reg_fecha_registro",
            kn_registros.fecha_sys as "reg_fecha_sys",
            kn_usuarios.usuario as "usuario",
            kn_usuarios.id_kn_usuario as "u_id_kn_usuario",
            kn_usuarios.grado as "u_grado",
            kn_usuarios.id_escuela as "id_escuela",
            kn_escuelas.escuela as "escuela",
            recursos_imm.id_recurso as "rec_id_recurso",
            recursos_imm.unityTarget as "rec_unityTarget",
            recursos_imm.grado as "rec_grado",
            recursos_imm.idioma as "rec_idioma",
            recursos_imm.tipo_recurso as "rec_tipo",
            IF((recursos_imm.recurso <> ""), recursos_imm.recurso, recursos_imm.unityBundle) as "recurso",
            date_format(kn_registros.fecha_sys, "%Y-%m-%d") as fecha,
            date_format(DATE_SUB(kn_registros.fecha_sys, INTERVAL 5 HOUR), "%h:%i") as fecha_hora
        FROM kn_registros
            LEFT JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
            LEFT JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
            LEFT JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
        WHERE kn_registros.fecha_registro > CURDATE()
        ORDER BY kn_registros.fecha_sys DESC';
        if($request = $this->selectAll(true, $query_select))
            return array('status' => true, 'data' => $request,'msg' => 'Datos encontrados');
        else
            return array('status' => false, 'msg' => 'Datos no encontrados');
    }
    public function getRecursos(){
        $query_select = 'SELECT recursos_imm.id_recurso,
                            recursos_imm.recurso,
                            recursos_imm.unityTarget,
                            recursos_imm.unityBundle,
                            recursos_imm.reto,
                            recursos_imm.grado,
                            recursos_imm.tipo_recurso,
                            recursos_imm.version_recurso,
                            recursos_imm.idioma,
                            recursos_imm.pathw,
                            recursos_imm.sesion,
                            recursos_imm.ciclo,
                            recursos_imm.estatus,
                            recursos_imm.fecha_creacion as fecha_creacion_recurso,
                            registros_kn.id_registro,
                            registros_kn.usuario,
                            registros_kn.escuela,
                            registros_kn.reg_id_recurso,
                            registros_kn.ubicacion,
                            registros_kn.reg_version_app,
                            registros_kn.reg_accion,
                            registros_kn.reg_fecha_registro as fecha_registro,
                            date_format(reg_fecha_registro, "%Y-%m-%d") as fecha_format
                    FROM recursos_imm 
                    LEFT JOIN registros_kn ON recursos_imm.id_recurso = registros_kn.reg_id_recurso
                    WHERE recursos_imm.estatus != "Eliminado"
                    GROUP BY recursos_imm.unityTarget';
        $request = $this->selectAll(true, $query_select);
        return $request;
    }
    public function getImmVersion(){
        $sql = "SELECT  kn_registros.version_app, 
                        COUNT(kn_registros.version_app) as tot_app, 
                        COUNT(kn_registros.version_app)/(SELECT COUNT(kn_registros.version_app)
                                                        FROM kn_registros 
                                                        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
                                                        WHERE id_registro IN (SELECT MAX(id_registro) 
                                                                            FROM kn_registros 
                                                                            GROUP BY id_kn_usuario ))*100 AS porciento
                        FROM kn_registros 
                        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
                        WHERE id_registro IN (SELECT MAX(id_registro) FROM kn_registros GROUP BY id_kn_usuario ) and kn_registros.version_app is not null
                        GROUP BY kn_registros.version_app";
        $request = $this->selectAll(true, $sql);
        return $request; 
    }
    
    public function setPrueba(string $nombre, int $edad){
        $query_insert = "INSERT INTO prueba(nombre, edad) VALUES(?,?)";
        $arrData = array($nombre, $edad);
        $request_insert = $this->insert(true, $query_insert, $arrData);
        return $request_insert;
    }
    public function getPrueba($id){
        $sql = "SELECT * FROM prueba WHERE id = $id";
        $request = $this->select(true, $sql);
        return $request;
    }
    public function updatePrueba($id,$nombre,$edad){
        $sql = "UPDATE prueba SET nombre = ?, edad = ? WHERE id = $id";
        $arrData = array($nombre,$edad);
        $request = $this->update(true, $sql, $arrData);
        return $request;
    }
    public function getPruebas(){
        $sql = "SELECT * FROM prueba";
        $arrData = $this->selectAll(true, $sql);
        return $arrData;
    }
    public function deletePrueba($id){
        $sql = "DELETE FROM prueba where id = $id";
        $arrData = $this->delete(true, $sql);
        return $arrData;
    }
}

?>