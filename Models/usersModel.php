<?php
class usersModel extends mysql{
    public function __construct(){
        parent::__construct();
    }
    public function selectUsers(){
        $sql = "SELECT id_usuario, imagen, usuario, correo, rol FROM usuarios";
        $request = $this->selectAll($sql);
        $this->bitacora(array("CONFIGURACION: USUARIOS",$_SESSION['id_user']));
        return $request;
    }
    public function selectUser($id){
        $sql = "SELECT * FROM usuarios WHERE id_usuario = $id;";
        if($request = $this->select(true, $sql)){
            return array("status"=>true,"message"=>"Se encontraron los datos del usuario","data"=>$request);
        }
        else{
            return array("status"=>false,"message"=>"No se encontró el usuario","data"=>[]);
        }
    }
    public function editUser(array $arrData){
        $sql = "SELECT count(*) as total_usuarios FROM usuarios WHERE id_usuario = ".$arrData['idUsuario'];
        if($request = $this->selectAll(true, $sql)){
            $queryUpdate =  "UPDATE usuarios SET rol_Immersive = ?, rol_Interactives = ?, rol = ? WHERE (id_usuario = ".$arrData['idUsuario'].")";
            $arrUpdate = Array($arrData['accesoImm'],$arrData['accesoInt'],$arrData['tipoUsuario']);
            if($request = $this->update(true, $queryUpdate, $arrUpdate)){
                $this->bitacora(array("CONFIGURACION: Editar usuario ".$id,$_SESSION['id_user']));
                return array("status" => true, "msg"=>"Usuario editado con exito");
            }
            else{
                return array("status" => false, "msg"=>"no se pued editar usuario");
            }
    
        }
        else{
            return array("status"=>false, "msg"=>"Usuario no existe");
        }
    }
}
?>