<?php

class getFileImmModel extends mysql{
    public function __construct(){
        // echo "Mensaje desde el modelo Home<br>";
        parent::__construct();
    }
    public function getFile(string $fileName){
        $fileName = strClean($fileName);
        $local_file = 'KnImmersiveVideos/'.$fileName;
        if(file_exists($local_file) && $fileName != ""){
            $this->bitacora(array("IMMERSIVE_APP: Video encontrado: ".$local_file,null));
            return array("val" => true, "url" => BASE_URL.$local_file);
        }
        else{
            $this->bitacora(array("IMMERSIVE_APP: Video no encontrado: ".$local_file,null));
            return array("val" => false,"url" => "error 404");
        }
    }
}

?>