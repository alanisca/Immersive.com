<?php
class usuariosModel extends mysql{
    public function __construct(){
        parent::__construct();
    }
    public function getUsuarios(){
        $sql = "SELECT * FROM KnImmersive.kn_usuarios;";
        $request = $this->selectAll(true, $sql);
        return array("status"=>true,"data"=>$request);
    }
    public function getUsuario($id_kn_usuario){
        $sql = "SELECT  kn_escuelas.escuela, 
                        kn_escuelas.id_escuela, 
                        kn_usuarios.usuario, 
                        kn_usuarios.nombre, 
                        kn_usuarios.apellido
                FROM kn_usuarios
                INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
                WHERE kn_usuarios.id_kn_usuario = $id_kn_usuario
                ORDER BY kn_usuarios.id_kn_usuario;";
        $request = $this->select(true, $sql);
        if($request){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "txt"=>"Usuario no encontrado");
        }
    }
    public function getRegistrosUsuarioReto($id_kn_usuario){
        $sql = "SELECT  kn_registros.id_kn_usuario as id_kn_usuario,
                count(distinct if(recursos_imm.reto = 1, recursos_imm.unityTarget, null)) as reto1, 
                count(distinct if(recursos_imm.reto = 2, recursos_imm.unityTarget, null)) as reto2, 
                count(distinct if(recursos_imm.reto = 3, recursos_imm.unityTarget, null)) as reto3, 
                count(distinct if(recursos_imm.reto = 4, recursos_imm.unityTarget, null)) as reto4, 
                count(distinct if(recursos_imm.reto = 5, recursos_imm.unityTarget, null)) as reto5, 
                count(distinct if(recursos_imm.reto = 6, recursos_imm.unityTarget, null)) as reto6, 
                count(distinct if(recursos_imm.reto = 7, recursos_imm.unityTarget, null)) as reto7, 
                count(distinct if(recursos_imm.reto = 8, recursos_imm.unityTarget, null)) as reto8, 
                count(distinct if(recursos_imm.reto = 0 OR isnull(recursos_imm.reto), recursos_imm.unityTarget, null)) as reto0,
                count(distinct recursos_imm.unityTarget) as total
        FROM kn_registros
        INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
        WHERE kn_registros.id_kn_usuario = $id_kn_usuario
        GROUP BY kn_registros.id_kn_usuario
        ORDER BY total desc;";
        $request = $this->select(true, $sql);
        if($request){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "txt"=>"Usuario no encontrado");
        }
    }

    public function getRegistrosUsuario($id_kn_usuario){
        $sql = "SELECT  recursos_imm.recurso,
                recursos_imm.id_recurso,
                recursos_imm.unityTarget,
                kn_registros.version_app, 
                kn_registros.dispositivo,
                kn_registros.ios,
                Period.name as period,
                kn_registros.fecha_registro
        FROM kn_registros 
        INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
        LEFT JOIN Period ON kn_registros.idPeriod_int = Period.idPeriod_int
        WHERE id_kn_usuario = $id_kn_usuario
        ORDER BY kn_registros.fecha_registro desc;";
        $request = $this->selectAll(true, $sql);
        if($request){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "txt"=>"Usuario no encontrado");
        }
    }
}
?>