<?php
class recordModel extends mysql{
    private $userName;
    private $unityTarget;
    private $accion;
    private $school;
    private $country;
    private $id_country;
    private $date;
    private $latitude;
    private $longitude;
    private $version_app;
    private $dispositivo;
    private $ios;
    private $escuela_url;
    private $idUserInt;
    private $idPeriodInt;
    private $Challenge;
    private $idResourceInt;
    
    private $idUser_int;
    private $idChallenge_int;
    private $idResource_int;
    private $appVersion;
    private $device;
    private $iosDevice;
    private $createTime;
    private $updateTime;


    public function __construct(){
        parent::__construct();
    }

    /* Nuevo registro */
    public function newRecord($arrayRecord){
        $isUser = false;
        $isUnitytarget = false;
        $this->userName = $arrayRecord["userName"];
        $this->unityTarget = $arrayRecord["unityTarget"];
        // $this->isUnitytarget = $arrayRecord["unityTarget"];
        $this->accion = $arrayRecord["accion"];
        $this->date =  date('Y/m/d h:i:s',strtotime($arrayRecord["date"]));
        $this->version_app =  $arrayRecord["version_app"];
        $this->dispositivo = $arrayRecord["dispositivo"];
        $this->ios = $arrayRecord["ios"];
        $this->escuela_url = $arrayRecord["escuela"];
        $this->idPeriodInt = $arrayRecord["idPeriodInt"];
        $this->idUserInt = $arrayRecord["idUserInt"];
        $this->Challenge = $arrayRecord["Challenge"];
        if ($this->userName != "") {//Tiene ID CORRECTO
            $sqluser = "SELECT id_kn_usuario,id_escuela FROM kn_usuarios WHERE usuario = '{$this->userName}'";
            if($requestIdUser = $this->select(true, $sqluser)){
                /* UPDATE escuela */
                if($this->escuela_url != ""){
                    $sqlescuela = "SELECT id_escuela FROM kn_escuelas WHERE escuela = '{$this->escuela_url}'";
                    if($requestIdescuela = $this->select(true, $sqlescuela)){
                        if($requestIdescuela["id_escuela"] != $requestIdUser["id_escuela"] ){
                            $sqlEscuelaUpdate = "UPDATE kn_usuarios SET id_escuela = ? WHERE (`id_kn_usuario` = ".$requestIdUser['id_kn_usuario'].")";
                            $request = $this->update(true, $sqlEscuelaUpdate, Array($requestIdescuela["id_escuela"]));
                            // $this->updateAWS($sqlEscuelaUpdate, Array($requestIdescuela["id_escuela"]));//AWS
                        }
                    }
                }
                $isUser = true;
            }
            if( $isUser &&  !is_null($this->idUserInt) && $this->idUserInt != '' ) $this->updateKnUsuario($requestIdUser["id_kn_usuario"], $this->idUserInt);
            if ($this->accion == "instanciar"){
                $sqlRecurso = "SELECT * FROM recursos_imm WHERE unityTarget = '$this->unityTarget'";
                if($requestIdRecurso = $this->select(true, $sqlRecurso)){
                    $isUnitytarget = true;
                    if ($requestIdRecurso["estatus"] != "En linea") {
                        $queryUpdateLinea = "UPDATE recursos_imm SET estatus = ? WHERE id_recurso = ".$requestIdRecurso["id_recurso"];
                        $request = $this->update(true, $queryUpdateLinea, Array("En linea"));
                        // $this->updateAWS($queryUpdateLinea, Array("En linea"));
                    }
                }
            }
            else{
                /* login y logout */
                $isUnitytarget = true;
            }
            
            $this->bitacora(array("idPeriod_Int: accion:".$this->accion.' IdUser:'.$requestIdUser["id_kn_usuario"].' IdRecurso:'. $requestIdRecurso["id_recurso"].' date:'.$this->date.' version_app:'.$this->version_app.' dispositivo:'.$this->dispositivo.' ios:'.$this->ios.' idPeriodInt:'.$this->idPeriodInt,null));
            if($isUser && $isUnitytarget && $this->accion == "instanciar"){
                $query_insert = "INSERT INTO kn_registros(id_kn_usuario, id_recurso, accion, fecha_registro, version_app, dispositivo, ios, idPeriod_Int) VALUES(?,?,?,?,?,?,?,?)";
                $arrData = array($requestIdUser["id_kn_usuario"], $requestIdRecurso["id_recurso"], $this->accion, $this->date, $this->version_app,$this->dispositivo,$this->ios,$this->idPeriodInt);
                $request_insert = $this->insert(true, $query_insert, $arrData);
                $exportData = $this->exportRegistro($request_insert);
            }
        }
        return array("username"=>$isUser,"unityTarget"=>$isUnitytarget,"newRecord"=>$isUser && $isUnitytarget,"exportData"=>$exportData);
    }


    /* Nuevo recurso */
    private $unitybundle;
    public function selectUnity($arrayUnityTarget){
        $this->isUnitytarget = $arrayUnityTarget["unitytarget"];
        $this->unitybundle = $arrayUnityTarget["unitybundle"];
        $sqlunityTarget = "SELECT id_recurso FROM recursos_imm WHERE unityTarget = '{$this->isUnitytarget}'";
        if($requestIdRecurso = $this->select(true, $sqlunityTarget)){
            return array("id_recurso"=>$requestIdRecurso["id_recurso"],"newUnitytarget"=>false);
        }
        else{
            $query_insert = "INSERT INTO recursos_imm( unityTarget, unityBundle, comentarios, origen) VALUES(?,?,?,?)";
            $arrData = array($this->isUnitytarget, $this->unitybundle, "Creado desde aplicación","Aplicación");
            $request_insert = $this->insert(true, $query_insert, $arrData);
            return array("id_recurso"=>$request_insert,"newRecurso"=>true);
        }
    }

    public function newTargetModel($arrayTarget){
        $this->isUnitytarget = $arrayTarget[0];
        $sqlunityTarget = "SELECT id_recurso FROM recursos_imm WHERE unityTarget = '{$this->isUnitytarget}'";
        if($requestIdRecurso = $this->select(true, $sqlunityTarget)){
            return array("id_kn_usuario"=>$requestIdRecurso["id_recurso"],"newUnitytarget"=>false);
        }
        else{
            $query_insert = "INSERT INTO recursos_imm(unityTarget,recurso,hoja,unityBundle,reto,grado,tipo_recurso,version_recurso,idioma,pathw,sesion,ciclo,comentarios,origen) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $request_insert = $this->insert(true, $query_insert, $arrayTarget);
            return array("id_recurso"=>$request_insert,"newRecurso"=>true);
        }
    }
    /* Nuevo usuario */
    private $nombre;
    private $apellido;
    private $escuela;
    private $pais;
    private $grado;
    private $tipo;

    public function selectUser($arrayUser){
        $this->userName = $arrayUser["userName"];
        $this->nombre = $arrayUser["nombre"];
        $this->apellido = $arrayUser["apellido"];
        $this->escuela = $arrayUser["escuela"];
        $this->pais = $arrayUser["pais"];
        $this->grado = $arrayUser["grado"];
        $this->tipo = $arrayUser["tipo"];
        if ($this->userName != "") {
            $sqluser = "SELECT id_kn_usuario FROM kn_usuarios WHERE usuario = '{$this->userName}'";
            if($requestIdUser = $this->select(true, $sqluser)){
                return array("id_kn_usuario"=>$requestIdUser["id_kn_usuario"],"newUser"=>false);
            }
            else{
                $idSchool = $this->selectSchool($this->escuela, $this->pais);
                $query_insert = "INSERT INTO kn_usuarios( id_escuela, usuario, nombre, apellido, grado, tipo) VALUES(?,?,?,?,?,?)";
                $arrData = array($idSchool, $this->userName, $this->nombre, $this->apellido, $this->grado, $this->tipo);
                $request_insert = $this->insert(true, $query_insert, $arrData);
                return array("id_kn_usuario"=>$request_insert,"newUser"=>true);
            }
        }
    }
    public function selectSchool($escuela, $pais){
        $this->school = $escuela;
        $this->country = $pais;
        $this->id_country = $this->selectPais($this->country);
        $sql = "SELECT * FROM kn_escuelas WHERE escuela = '{$this->school}' and id_pais = {$this->id_country}";
        if ($request = $this->select(true, $sql)) {
            return $request["id_escuela"];
        }
        else{
            $query_insert = "INSERT INTO kn_escuelas(escuela, id_pais) VALUES(?,?)";
            $arrData = array($this->school, $this->id_country);
            $request_insert = $this->insert(true, $query_insert, $arrData);
            return $request_insert;
        }
    }
    public function selectPais($pais){
        $sql = "SELECT id_pais FROM kn_pais WHERE pais = '{$pais}'";
        if($request = $this->select(true, $sql)){
            return $request["id_pais"];
        }
        else{
            $query_insert = "INSERT INTO kn_pais(pais) VALUES(?)";
            $arrData = array($pais);
            $request_insert = $this->insert(true, $query_insert, $arrData);
            return $request_insert;
        }
    }
    public function pruebaRecord($arrayRecord){
        $this->bitacora(array("Prueba: {Contador:".$arrayRecord["contador"].", Servicio:".$arrayRecord["cadena"]."}",null));
        return array("Respuesta"=>$arrayRecord["contador"]);
    }

    public function imm_version_model(int $id_pais){
        $sql = "SELECT version FROM kn_version WHERE status = 1 and id_pais = $id_pais limit 1";
        if($request = $this->select(true, $sql)){
            return $request["version"];
        }
    }
    public function getRegistros_model(){
        $sql = "SELECT * FROM REGISTROS_KN WHERE usuario >= 'a' limit 100000";
        if($request = $this->selectAll(true, $sql)){
            return $request;
        }
    }
    public function updateKnUsuario($idKnUsuario, $idUser_int){
        $sql = "SELECT id_kn_usuario, idUser_int FROM kn_usuarios WHERE idUser_int = $idUser_int";
        if($request = $this->selectAll(true, $sql)){
            return $request;
        }
        else{
            $sqlUpdate = "UPDATE kn_usuarios SET idUser_int = ? WHERE id_kn_usuario = $idKnUsuario";
            $request = $this->update(true, $sqlUpdate, Array($idUser_int));
            return $request;
        }
    }
    public function newRecursoUsuario($idRecurso, $idUsuario){

    }
    public function get_graficaVersionIMM(){
        $sql = "SELECT kn_registros.version_app, COUNT(kn_registros.version_app) as tot_app, 
                COUNT(kn_registros.version_app)/(SELECT COUNT(kn_registros.version_app)
            FROM kn_registros 
            INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
            WHERE id_registro IN (SELECT MAX(id_registro) FROM kn_registros GROUP BY id_kn_usuario ))*100 AS porciento
            FROM kn_registros 
            INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
            WHERE id_registro IN (SELECT MAX(id_registro) FROM kn_registros GROUP BY id_kn_usuario ) and kn_registros.version_app is not null
            GROUP BY kn_registros.version_app
            ORDER BY version_app desc";
        $request = $this->selectAll(true, $sql);
        return $request; 
    }
    public function get_versionIMM(){
        $sql = "SELECT version FROM kn_version WHERE id_pais = 1 AND status = 1";
        $request = $this->select(true, $sql);
        return $request;
    }
    public function get_totRecursos(){
        $sql = "SELECT count(1) as total_recursos FROM recursos_imm WHERE estatus = 'En linea'";
        $request = $this->select(true, $sql);
        return $request; 
    }
    public function get_totEscuelas(){
        $sql = "SELECT COUNT(1) as total_escuelas FROM kn_escuelas";
        $request = $this->select(true, $sql);
        return $request; 
    }
    public function get_totAlumnos(){
        $sql = "SELECT COUNT(1) as tot_usuarios FROM kn_usuarios";
        $request = $this->select(true, $sql);
        return $request; 
    }

    public function exportRegistro(int $idRegistro){
        $sql = "SELECT 	kn_usuarios.usuario,
                        kn_pais.id_pais,
                        kn_usuarios.idUser_int as idUser_int,
                        recursos_imm.idResource_int as idResource_int,
                        recursos_imm.unityTarget as unityTarget,
                        kn_registros.version_app as appVersion,
                        kn_registros.dispositivo as device,
                        kn_registros.ios as iosDevice,
                        kn_registros.fecha_registro as createTime,
                        kn_registros.idPeriod_int
                FROM kn_registros
                    INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
                    INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
                    INNER JOIN kn_pais ON kn_escuelas.id_pais = kn_pais.id_pais
                    INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
                    INNER JOIN Period ON Period.idPeriod_int = kn_registros.idPeriod_int
                WHERE id_registro = $idRegistro";
        if($request = $this->select(true, $sql)){
            $error = null;
            /* Verificar idUser_int */
            if( $request["idUser_int"] !== null && !empty($request["idUser_int"]) ) $this->idUser_int = $request["idUser_int"];
            else{
                $requestUserID = $this->getIdUserInt($request["usuario"]);
                if($requestUserID["status"]) $this->idUser_int = $requestUserID["data"];
                else $error = $error.' idUser_int ';
            }
            /* Verificar idResource_int */
            if( $request["idResource_int"] !== null && !empty($request["idResource_int"]) ) $this->idResource_int = $request["idResource_int"];
            else{
                $requestResourceInt = $this->getidResourceInt($request["unityTarget"]);
                if($requestResourceInt.status) $this->idResource_int = $requestResourceInt["data"].idUser_int;
                else $error = $error.' idResource_int ';
            }
            $this->idChallenge_int =  $this->getIdChallenge((int)$this->idResource_int, (int)$request["id_pais"], (string)$request["createTime"], (int)$request["idPeriod_int"]);
            // $this->idChallenge_int = $this->getIdChallenge(191110, 2, "2021-08-05 10:25:22");
            if((bool)$this->idChallenge_int["status"])
                $this->idChallenge_int = $this->idChallenge_int["data"][0];
            else $error = $error.' idChallenge_int ';
            $this->appVersion = $request["appVersion"];
            $this->device = $request["device"];
            $this->iosDevice = $request["iosDevice"];
            $this->createTime = $request["createTime"];
            if($error === null){
                $query_insert =  "INSERT INTO immersiveRegister (`idUser_int`, `idChallenge_int`, `idResource_int`, `appVersion`, `device`, `iosDevice`, `createTime`) VALUES (?,?,?,?,?,?,?)";
                $arrInsert = Array($this->idUser_int,
                                    $this->idChallenge_int["idChallenge_int"],
                                    $this->idResource_int,
                                    $this->appVersion,
                                    $this->device,
                                    $this->iosDevice,
                                    $this->createTime);
                $request_insert = $this->insert(true ,$query_insert, $arrInsert);

                $arrUpdate = Array('Exportado');
            }
            else{
                $arrUpdate = Array('Error:'.$error);
            }
            $queryUpdate = "UPDATE kn_registros SET `latitud` = ? WHERE id_registro = $idRegistro";
            $requestUpDate = $this->update(true, $queryUpdate, $arrUpdate);
            if( $requestUpDate = $this->update(true, $queryUpdate, $arrUpdate))
                return array("status" => true, "msg"=>"Registro exportado", "Arreglo"=>$arrUpdate);
            else 
                return array("status" => false, "msg"=>$arrUpdate, "data"=>array("Registro"=>$request));
        }
    }
    public function getIdUserInt(string $userName){
        $sql = "SELECT idUser_int FROM User WHERE userName = '$userName'";
        if($request = $this->selectAll(false, $sql)){
            return array("status"=>true,"msg"=>"Usuario encontrado","data"=>$request["idUser_int"]);
        }
        else{
            return array("status"=>false, "msg"=>"Usuario no encontrado");
        }
    }
    public function getIdResourceInt(string $unityTarget){
        $sql = "SELECT idResource_int FROM Resource WHERE unityTarget = $unityTarget";
        if($request = $this->selectAll(false, $sql)){
            return array("status"=>true,"msg"=>"Recurso encontrado","data"=>$request);
        }
        else{
            return array("status"=>false, "msg"=>"Recurso no encontrado");
        }
    }
    public function getIdChallenge(int $idResource_int, int $idCountry_int, string $createTime, int $idPeriod_int){
        $sql = "SELECT Challenge.idChallenge_int as idChallenge_int
                FROM Resource
                    INNER JOIN Challenge_UnityBundle ON Resource.idResource_int = Challenge_UnityBundle.idResource_int
                    INNER JOIN Challenge ON Challenge_UnityBundle.idChallenge_int = Challenge.idChallenge_int
                    INNER JOIN Period ON Challenge.idPeriod_int = Period.idPeriod_int
                WHERE (Resource.idResource_int = $idResource_int AND Period.idCountry_int = $idCountry_int AND '$createTime' BETWEEN Period.startDate AND Period.endDate) 
                OR (Period.idPeriod_int = $idPeriod_int AND Resource.idResource_int = $idResource_int AND Period.idCountry_int = $idCountry_int)";
        if($request = $this->selectAll(false, $sql)){
            return array("status"=>true,"msg"=>"Challenge encontrado","data"=>$request);
        }
        else{
            return array("status"=>false, "msg"=>"Challenge no encontrado");
        }
    }
}
?>