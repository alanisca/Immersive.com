<?php
class catalogoImmModel extends mysql{
    public function __construct(){
        parent::__construct();
    }
    public function getIMMS(){
        $sql = "SELECT * FROM recursos_imm WHERE Estatus != 'Cancelada' ORDER BY reto, grado, sesion";
        $request = $this->selectAll(true, $sql);
        $this->bitacora(array("IMMERSIVE: CATALOGO",$_SESSION['id_user']));
        return $request;
    }
    public function selectReqReto(array $arrayRecurso){
        $where = "";
        $whereTexto = "";
        $whereReto = "";
        $whereGrado = "";
        $whereTipo = "";
        $whereVersion = "";
        $whereEstatus = "";
        if(isset($arrayRecurso["Texto"]) && !is_null($arrayRecurso["Texto"]) && !empty($arrayRecurso["Texto"]) ){
            $whereTexto = 'recurso like "%'.$arrayRecurso["Texto"].'%" or unityTarget like "%'.$arrayRecurso["Texto"].'%" or unityBundle like "%'.$arrayRecurso["Texto"].'%"';
        }
        if(isset($arrayRecurso["Reto"]) && !is_null($arrayRecurso["Reto"]) && !empty($arrayRecurso["Reto"]) ){
            $tags = explode(',' , $arrayRecurso["Reto"]);
            foreach($tags as $i =>$key) {
                if($whereReto != "") $whereReto .= " or ";
                $whereReto .= "recursos_imm.reto=".$key;
            }
        }
        if(isset($arrayRecurso["Grado"]) && !is_null($arrayRecurso["Grado"]) && !empty($arrayRecurso["Grado"]) ){
            $tags = explode(',' , $arrayRecurso["Grado"]);
            foreach($tags as $i =>$key) {
                if($whereGrado != "") $whereGrado .= " or ";
                $whereGrado .= "recursos_imm.grado='".$key."'";
            }
        }
        if(isset($arrayRecurso["Tipo"]) && !is_null($arrayRecurso["Tipo"]) && !empty($arrayRecurso["Tipo"]) ){
            $tags = explode(',' , $arrayRecurso["Tipo"]);
            foreach($tags as $i =>$key) {
                if($whereTipo != "") $whereTipo .= " or ";
                $whereTipo .= "recursos_imm.tipo_recurso='".$key."'";
            }
        }
        if(isset($arrayRecurso["Version"]) && !is_null($arrayRecurso["Version"]) && !empty($arrayRecurso["Version"]) ){
            $tags = explode(',' , $arrayRecurso["Version"]);
            foreach($tags as $i =>$key) {
                if($whereVersion != "") $whereVersion .= " or ";
                $whereVersion .= "recursos_imm.version_recurso='".$key."'";
            }
        }
        if(isset($arrayRecurso["Estatus"]) && !is_null($arrayRecurso["Estatus"]) && !empty($arrayRecurso["Estatus"]) ){
            $tags = explode(',' , $arrayRecurso["Estatus"]);
            foreach($tags as $i =>$key) {
                if($whereEstatus != "") $whereEstatus .= " or ";
                $whereEstatus .= "recursos_imm.estatus='".$key."'";
            }
        }

        if($whereTexto != "") $where .= " and (".$whereTexto.")";
        if($whereReto != "") $where .= " and (".$whereReto.")";
        if($whereGrado != "") $where .= " and (".$whereGrado.")";
        if($whereTipo != "") $where .= " and (".$whereTipo.")";
        if($whereVersion != "") $where .= " and (".$whereVersion.")";
        if($whereEstatus != "") $where .= " and (".$whereEstatus.")";

        // $sql = "SELECT recursos_imm.*, count(kn_registros.id_recurso) as registros FROM recursos_imm 
        // LEFT JOIN kn_registros ON recursos_imm.id_recurso = kn_registros.id_recurso
        // WHERE ( recursos_imm.estatus != 'Eliminado' )" ;
        $sql = "SELECT recursos_imm.* FROM recursos_imm 
        WHERE ( recursos_imm.estatus != 'Eliminado' )" ;
        $orderSQL = " group by recursos_imm.id_recurso ORDER BY FIELD(recursos_imm.grado, 'K1','K2','K3','PF','E1','E2','E3','E4','E5','E6','M7','M8','M9'), recursos_imm.reto, recursos_imm.unityTarget";
        $request = $this->selectAll(true, $sql.$where." OR ( (recursos_imm.grado is null OR recursos_imm.grado = '' OR recursos_imm.reto = '' OR recursos_imm.reto is null) and (recursos_imm.estatus != 'Eliminado') )".$orderSQL);
        // echo $sql.$where." OR ( (recursos_imm.grado is null OR recursos_imm.grado = '' OR recursos_imm.reto = '' OR recursos_imm.reto is null) and (recursos_imm.estatus != 'Eliminado') )".$orderSQLç
        return $request;
    }
    public function newRecurso($arrData){
        $whereRec = "";
        $whereUnitytarget = "";
        $whereUnitybundle = "";
        $whereRecurso = "";
        if ($arrData['unityTarget'] != "") {
            // $whereUnitytarget = "unityTarget = '".$arrData['unityTarget']."'";
            $whereRec = " unityTarget = '".$arrData['unityTarget']."'";
        }
        if ($arrData['unityBundle'] != "") {
            if ($whereRec != "") $whereRec .= " or ";
            $whereRec .= "unityBundle = '".$arrData['unityBundle']."'";
        }
        if ($arrData['titulo'] != "") {
            if ($whereRec != "") $whereRec .= " and ";
            $whereRec .= " (recurso = '".$arrData["titulo"]."' and grado = '".$arrData["grado"]."')";
        }

        $sql = "SELECT * FROM recursos_imm WHERE estatus != 'Cancelada' and (".$whereRec.")";

        // echo $sql;
        if($request = $this->selectAll(true, $sql)){
            // dep($request);
            return array("status"=>false,"txt"=>"Recurso ya existe");
        }
        $query_insertRecurso =  "INSERT INTO recursos_imm(id_requisicion,
                                            unityTarget,
                                            recurso,
                                            unityBundle,
                                            reto,
                                            grado,
                                            tipo_recurso,
                                            version_recurso,
                                            idioma,
                                            pathw,
                                            sesion,
                                            ciclo,
                                            liga_script,
                                            liga_autoria,
                                            liga_wf,
                                            comentarios,
                                            version,
                                            estatus) 
                                    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $arrInsertRecurso = Array(
            $arrData["requisicion"],
            $arrData["unityTarget"],
            $arrData["titulo"],
            $arrData["unityBundle"],
            $arrData["reto"],
            $arrData["grado"],
            $arrData["tipo_recurso"],
            $arrData["version_recurso"],
            $arrData["idioma"],
            $arrData["pathway"],
            $arrData["sesion"],
            $arrData["ciclo"],
            $arrData["url_script"],
            $arrData["url_autoria"],
            $arrData["url_workflow"],
            $arrData["comentarios"],
            $arrData["version"],
            $arrData["estatus"]
        );
        $request_insert = $this->insert(true, $query_insertRecurso, $arrInsertRecurso);
        $arrReturn = array("value"=>true,
                            "status"=>$request_insert,
                            "unityTarget"=>$arrData["unityTarget"],
                            "recurso"=>$arrData["titulo"],
                            "grado"=>$arrData["grado"],
                            "tipo_recurso"=>$arrData["tipo_recurso"],
                            "version_recurso"=>$arrData["version_recurso"],
                            "idioma"=>$arrData["idioma"],
                            "estatus"=>$arrData["estatus"],
                            "id_recurso"=>$arrData["id_recurso"]);
        return $arrReturn;
    }
    public function updateRecurso(array $arrData){
        if($arrData["id_recurso"]>0){
            $queryUpdateRecurso =  "UPDATE recursos_imm 
                                    SET id_requisicion = ?,
                                        unityTarget = ?,
                                        recurso = ?,
                                        unityBundle = ?,
                                        reto = ?,
                                        grado = ?,
                                        tipo_recurso = ?,
                                        version_recurso = ?,
                                        idioma = ?,
                                        pathw = ?,
                                        sesion = ?,
                                        ciclo = ?,
                                        liga_script = ?,
                                        liga_autoria = ?,
                                        liga_wf = ?,
                                        comentarios = ?,
                                        version = ?,
                                        estatus = ?
                                    WHERE id_recurso = ".$arrData["id_recurso"]."";
            $arrUpdateDate = Array(
                $arrData["requisicion"],
                $arrData["unityTarget"],
                $arrData["titulo"],
                $arrData["unityBundle"],
                $arrData["reto"],
                $arrData["grado"],
                $arrData["tipo_recurso"],
                $arrData["version_recurso"],
                $arrData["idioma"],
                $arrData["pathway"],
                $arrData["sesion"],
                $arrData["ciclo"],
                $arrData["url_script"],
                $arrData["url_autoria"],
                $arrData["url_workflow"],
                $arrData["comentarios"],
                $arrData["version"],
                $arrData["estatus"]
            );
            $request = $this->update(true, $queryUpdateRecurso, $arrUpdateDate);
            $arrReturn = array("status"=>$request,
                                "unityTarget"=>$arrData["unityTarget"],
                                "recurso"=>$arrData["titulo"],
                                "grado"=>$arrData["grado"],
                                "tipo_recurso"=>$arrData["tipo_recurso"],
                                "version_recurso"=>$arrData["version_recurso"],
                                "idioma"=>$arrData["idioma"],
                                "estatus"=>$arrData["estatus"],
                                "id_recurso"=>$arrData["id_recurso"]);
            return $arrReturn;
        }
        else{
            return array("status"=>false,"message"=>"No cuenta con ID");
        }
    }
    public function deleteRecurso(int $arrData){
        $queryDeleteRecurso =  "UPDATE recursos_imm 
                                SET estatus = ?
                                WHERE id_recurso = ".$arrData;
        $arrDeleteDate = Array("Eliminado");
        $request = $this->update(true, $queryDeleteRecurso, $arrDeleteDate);
        return array("status"=>true,"registros"=>$request);
    }
    public function getVersions(int $id_pais = 1){
        $sql = "SELECT * FROM kn_version WHERE id_pais = $id_pais ORDER BY version desc";
        $request = $this->selectAll(true, $sql);
        return $request;
    }
    public function saveVersion(string $version, string $descripcion, int $id_pais = 1){
        $sql = "SELECT * FROM kn_version WHERE version = '".$version."' and id_pais = ".$id_pais;
        if($request = $this->selectAll(true, $sql)){
            return array("status"=>false, "msg"=>" Versión ya existe");
        }
        else{
            $queryUpdate =  "UPDATE kn_version SET status = ? WHERE id_pais = ".$id_pais;
            $arrUpdate = Array(0);
            $request = $this->update(true, $queryUpdate, $arrUpdate);

            $query_insert =  "INSERT INTO kn_version(version,descripcion,status,id_pais) VALUES (?,?,?,?)";
            $arrInsert = Array($version,$descripcion,1,$id_pais);
            $request_insert = $this->insert(true, $query_insert, $arrInsert);
            $this->bitacora(array("IMM: Nueva version:".$version,$_SESSION['id_user']));
            return array("status" => true, "msg"=>"Versión agregada");
        }
    }
    public function changeVersion(int $version, int $id_pais = 1){
        $queryUpdate =  "UPDATE kn_version SET status = ? WHERE id_pais = ".$id_pais;
        $arrUpdate = Array(0);
        $request = $this->update(true, $queryUpdate, $arrUpdate);

        $queryUpdate =  "UPDATE kn_version SET status = ? WHERE id_version = ".$version." and id_pais = ".$id_pais;
        $arrUpdate = Array(1);
        $request = $this->update(true, $queryUpdate, $arrUpdate);

        $this->bitacora(array("IMM: Cambio de version:".$version.", ID:".$version ,$_SESSION['id_user']));
        return array("status" => true, "msg"=>"Cambio de versión");
    }
}
?>