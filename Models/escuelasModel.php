<?php
class escuelasModel extends mysql{
    public function __construct(){
        parent::__construct();
    }
    public function getEscuelas(){
        $sql = "SELECT  kn_escuelas.id_escuela as id_escuela,
                        kn_escuelas.id_pais as id_pais,
                        kn_escuelas.Escuela as escuela, 
                        count(distinct if(recursos_imm.reto = 1, recursos_imm.unityTarget, null)) as reto1, 
                        count(distinct if(recursos_imm.reto = 2, recursos_imm.unityTarget, null)) as reto2, 
                        count(distinct if(recursos_imm.reto = 3, recursos_imm.unityTarget, null)) as reto3, 
                        count(distinct if(recursos_imm.reto = 4, recursos_imm.unityTarget, null)) as reto4, 
                        count(distinct if(recursos_imm.reto = 5, recursos_imm.unityTarget, null)) as reto5, 
                        count(distinct if(recursos_imm.reto = 6, recursos_imm.unityTarget, null)) as reto6, 
                        count(distinct if(recursos_imm.reto = 7, recursos_imm.unityTarget, null)) as reto7, 
                        count(distinct if(recursos_imm.reto = 8, recursos_imm.unityTarget, null)) as reto8, 
                        count(distinct if(recursos_imm.reto = 0, recursos_imm.unityTarget, null)) as reto0,
                        count(distinct recursos_imm.unityTarget) as total
                FROM kn_registros
                INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
                INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
                INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
                WHERE kn_registros.fecha_sys > '2022-09'
                GROUP BY kn_escuelas.id_escuela
                ORDER BY total desc;";
        $request = $this->selectAll(true, $sql);
        return array("status"=>true,"data"=>$request);
    }
    public function getEscuela($id_escuela){
        $sql = "SELECT * FROM kn_escuelas INNER JOIN kn_pais ON kn_escuelas.id_pais = kn_pais.id_pais WHERE id_escuela = ".$id_escuela;
        $request = $this->select(true, $sql);
        if($request){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "txt"=>"Escuela no encontrada");
        }
    }
    public function getEscuelaUsuarios($id_escuela){
        $sql = "SELECT kn_usuarios.id_kn_usuario, kn_usuarios.usuario, kn_usuarios.grado, kn_usuarios.tipo, count(distinct kn_registros.id_recurso) as recursos
        FROM kn_registros
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
        WHERE kn_usuarios.id_escuela = $id_escuela
        GROUP BY usuario
        ORDER BY kn_usuarios.grado, kn_usuarios.tipo";
        $request = $this->selectAll(true, $sql);
        if($request){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "txt"=>"Escuela no encontrada");
        }
    }
    public function getEscuelaRecursos($id_escuela){
        $sql = "SELECT 
            recursos_imm.id_recurso as 'rec_id_recurso',
            IF((recursos_imm.recurso <> ''), recursos_imm.recurso, recursos_imm.unityBundle) as 'recurso',
            recursos_imm.unityTarget as 'rec_unityTarget',
            recursos_imm.grado as 'rec_grado',
            recursos_imm.reto as 'rec_reto',
            recursos_imm.idioma as 'rec_idioma',
            recursos_imm.tipo_recurso as 'rec_tipo'
        FROM kn_registros
        INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
        WHERE kn_usuarios.id_escuela = $id_escuela
        GROUP BY kn_registros.id_recurso";
        $request = $this->selectAll(true, $sql);
        if($request){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "txt"=>"Escuela no encontrada");
        }
    }
    public function getEscuelasTop3($id_escuela){
        $sql = "(SELECT  kn_escuelas.id_escuela as id_escuela,
                kn_escuelas.id_pais as id_pais,
                kn_escuelas.Escuela as escuela, 
                count(distinct if(recursos_imm.reto = 1, recursos_imm.unityTarget, null)) as reto1, 
                count(distinct if(recursos_imm.reto = 2, recursos_imm.unityTarget, null)) as reto2, 
                count(distinct if(recursos_imm.reto = 3, recursos_imm.unityTarget, null)) as reto3, 
                count(distinct if(recursos_imm.reto = 4, recursos_imm.unityTarget, null)) as reto4, 
                count(distinct if(recursos_imm.reto = 5, recursos_imm.unityTarget, null)) as reto5, 
                count(distinct if(recursos_imm.reto = 6, recursos_imm.unityTarget, null)) as reto6, 
                count(distinct if(recursos_imm.reto = 7, recursos_imm.unityTarget, null)) as reto7, 
                count(distinct if(recursos_imm.reto = 8, recursos_imm.unityTarget, null)) as reto8, 
                count(distinct if(recursos_imm.reto = 0, recursos_imm.unityTarget, null)) as reto0,
                count(distinct recursos_imm.unityTarget) as total
        FROM kn_registros 
        INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
        WHERE kn_escuelas.id_escuela = 1
        GROUP BY kn_escuelas.id_escuela) 
        UNION
        (SELECT  kn_escuelas.id_escuela as id_escuela,
                kn_escuelas.id_pais as id_pais,
                kn_escuelas.Escuela as escuela, 
                count(distinct if(recursos_imm.reto = 1, recursos_imm.unityTarget, null)) as reto1, 
                count(distinct if(recursos_imm.reto = 2, recursos_imm.unityTarget, null)) as reto2, 
                count(distinct if(recursos_imm.reto = 3, recursos_imm.unityTarget, null)) as reto3, 
                count(distinct if(recursos_imm.reto = 4, recursos_imm.unityTarget, null)) as reto4, 
                count(distinct if(recursos_imm.reto = 5, recursos_imm.unityTarget, null)) as reto5, 
                count(distinct if(recursos_imm.reto = 6, recursos_imm.unityTarget, null)) as reto6, 
                count(distinct if(recursos_imm.reto = 7, recursos_imm.unityTarget, null)) as reto7, 
                count(distinct if(recursos_imm.reto = 8, recursos_imm.unityTarget, null)) as reto8, 
                count(distinct if(recursos_imm.reto = 0, recursos_imm.unityTarget, null)) as reto0,
                count(distinct recursos_imm.unityTarget) as total
        FROM kn_registros 
        INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
        WHERE  kn_registros.fecha_sys > '2022-09' and kn_escuelas.id_escuela != 1
        GROUP BY kn_escuelas.id_escuela
        ORDER BY total desc
        limit 0,2) 
        UNION
        (SELECT  kn_escuelas.id_escuela as id_escuela,
                kn_escuelas.id_pais as id_pais,
                kn_escuelas.Escuela as escuela, 
                count(distinct if(recursos_imm.reto = 1, recursos_imm.unityTarget, null)) as reto1, 
                count(distinct if(recursos_imm.reto = 2, recursos_imm.unityTarget, null)) as reto2, 
                count(distinct if(recursos_imm.reto = 3, recursos_imm.unityTarget, null)) as reto3, 
                count(distinct if(recursos_imm.reto = 4, recursos_imm.unityTarget, null)) as reto4, 
                count(distinct if(recursos_imm.reto = 5, recursos_imm.unityTarget, null)) as reto5, 
                count(distinct if(recursos_imm.reto = 6, recursos_imm.unityTarget, null)) as reto6, 
                count(distinct if(recursos_imm.reto = 7, recursos_imm.unityTarget, null)) as reto7, 
                count(distinct if(recursos_imm.reto = 8, recursos_imm.unityTarget, null)) as reto8, 
                count(distinct if(recursos_imm.reto = 0, recursos_imm.unityTarget, null)) as reto0,
                count(distinct recursos_imm.unityTarget) as total
        FROM kn_registros
        INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
        WHERE kn_escuelas.id_escuela = $id_escuela and kn_registros.fecha_sys > '2022-09'
        GROUP BY kn_escuelas.id_escuela
        ORDER BY total desc)
        UNION
        (SELECT '0' as id_escuela,
                '0' as id_pais,
                'PROMEDIO' as escuela,
                AVG(reto1) as reto1,
                AVG(reto2) as reto2,
                AVG(reto3) as reto3,
                AVG(reto4) as reto4,
                AVG(reto5) as reto5,
                AVG(reto6) as reto6,
                AVG(reto7) as reto7,
                AVG(reto8) as reto8,
                AVG(reto0) as reto0,
                AVG(total) as total
        FROM
        (SELECT  kn_escuelas.id_escuela as id_escuela,
                kn_escuelas.id_pais as id_pais,
                kn_escuelas.Escuela as escuela, 
                count(distinct if(recursos_imm.reto = 1, recursos_imm.unityTarget, null)) as reto1, 
                count(distinct if(recursos_imm.reto = 2, recursos_imm.unityTarget, null)) as reto2, 
                count(distinct if(recursos_imm.reto = 3, recursos_imm.unityTarget, null)) as reto3, 
                count(distinct if(recursos_imm.reto = 4, recursos_imm.unityTarget, null)) as reto4, 
                count(distinct if(recursos_imm.reto = 5, recursos_imm.unityTarget, null)) as reto5, 
                count(distinct if(recursos_imm.reto = 6, recursos_imm.unityTarget, null)) as reto6, 
                count(distinct if(recursos_imm.reto = 7, recursos_imm.unityTarget, null)) as reto7, 
                count(distinct if(recursos_imm.reto = 8, recursos_imm.unityTarget, null)) as reto8, 
                count(distinct if(recursos_imm.reto = 0, recursos_imm.unityTarget, null)) as reto0,
                count(distinct recursos_imm.unityTarget) as total
        FROM kn_registros
        INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
        WHERE recursos_imm.estatus = 'En linea' and kn_registros.fecha_sys > '2022-09'
        GROUP BY kn_escuelas.id_escuela) as a)";
        $request = $this->selectAll(true, $sql);
        return array("status"=>true,"data"=>$request);
    }
    public function getEscuelasTop3Grado($id_escuela){
        $sql = "(SELECT  kn_escuelas.id_escuela as id_escuela,
                        kn_escuelas.id_pais as id_pais,
                        kn_escuelas.Escuela as escuela, 
                        count(distinct if(recursos_imm.grado = 'K', recursos_imm.unityTarget, null)) as gradoK,
                        count(distinct if(recursos_imm.grado = 'K1', recursos_imm.unityTarget, null)) as gradoK1,
                        count(distinct if(recursos_imm.grado = 'K2', recursos_imm.unityTarget, null)) as gradoK2,
                        count(distinct if(recursos_imm.grado = 'K3', recursos_imm.unityTarget, null)) as gradoK3,
                        count(distinct if(recursos_imm.grado = 'E', recursos_imm.unityTarget, null)) as gradoE,
                        count(distinct if(recursos_imm.grado = 'E1', recursos_imm.unityTarget, null)) as gradoE1,
                        count(distinct if(recursos_imm.grado = 'E2', recursos_imm.unityTarget, null)) as gradoE2,
                        count(distinct if(recursos_imm.grado = 'E3', recursos_imm.unityTarget, null)) as gradoE3,
                        count(distinct if(recursos_imm.grado = 'E4', recursos_imm.unityTarget, null)) as gradoE4,
                        count(distinct if(recursos_imm.grado = 'E5', recursos_imm.unityTarget, null)) as gradoE5,
                        count(distinct if(recursos_imm.grado = 'E6', recursos_imm.unityTarget, null)) as gradoE6,
                        count(distinct if(recursos_imm.grado = 'M', recursos_imm.unityTarget, null)) as gradoM,
                        count(distinct if(recursos_imm.grado = 'M7', recursos_imm.unityTarget, null)) as gradoM7,
                        count(distinct if(recursos_imm.grado = 'M8', recursos_imm.unityTarget, null)) as gradoM8,
                        count(distinct if(recursos_imm.grado = 'M9', recursos_imm.unityTarget, null)) as gradoM9,
                        count(distinct if(recursos_imm.grado = 'PF', recursos_imm.unityTarget, null)) as gradoPF,
                        count(distinct if(recursos_imm.grado = 'null', recursos_imm.unityTarget, null)) as gradoNull,
                        count(distinct recursos_imm.unityTarget) as total
                FROM kn_registros
                INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
                INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
                INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
                WHERE recursos_imm.estatus = 'En linea' and kn_escuelas.id_escuela = 1
                GROUP BY kn_escuelas.id_escuela
                ORDER BY total desc) 
                UNION
                (SELECT  kn_escuelas.id_escuela as id_escuela,
                        kn_escuelas.id_pais as id_pais,
                        kn_escuelas.Escuela as escuela, 
                        count(distinct if(recursos_imm.grado = 'K', recursos_imm.unityTarget, null)) as gradoK,
                        count(distinct if(recursos_imm.grado = 'K1', recursos_imm.unityTarget, null)) as gradoK1,
                        count(distinct if(recursos_imm.grado = 'K2', recursos_imm.unityTarget, null)) as gradoK2,
                        count(distinct if(recursos_imm.grado = 'K3', recursos_imm.unityTarget, null)) as gradoK3,
                        count(distinct if(recursos_imm.grado = 'E', recursos_imm.unityTarget, null)) as gradoE,
                        count(distinct if(recursos_imm.grado = 'E1', recursos_imm.unityTarget, null)) as gradoE1,
                        count(distinct if(recursos_imm.grado = 'E2', recursos_imm.unityTarget, null)) as gradoE2,
                        count(distinct if(recursos_imm.grado = 'E3', recursos_imm.unityTarget, null)) as gradoE3,
                        count(distinct if(recursos_imm.grado = 'E4', recursos_imm.unityTarget, null)) as gradoE4,
                        count(distinct if(recursos_imm.grado = 'E5', recursos_imm.unityTarget, null)) as gradoE5,
                        count(distinct if(recursos_imm.grado = 'E6', recursos_imm.unityTarget, null)) as gradoE6,
                        count(distinct if(recursos_imm.grado = 'M', recursos_imm.unityTarget, null)) as gradoM,
                        count(distinct if(recursos_imm.grado = 'M7', recursos_imm.unityTarget, null)) as gradoM7,
                        count(distinct if(recursos_imm.grado = 'M8', recursos_imm.unityTarget, null)) as gradoM8,
                        count(distinct if(recursos_imm.grado = 'M9', recursos_imm.unityTarget, null)) as gradoM9,
                        count(distinct if(recursos_imm.grado = 'PF', recursos_imm.unityTarget, null)) as gradoPF,
                        count(distinct if(recursos_imm.grado = 'null', recursos_imm.unityTarget, null)) as gradoNull,
                        count(distinct recursos_imm.unityTarget) as total
                FROM kn_registros
                INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
                INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
                INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
                WHERE recursos_imm.estatus = 'En linea' and kn_registros.fecha_sys > '2022-09' and kn_escuelas.id_escuela != 1
                GROUP BY kn_escuelas.id_escuela
                ORDER BY total desc
                limit 2) 
                UNION
                (SELECT  kn_escuelas.id_escuela as id_escuela,
                        kn_escuelas.id_pais as id_pais,
                        kn_escuelas.Escuela as escuela, 
                        count(distinct if(recursos_imm.grado = 'K', recursos_imm.unityTarget, null)) as gradoK,
                        count(distinct if(recursos_imm.grado = 'K1', recursos_imm.unityTarget, null)) as gradoK1,
                        count(distinct if(recursos_imm.grado = 'K2', recursos_imm.unityTarget, null)) as gradoK2,
                        count(distinct if(recursos_imm.grado = 'K3', recursos_imm.unityTarget, null)) as gradoK3,
                        count(distinct if(recursos_imm.grado = 'E', recursos_imm.unityTarget, null)) as gradoE,
                        count(distinct if(recursos_imm.grado = 'E1', recursos_imm.unityTarget, null)) as gradoE1,
                        count(distinct if(recursos_imm.grado = 'E2', recursos_imm.unityTarget, null)) as gradoE2,
                        count(distinct if(recursos_imm.grado = 'E3', recursos_imm.unityTarget, null)) as gradoE3,
                        count(distinct if(recursos_imm.grado = 'E4', recursos_imm.unityTarget, null)) as gradoE4,
                        count(distinct if(recursos_imm.grado = 'E5', recursos_imm.unityTarget, null)) as gradoE5,
                        count(distinct if(recursos_imm.grado = 'E6', recursos_imm.unityTarget, null)) as gradoE6,
                        count(distinct if(recursos_imm.grado = 'M', recursos_imm.unityTarget, null)) as gradoM,
                        count(distinct if(recursos_imm.grado = 'M7', recursos_imm.unityTarget, null)) as gradoM7,
                        count(distinct if(recursos_imm.grado = 'M8', recursos_imm.unityTarget, null)) as gradoM8,
                        count(distinct if(recursos_imm.grado = 'M9', recursos_imm.unityTarget, null)) as gradoM9,
                        count(distinct if(recursos_imm.grado = 'PF', recursos_imm.unityTarget, null)) as gradoPF,
                        count(distinct if(recursos_imm.grado = 'null', recursos_imm.unityTarget, null)) as gradonull,
                        count(distinct recursos_imm.unityTarget) as total
                FROM kn_registros
                INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
                INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
                INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
                WHERE kn_escuelas.id_escuela = $id_escuela AND recursos_imm.estatus = 'En linea' and kn_registros.fecha_sys > '2022-09'
                GROUP BY kn_escuelas.id_escuela
                ORDER BY total desc)
                UNION
                (SELECT '0' as id_escuela,
                        '0' as id_pais,
                        'PROMEDIO' as escuela,
                        AVG(gradoK) as gradoK,
                        AVG(gradoK1) as gradoK1,
                        AVG(gradoK2) as gradoK2,
                        AVG(gradoK3) as gradoK3,
                        AVG(gradoE) as gradoE,
                        AVG(gradoE1) as gradoE1,
                        AVG(gradoE2) as gradoE2,
                        AVG(gradoE3) as gradoE3,
                        AVG(gradoE4) as gradoE4,
                        AVG(gradoE5) as gradoE5,
                        AVG(gradoE6) as gradoE6,
                        AVG(gradoM) as gradoM,
                        AVG(gradoM7) as gradoM7,
                        AVG(gradoM8) as gradoM8,
                        AVG(gradoM9) as gradoM9,
                        AVG(gradoPF) as gradoPF,
                        AVG(gradonull) as gradonull,
                        AVG(total) as total
                FROM
                (SELECT  kn_escuelas.id_escuela as id_escuela,
                        kn_escuelas.id_pais as id_pais,
                        kn_escuelas.Escuela as escuela, 
                        count(distinct if(recursos_imm.grado = 'K', recursos_imm.unityTarget, null)) as gradoK,
                        count(distinct if(recursos_imm.grado = 'K1', recursos_imm.unityTarget, null)) as gradoK1,
                        count(distinct if(recursos_imm.grado = 'K2', recursos_imm.unityTarget, null)) as gradoK2,
                        count(distinct if(recursos_imm.grado = 'K3', recursos_imm.unityTarget, null)) as gradoK3,
                        count(distinct if(recursos_imm.grado = 'E', recursos_imm.unityTarget, null)) as gradoE,
                        count(distinct if(recursos_imm.grado = 'E1', recursos_imm.unityTarget, null)) as gradoE1,
                        count(distinct if(recursos_imm.grado = 'E2', recursos_imm.unityTarget, null)) as gradoE2,
                        count(distinct if(recursos_imm.grado = 'E3', recursos_imm.unityTarget, null)) as gradoE3,
                        count(distinct if(recursos_imm.grado = 'E4', recursos_imm.unityTarget, null)) as gradoE4,
                        count(distinct if(recursos_imm.grado = 'E5', recursos_imm.unityTarget, null)) as gradoE5,
                        count(distinct if(recursos_imm.grado = 'E6', recursos_imm.unityTarget, null)) as gradoE6,
                        count(distinct if(recursos_imm.grado = 'M', recursos_imm.unityTarget, null)) as gradoM,
                        count(distinct if(recursos_imm.grado = 'M7', recursos_imm.unityTarget, null)) as gradoM7,
                        count(distinct if(recursos_imm.grado = 'M8', recursos_imm.unityTarget, null)) as gradoM8,
                        count(distinct if(recursos_imm.grado = 'M9', recursos_imm.unityTarget, null)) as gradoM9,
                        count(distinct if(recursos_imm.grado = 'PF', recursos_imm.unityTarget, null)) as gradoPF,
                        count(distinct if(recursos_imm.grado = 'null', recursos_imm.unityTarget, null)) as gradonull,
                        count(distinct recursos_imm.unityTarget) as total
                FROM kn_registros
                INNER JOIN recursos_imm ON kn_registros.id_recurso = recursos_imm.id_recurso
                INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
                INNER JOIN kn_escuelas ON kn_usuarios.id_escuela = kn_escuelas.id_escuela
                WHERE recursos_imm.estatus = 'En linea' and kn_registros.fecha_sys > '2022-09'
                GROUP BY kn_escuelas.id_escuela) as a)";
        $request = $this->selectAll(true, $sql);
        return array("status"=>true,"data"=>$request);
    }
    public function getVersionApp($id_escuela){
        $sql = "SELECT kn_registros.version_app, COUNT(kn_registros.version_app) as tot_app, 
        COUNT(kn_registros.version_app)/(SELECT COUNT(kn_registros.version_app)
        FROM kn_registros 
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        WHERE id_registro IN (SELECT MAX(id_registro) FROM kn_registros GROUP BY id_kn_usuario ) and id_escuela = $id_escuela)*100 AS porciento
        
        FROM kn_registros 
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        WHERE id_registro IN (SELECT MAX(id_registro) FROM kn_registros GROUP BY id_kn_usuario ) and id_escuela = $id_escuela and kn_registros.version_app is not null
        GROUP BY kn_registros.version_app";
        $request = $this->selectAll(true, $sql);
        if($request){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "txt"=>"Escuela no encontrada");
        }
    }
    public function getDispositivo($id_escuela){
        $sql = "SELECT kn_registros.dispositivo, COUNT(kn_registros.dispositivo) as tot_dispositivo,
        COUNT(kn_registros.dispositivo)/(SELECT COUNT(kn_registros.dispositivo)
        FROM kn_registros 
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        WHERE id_registro IN (SELECT MAX(id_registro) FROM kn_registros GROUP BY id_kn_usuario ) and id_escuela = $id_escuela)*100 AS porciento
        FROM kn_registros 
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        WHERE id_registro IN (SELECT MAX(id_registro) FROM kn_registros GROUP BY id_kn_usuario ) and id_escuela = $id_escuela and kn_registros.dispositivo is not null
        GROUP BY kn_registros.dispositivo";
        $request = $this->selectAll(true, $sql);
        if($request){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "txt"=>"Escuela no encontrada");
        }
    }
    public function getios($id_escuela){
        $sql = "SELECT kn_registros.ios, COUNT(kn_registros.ios) as tot_ios, 
        COUNT(kn_registros.ios)/(SELECT COUNT(kn_registros.ios)
        FROM kn_registros 
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        WHERE id_registro IN (SELECT MAX(id_registro) FROM kn_registros GROUP BY id_kn_usuario ) and id_escuela = $id_escuela)*100 AS porciento
        FROM kn_registros 
        INNER JOIN kn_usuarios ON kn_registros.id_kn_usuario = kn_usuarios.id_kn_usuario
        WHERE id_registro IN (SELECT MAX(id_registro) FROM kn_registros GROUP BY id_kn_usuario ) and id_escuela = $id_escuela and kn_registros.ios is not null
        GROUP BY kn_registros.ios";
        $request = $this->selectAll(true, $sql);
        if($request){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "txt"=>"Escuela no encontrada");
        }
    }
    public function getPromedioEscuelas($id_escuela){
        $sql = "";
        $request = $this->selectAll(true, $sql);
        if($request){
            return array("status"=>true, "data"=>$request);
        }
        else{
            return array("status"=>false, "txt"=>"Escuela no encontrada");
        }
    }
    public function getPeriod(){
        $sql = 'SELECT kn_registros.idPeriod_int, Period.name
                FROM kn_registros LEFT JOIN Period ON kn_registros.idPeriod_int = Period.idPeriod_int
                GROUP BY kn_registros.idPeriod_int';
        $request = $this->selectAll(true, $sql);
        if($request){
            return array("status"=>true,"data"=>$request);
        }
    }
}
?>