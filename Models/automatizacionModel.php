<?php
class automatizacionModel extends mysql{
    public function __construct(){
        parent::__construct();
    }
    public function selectinteractives(){
        $sql = "SELECT * FROM interactives WHERE Tipo = 'Multiple Choice' and (Version = '3 Opc' ) and Version != 'IMV' and Grado not like '%K%' limit 0, 50";
        $request = $this->selectAll(true, $sql);
        $this->bitacora(array("INTERACTIVES: AUTOMATIZACION",$_SESSION['id_user']));
        return $request;
    }
    public function selectInteractive($id){
        $sql = "SELECT * FROM interactives WHERE id_RB = $id;";
        $request = $this->select(true, $sql);
        $this->bitacora(array("INTERACTIVES: AUTOMATIZACION VER",$_SESSION['id_user']));
        return $request;
    }
    public function updateInteractive($id,$nomenclatura ){
        $sql = "UPDATE interactives SET nomenclatura = ?, descargado = '1' WHERE id_RB = $id";
        $arrData = array($nomenclatura);
        $request = $this->update(true, $sql, $arrData);
        return $request;
    }
    public function uploadZIP(string $url, $id){
        // curl_setopt($archivo_descarga, CURLOPT_URL, ''); //ponemos lo que queremos descargar
        $remote_file_url = $url;
        $url1 = substr($url,strpos($url,"resources/")+10,strpos($url,".zip"));
        $url2 = substr($url1,strpos($url1,"_")+1,strrpos($url1,"_"));
        $url3 = substr($url2,0,strpos($url2,"_"));
        $local_file = 'assets/interactives/original/'.$id.'.zip';
        if(file_exists($local_file)){
            return $url3;
        }
        else{
            $copy = copy($remote_file_url, $local_file);
            if ($copy) {
                $zip = new ZipArchive;
                // Declaramos el fichero a descomprimir, puede ser enviada desde un formulario
                $comprimido= $zip->open($local_file);
                if ($comprimido=== TRUE) {// Declaramos la carpeta que almacenara ficheros descomprimidos
                    $zip->extractTo('assets/interactives/original/'.$url3);
                    $zip->close();
                    $this->updateInteractive($id, $url3);
                    // Imprimimos si todo salio bien
                        // echo 'El fichero se descomprimio correctamente!';
                    } else {
                    // Si algo salio mal, se imprime esta seccion
                        // echo 'Error descomprimiendo el archivo zip';
                        return false;
                    }
                return $url3;
            } else {
                return 2;
            }
        }
    }
}
?>